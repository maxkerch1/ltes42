#!/usr/bin/python2
## dediserve servers should have 'fuman' in hsotname
##spacer limit is 100g for vpsserver, 50g for cloudways, 30g for dediserve,  20g for time4vps
import os,sys
from os import system as cmd
from os import popen as cmd2
from os import popen3 as cmd3

HOME=os.getenv('HOME')+'/';

ffmpeg_cmd_name='parg'

def highlight(word):
  highlight_begin='\x1b[40m\x1b[37m\x1b[1m';
  highlight_reset='\x1b[39m\x1b[39m\x1b[0m';
  return highlight_begin+word+highlight_reset

hk_app_id='';oc_id='';scalingo_id=''
if os.path.exists(HOME+'/.env'):
  b=[i.strip() for i in open(HOME+'/.env').readlines() if i.strip()]
  b2=[i for i in b if 'HK_APP_ID=' in i]
  b3=[i for i in b if 'OC_NUM=' in i]
  b4=[i for i in b if 'SC_NUM=' in i]
  if b2:
    hk_app_id=b2[0].replace('HK_APP_ID=','').strip()
  elif b3:
    oc_id=b3[0].replace('OC_NUM=','').strip()
  elif b4:
    x=int(b4[0].replace('SC_NUM=','').strip())
    #oveeride based on worker , use sc_num to mul 5
    if os.environ.has_key('CONTAINER') and os.environ['CONTAINER'].startswith('worker'):
      scalingo_id=(5*(x-1))+int(os.environ['CONTAINER'].replace('worker-',''))
      print 'overriding scalingo_id to '+str(scalingo_id)
    
elif os.path.exists(HOME+'/app/.env'):
  b=[i.strip() for i in open(HOME+'/app/.env').readlines() if i.strip()]
  b3=[i for i in b if 'OC_NUM=' in i]
  b4=[i for i in b if 'SC_NUM=' in i]
  if b3:
    oc_id=b3[0].replace('OC_NUM=','').strip()
  elif b4:
    scalingo_id=b4[0].replace('SC_NUM=','').strip()
    # ~ if os.environ.has_key('CONTAINER') and os.environ['CONTAINER'].startswith('worker'):
    # ~ print str([k for k in os.environ])
    if os.environ.has_key('CONTAINER'):
      scalingo_id=os.environ['CONTAINER'].replace('worker-','')
      # ~ print 'overriding scalingo_id to '+str(scalingo_id)
    

tqdm_found=False
if os.path.exists(HOME+'/app/requirements.txt'):
  tqdm_found=[i.strip() for i in open(HOME+'/app/requirements.txt').readlines() if i.strip()=='tqdm']
  
uname=os.uname()
systype='';
if 'xshell' in uname[1]: systype='xshellz'
elif 'devshell' in uname[1]: systype='cloudshell'
elif 'platapp' in uname[1]: systype='platform.sh'
elif 'ubuntu-' in uname[1] and 'mb-' in uname[1]: systype='vpsserver';
elif 'shananalla88' in uname[1] or 'jan781198791' in  uname[1]: systype='paiza'
elif 'goorm' in uname[1]: systype='goorm'
elif oc_id:
  if tqdm_found: systype='cloudfoundry'
  elif 'blog-django' in uname[1] or 'sample' in uname[1]: systype='openshift'
elif os.getenv('USER') in ['vcap']:  systype='openshift';  os.system('ln -s app/.env .')
elif scalingo_id: systype='scalingo'
elif hk_app_id or os.path.exists(HOME+'/Procfile'): systype='heroku'
elif  'alwaysdata' in uname[2]: systype='alwaysdata'
elif  'ubu16_test' in uname[1]: systype='codeanywhere'
elif  'cloudways' in uname[1]: systype='cloudways'
elif 'fuman' in uname[1] : systype='dediserve'
elif 'time4vps' in  uname[1]: systype='time4vps'
elif 'de1' in  uname[1]: systype='hashbang'


if len(sys.argv)>1 and sys.argv[-1] not in ['clshellinit','setscreentitle']:
  systype=sys.argv[-1]


if __name__=='__main__':
  if sys.argv[-1] in ['clshellinit']:
    # ~ print sys.argv
    if systype in ['cloudshell']:
      
      if not os.path.exists('/usr/bin/screen'): #only run command if needed
        # ~ print highlight('setting up cloudshell\n..updating git')    
        cmd('rm -fv ~/.python_scripts/bin/ffmpeg')    
#        a=open(HOME+'.setup','w')
#        a.write('screen');a.close()
        # ~ runc='sudo apt-get install opus-tools htop time screen byobu mediainfo rtorrent libssl1.0.0:amd64 -y'
        # ~ x=cmd2('cd ~/testpaw && git pull --verbose').read()
        # ~ x=cmd2('cd ~/testpaw-data && git pull --verbose').read()
        # ~ try:
          # ~ dirsize=float(cmd2('cd ~/testpaw && du -sh .').read().split()[0].strip().replace('M',''))
          # ~ if dirsize> 6.5:          cmd2('cd ~/testpaw && git gc')          
          # ~ dirsize=float(cmd2('cd ~/testpaw-data && du -sh .').read().split()[0].strip().replace('M',''))
          # ~ if dirsize> 9.5:          cmd2('cd ~/testpaw-data && git gc')
        # ~ except:
          # ~ pass
        
        # ~ print highlight('..updated git')
        # ~ runc='sudo apt-get install screen mediainfo libssl1.0.0:amd64 rtorrent -y'
        runc='sudo apt-get install screen rtorrent -y'        
        x=cmd2(runc).read()
        print highlight('...installed packages')
      else:
        pass

    else:
       pass; #for non-cloudshell systems
    sys.exit(1)
  elif len(sys.argv)>1:
    if sys.argv[1] in ['setscreentitle']:
      if len(sys.argv)>2:
        title=sys.argv[2]
        info=r'echo -n "\033k%s\033\\"' %(title)
        # ~ print info
        cmd(info)
        sys.exit(0)
    elif sys.argv[1] in ['check_gsh_valid']:
      disabled_accounts=[7,9,10,14,17,23]
      gsh_account=int(sys.argv[2])
      if gsh_account in disabled_accounts and sys.argv[-1] not in ['force','f']:
        print highlight('gsh account %.2d was disabled' %(gsh_account))
        sys.exit(1)
    elif sys.argv[1] in ['plinit']:
      print highlight('creating structure in ~/work/inst for platform.sh init..')
      mydir='work'
      cmd('mkdir -pv ~/work/inst/bin ~/work/inst/.cache ~/work/inst/ssh ~/work/inst/local ~/work/inst/config ~/work/inst/local/elinks' )
      os.chdir(HOME+'work/inst')
      print highlight('getting  testpaw, testpaw-data ...')
      x=cmd2('git clone --depth=1 https://shananalla88:nazgul0288@bitbucket.org/shananalla88/testpaw.git').read()
      x=cmd2('git clone --depth=1 https://shananalla88:nazgul0288@bitbucket.org/shananalla88/testpaw-data.git').read()
      os.chdir(HOME+'work/inst/bin/')
      print highlight('getting  progs ...')
      for prog in ['screen','drive','youtube-dl',ffmpeg_cmd_name,'ffprobe','aria2c','vd','enc']:
        if prog in ['upx','screen','drive','ffprobe','aria2c']:
          # ~ cmd('wget -q http://dl.bintray.com/parker285/dotf/'+prog+' -O '+prog+'&&chmod a+x '+prog)
          cmd('wget -q https://github.com/huranjirak/static/releases/latest/download/'+prog+' -O '+prog+'&&chmod a+x '+prog)
        elif prog in [ffmpeg_cmd_name]:
          # ~ cmd('wget -q https://johnvansickle.com/ffmpeg/builds/ffmpeg-git-amd64-static.tar.xz && tar xf ffmpeg-git-amd64-static.tar.xz && rm -f ffmpeg-git-amd64-static.tar.xz && cd ffmpeg* && mv -vf  ffmpeg ~/work/inst/bin/'+prog+'&&cd ..&&rm -rf ffmpeg*')
          # ~ cmd('wget -q http://dl.bintray.com/parker285/dotf/jumpli -O '+prog+' && chmod a+x prog')
          cmd('wget -q https://github.com/julanbator/parg/releases/latest/download/parg -O '+prog+' && chmod a+x parg')
        elif prog in ['youtube-dl']:
          cmd('wget -q https://yt-dl.org/downloads/latest/youtube-dl -O youtube-dl&&chmod a+x youtube-dl')
        elif prog in ['vd','enc']:
          cmd('ln -sv ~/work/inst/testpaw/'+prog+' .')
        print prog+'..'
      print highlight('gettiing gdrives ...')
      os.chdir(HOME+'/work')
      # ~ cmd('wget -q http://dl.bintray.com/parker285/dotf/gd30.zip')
      cmd('wget -q https://github.com/huranjirak/dynamic/releases/latest/download/gd30.zip')
      x=cmd2('unzip -q -P ilovefumanchu gd30.zip').read()
      if os.path.exists('gd30.zip'): os.remove('gd30.zip')
      sys.exit(0)
        
    
  os.chdir(HOME)
  print 'detected system of type: '+highlight(systype.upper())

  # ~ for dediserve need pip,git
  git_cmd='git'
   
  cmd('mkdir -pv work')
  if systype in ['goorm','vpsserver','codeanywhere','cloudshell','dediserve','time4vps']:
    print highlight('updating apt ...')
    x=cmd2('sudo apt-get update').read()
    aptprogs='git opus-tools  htop time python-pip unzip mediainfo screen rtorrent fontconfig p7zip-full  aria2 libssl1.0.0:amd64'  #imagemagick not because large, libnuma-dev only needed by x265 cli,  webp pulls in too many deps
    print highlight('installing apt progs ...')+'\n'+aptprogs
    x=cmd2('sudo apt-get install '+aptprogs+' -y').read()
    
  print highlight('getting  testpaw, testpaw-data ...')
  x=cmd2(git_cmd+' clone --depth=1 https://shananalla88:nazgul0288@bitbucket.org/shananalla88/testpaw.git').read()
  x=cmd2(git_cmd+' clone --depth=1 https://shananalla88:nazgul0288@bitbucket.org/shananalla88/testpaw-data.git').read()
  
  for ff in ['.bash_aliases','.bash_profile','.screenrc']:
    # ~ if os.path.exists(ff): os.remove(ff)
    cmd('rm -fv '+ff+' && ln -sv testpaw-data/'+ff+' .')
  if systype not in ['cloudways','dediserve']: cmd('rm -fv .bashrc && ln -sv testpaw-data/.bashrc .')
  

  
  
  if systype in ['alwaysdata','xshellz','hashbang']:
    print highlight('doing elinks stuff')
    cmd('mkdir -pv ~/.elinks')
    os.chdir(HOME+'/.elinks')
    cmd('ln -svf ~/testpaw-data/elinks.conf .')
    cmd('ln -svf ~/testpaw-data/bookmarks.xbel .')
    cmd('ln -svf ~/testpaw/hooks.py .')    
  
  if systype not in ['alwaysdata','xshellz','hashbang','openshift','cloudfoundry','heroku','platform.sh']:
    print highlight('configuring aria')
    cmd('mkdir -p ~/.aria2')
    os.chdir(HOME+'/.aria2')
    cmd('ln -s ~/testpaw-data/aria2.conf .')
    
    print highlight('configuring youtube-dl')
    cmd('mkdir -p ~/.config/youtube-dl')
    os.chdir(HOME+'/.config/youtube-dl')
    cmd('ln -s ~/testpaw-data/youtube-dl_config config')
    
  os.chdir(HOME+'work')
  
  cmd('mkdir -pv 2')
  os.chdir('2')
  print highlight('getting basic progs and libs...')
  progsfile='python_scripts.tar.xz'
  # ~ progsfile='xshell_progs.tar.xz'
  # ~ if systype in ['alwaysdata','vpsserver','cloudshell','cloudways','dediserve','time4vps','openshift','cloudfoundry','heroku']:
    # ~ progsfile='alwaydsata_progs_new.tar.xz'
  
  
  cmd('wget -q https://github.com/huranjirak/static/releases/latest/download/'+progsfile)
  cmd('tar xf '+progsfile)
  cmd('mv -vf .python_scripts ~')
  os.chdir(HOME+'work')
  cmd('rm -rfv 2')
  if os.path.exists(progsfile): cmd('rm '+progsfile)
  
  print highlight('fixing progs and getting more as needed in ~/.python_scripts/bin...')
  os.chdir(HOME+'.python_scripts/bin')
  cmd('rm -fv vd enc')
  cmd('ln -sv ~/testpaw/vd .')
  cmd('ln -sv ~/testpaw/enc .')

  #install pip2 on openshift
  if systype in ['openshift']:# and ('no pip2' in cmd2('which pip2').read().strip()):
    cmd('wget -q https://bootstrap.pypa.io/get-pip.py&&python2 get-pip.py --user&&rm get-pip.py')
    os.chdir(HOME+'.python_scripts/lib')
    cmd('curl --progress-bar "http://guha67512.atwebpages.com/libssl.so.1.0.0" -o "libssl.so.1.0.0"')
    cmd('curl --progress-bar "http://guha67512.atwebpages.com/libcrypto.so.1.0.0" -o "libcrypto.so.1.0.0"')
    
  os.chdir(HOME+'.python_scripts/bin')
  if systype in ['paiza','goorm','vpsserver','codeanywhere','cloudshell','cloudways','dediserve','time4vps','cloudfoundry','openshift','heroku','platform.sh','scalingo']:
    print highlight('installing programs to ~/.python_scripts/bin/')
    
    progs=['phantomjs','youtube-dl','ffprobe',ffmpeg_cmd_name]; #appencoder
    for prog in progs:      
      #skipped for certain cases
      # ~ if (prog in [ffmpeg_cmd_name,'ffprobe']) and ffsuccess: continue
      if (prog in ['aria2c']) and (systype in ['time4vps']): continue
      if (prog in ['appencoder','phantomjs','unrar']) and (systype in ['cloudfoundry','openshift','heroku','platform.sh','scalingo']): continue; #no need 
      
      print prog,' ...'
      if os.path.exists(prog):os.remove(prog)
      
      #different files from different places
      if prog in ['youtube-dl']:
        cmd('wget -q https://yt-dl.org/downloads/latest/youtube-dl -O youtube-dl')
      # ~ elif prog in ['appencoder']:
        # ~ cmd('wget  -q https://raw.githubusercontent.com/ksvc/ks265codec/master/ubuntu_x64/appencoder -O '+prog)
      elif prog in ['phantomjs']:
        cmd('wget -q http://dl.bintray.com/parker285/dotf/'+prog+' -O '+prog)
      elif prog in [ffmpeg_cmd_name]:
        # ~ if systype in ['heroku','platform.sh','scalingo']:#use compressed version to save download size on heroku
        # ~ cmd('wget -q http://dl.bintray.com/parker285/dotf/jumpli -O '+prog)
        cmd('wget -q "https://github.com/huranjirak/parg/releases/latest/download/parg" -O '+prog)
        # ~ else:
          # ~ cmd('wget -q http://dl.bintray.com/parker285/dotf/jumpli.uncompressed -O '+prog)
      elif prog in ['ffprobe']:
        # ~ if systype in ['heroku','platform.sh','scalingo']:#use compressed version to save download size on heroku
        # ~ cmd('wget -q http://dl.bintray.com/parker285/dotf/'+prog+' -O '+prog)
        cmd('wget -q https://github.com/huranjirak/parg/releases/latest/download/'+prog+' -O '+prog)
        # ~ else:
          # ~ cmd('wget -q http://dl.bintray.com/parker285/dotf/'+prog+'.uncompressed -O '+prog)
      
      cmd('chmod a+x '+prog)
    
  
  # ~ cloudways has no sceen, so get staic
  #if systype in ['cloudways','openshift','cloudfoundry','platform.sh']:
  if systype in ['cloudways','openshift','cloudfoundry','platform.sh','scalingo']:
    cmd('wget -q https://github.com/huranjirak/static/releases/latest/download/screen &&chmod a+x screen')
    if systype in ['platform.sh','scalingo']:
      cmd('mkdir ~/.screen && chmod 700 ~/.screen && mkdir ~/work&&chmod 700 ~/work')
  if systype in ['openshift']:
    os.chdir(HOME)
    print highlight('rtorrent for openshift..')
    cmd('wget -q https://raw.githubusercontent.com/static-linux/static-binaries-i386/master/rtorrent-0.9.3.tar.gz')
    cmd('tar xf rtorrent-0.9.3.tar.gz&&cd rtorrent-0.9.3&&mv rtorrent ~/.python_scripts/bin/&&cd ..&&rm -r rtorrent*')
  # ~ cookies 
  os.chdir(HOME+'/.python_scripts')
  os.system('rm -fv cookies.txt&&ln -sv ~/testpaw/cookies.txt .')
  
  print highlight('gettiing gdrives ...')
  os.chdir(HOME+'/work')
  # ~ cmd('wget -q http://dl.bintray.com/parker285/dotf/gd30.zip')
  cmd('wget -q https://github.com/huranjirak/dynamic/releases/latest/download/gd30.zip')
  x=cmd2('unzip -q -P ilovefumanchu gd30.zip').read()
  if os.path.exists('gd30.zip'): os.remove('gd30.zip')
  
  
  if systype not in ['heroku','cloudfoundry','platform.sh','scalingo']:#heroku uses requirements.txt
    print highlight('update/install with pip...')
    pipcmd='pip'
    if systype in ['vpsserver','codeanywhere','cloudshell','platform.sh','scalingo']:    pipcmd='pip2'
    elif systype in ['openshift']:    pipcmd=HOME+'/.local/bin/pip2'
    x=cmd2(pipcmd+' install --user setuptools').read()
    x=cmd2(pipcmd+' install --user wheel').read()
    x=cmd2(pipcmd+' install --user bs4 requests cloudconvert tqdm colorama progressbar lazy_import  requests_toolbelt pyyaml ').read()
    if systype not in ['xshellz','alwaysdata']: x=cmd2(pipcmd+' install --user dropbox  lxml' ).read(); #problem with pymp4
    x=cmd2(pipcmd+' install --user --upgrade six requests').read()
    # ~ if systype not in ['openshift']:x=cmd2(pipcmd+' install --user  psutil').read()
  #clear pip cache
  if os.path.exists(HOME+'.cache'): cmd('rm -rf ~/.cache')

  
  
  # ~ if systype in ['vpsserver','cloudshell','xshellz']:
  # ~ if systype in ['xshellz','alwaysdata']:
    # ~ print highlight('updating proxy list')
    # ~ cmd(HOME+'/testpaw/vd update_proxies_list')
  
  #no apt warn
  if systype in ['cloudshell','openshift','heroku']:
    if systype in ['cloudshell']:
      cmd('mkdir -pv ~/.cloudshell && touch ~/.cloudshell/no-apt-get-warning && touch ~/.cloudshell/no-pip-warning && touch ~/.cloudshell/no-python-warning && touch ~/.cloudshell/no-pip-warning')
    cmd('echo  "schedule2 = low_diskspace, 60, 60, close_low_diskspace=300M\nthrottle.global_down.max_rate.set_kb = 4500\nthrottle.global_up.max_rate.set_kb = 750" > ~/.rtorrent.rc') #init files
  
  if systype not in ['heroku','openshift','cloudfoundry','platform.sh','scalingo']:#may cause problems to change ssh config on these platforms
    # ~ add ssh auth
    cmd('mkdir -pv ~/.ssh')
    al=''
    if os.path.exists(HOME+'/.ssh/authorized_keys'):
      a=open(HOME+'/.ssh/authorized_keys','r');al=a.read();a.close()
    if 'aniru@aniru' not in al:
      print highlight('authorizing ssh id_rsa ...')
      a=open(HOME+'/.ssh/authorized_keys','a')
      a.write('ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDRG8FKo6wWcPY3QtRNVvIGEVda63qX5icfqhhmNQ02q24KEnsM7SWu6ppIDsL9XQJ/nrOikEA7olFRk6yIwv9UaZUz5WtWbjIoSXMHCA+Z3znWHvB+mi2BjRp4G35LCijSfCE2xwAK1scdmv6imh0mQb/JwLZjoaYLSj25jIoMkXugGZy+FDWd96UvCd2af290CayV76J1muanfzkTQ0SaH2NDsNtgYMid4C2Vs5CRbQDxT30fAF0vSNZN+uhVqE/jZN0lmzqIsYiV6kjKRpufdcPoRrJvwypgkSyM4GkAZPX/GkcbpYU1pKu0lWbzNS4ZAPZUM8ApeUirsk+0J291 aniru@aniru-newLap-18')
      a.close()
  
  # ~ needed for clf cloudconvert
  if systype in ['xshellz','alwaysdata']:
    cmd('mkdir -pv ~/.python_scripts/avisynth_scripts/parallel_helpers/acinfo/')
  if systype in ['cloudfoundry']:
    cmd('cd&&ln -sv app/.env .')

  if systype in ['alwaysdata']: #hack for elinks-python
    os.system('mkdir -pv ~/.python_scripts/scripts')
    os.chdir(HOME+'/.python_scripts/scripts')
    os.system('ln -sv /usr/lib/python2.7/plat-x86_64-linux-gnu/_sysconfigdata_nd.py .')

  
  if systype in ['alwaysdata']:
    os.chdir(HOME+'/.python_scripts/bin')
    for prog in ['cwebp','elinks-latest','aria2c','mediafire-shell','elinks-python-xshell']:
      if os.path.exists(prog): os.remove(prog)
    
    
  if systype in ['xshellz','alwaysdata','hashbang']:
    os.chdir(HOME)
    cmd('mkdir -pv ~/.vim')
    cmd('ln -sv   ~/testpaw-data/vimrc ~/.vim/vimrc')

  if systype in ['platform.sh']: #put modifiable stuff (testpaw/,testpaw-data/, bin/ in work/inst/)

    print highlight('creating writable dirs ~/testpaw,~/testpaw-data,~/.python_scripts/bin for platform.sh')
    cmd('mkdir -pv ~/work/inst &&cd ~/work/inst&&mkdir testpaw testpaw-data bin local config ssh &&cd && rm -rf testpaw testpaw-data .python_scripts/bin && ln -sfv ~/work/inst/testpaw testpaw && ln -sfv ~/work/inst/testpaw-data testpaw-data && ln -sfv ~/work/inst/bin .python_scripts/bin&& ln -sfv ~/work/inst/local .local&& ln -sfv ~/work/inst/config .config&& ln -sfv ~/work/inst/ssh .ssh && ln -sfv ~/work/inst/cache .cache && ln -sfv ~/work/inst/config/netrc .netrc&& ln -sfv ~/work/inst/local/elinks .elinks ')
  # ~ elif systype in ['scalingo']: #put modifiable stuff (testpaw/,testpaw-data/, bin/ in work/inst/)
    # ~ cmd('mkdir -pv ~/tmp/inst &&cd ~/tmp/inst&&mkdir testpaw testpaw-data bin &&cd && rm -rf testpaw testpaw-data .python_scripts/bin && ln -sfv ~/tmp/inst/testpaw testpaw && ln -sfv ~/tmp/inst/testpaw-data testpaw-data && ln -sfv ~/tmp/inst/bin .python_scripts/bin')
  
  print highlight('done.size of home now:')
  cmd('du -sh ~/')  
  cmd('bash ~/.bash_profile')
  
