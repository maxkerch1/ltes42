from constants import *
# ~ from vd import solidfiles_queryfiles
requests=lazy_import.lazy_module('requests')

highlight_begin=colorama.Back.BLACK+colorama.Fore.WHITE+colorama.Style.BRIGHT
highlight_begin_red=colorama.Back.BLACK+colorama.Fore.RED+colorama.Style.BRIGHT
highlight_begin_yellow=colorama.Back.BLACK+colorama.Fore.YELLOW+colorama.Style.BRIGHT
highlight_begin_green=colorama.Back.BLACK+colorama.Fore.GREEN+colorama.Style.BRIGHT
highlight_begin_blue=colorama.Back.BLACK+colorama.Fore.BLUE+colorama.Style.BRIGHT
highlight_begin_magenta=colorama.Back.BLACK+colorama.Fore.MAGENTA+colorama.Style.BRIGHT
highlight_begin_cyan=colorama.Back.BLACK+colorama.Fore.CYAN+colorama.Style.BRIGHT
highlight_reset=colorama.Back.RESET+colorama.Fore.RESET+colorama.Style.RESET_ALL

TMP_MPV_IMAGEVIEWER_LOCKFILE='/tmp/tmp.mpvimageview.mylockfile'
  
def prunearia():
  finished_videos=[i for i in os.listdir('.') if i.endswith(video_extensions) and not os.path.exists(i+'.aria2')]
  empty_videos=[i for i in finished_videos if (os.stat(i.strip()).st_size/1024.)<=100];#less than 100k
  finished_videos=[i for i in finished_videos if (os.stat(i.strip()).st_size/1024.)>100];#less than 100k
  b=[i.strip() for i in open('linksfile.txt').readlines() if i.strip() and not i.startswith('#')]
  try:assert(len(b)%2==0)
  except AssertionError:
    print 'length of linksfile.txt: %d is not multuple of 2.exiting' %(len(b))
    sys.exit(1)
  unfinished_videos=[]
  for j in range(len(b)/2):
    vurl=b[2*j].strip()
    vname=b[(2*j)+1].strip().split('out=')[1]
    if vname in finished_videos:
      print highlight(vname+' finished, removing from linksfile.txt','green_fore')
    elif vname in empty_videos:
      print highlight(vname+' finished but less than 100k, removing from linksfile.txt','yellow_fore')
    else:
      unfinished_videos.append((vurl,vname))
  if unfinished_videos:
    a=open('linksfile.txt','w')
    for i in unfinished_videos: a.write(i[0]+'\n\tout='+i[1]+'\n')
    a.close()      
  else:
    print 'all videos finished!'
  if sys.argv[-1] in ['mv']:
    os.system('mkdir -p done')
    for i in finished_videos: os.system('mv "'+i+'" done/')
    os.system('du -sh done/')
    if empty_videos:
      os.system('mkdir -p empty')
      for i in empty_videos: os.system('mv "'+i+'" empty/')
 
def videobin_get_url_preview_single(url):
  vname='';r=''
  if '::' in url:    
    vname=url.split('::')[0].strip()
    if sys.argv[-1] in ['debug']: print 'inside: '+vname
    url=url.split('::')[1].strip()
    r=requests.get(url).content
  else:
    r=requests.get(url).content
    soup=bs4.BeautifulSoup(r,bsp)
    vname=soup('title')[0].text
    if not vname.startswith('PORN__'): vname='PORN__'+vname
    if not vname.endswith('.mp4'): vname+='.mp4'
    vname=re.sub('[^A-Za-z0-9_()-.#]+', '_', vname)
    vname=vname.replace(',','_').replace('www_0xxx_ws_','').replace('Watch_0xxx_','')

  if sys.argv[-1] in ['debug']: print 'before souping len(content) ',len(r)
  soup=bs4.BeautifulSoup(r,bsp)
  # ~ x=[i.text for i in  soup('script') if 'spriteSheetUrl' in i.text]
  # ~ x2=[i.text for i in  soup('script') if '.mp4' in i.text]
  x=[str(i) for i in  soup('script') if 'spriteSheetUrl' in str(i)]
  x2=[str(i) for i in  soup('script') if '.mp4' in str(i)]

  if not (x and x2):
    if sys.argv[-1] in ['debug']: print 'mp4 and spritesheet not found!'
    return None
  else: x=x[0];x2=x2[0]
  
  preview=x.split()[x.split().index('spriteSheetUrl')+2].replace('"','').replace(';','').replace("'","")
  sind=x2.find('sources:')
  x2=x2[sind+8:]
  sources=x2[x2.find('[')+1:x2.find(']')]
  mp4=[i for i in sources.split(',') if '.mp4' in i]
  if not mp4: return None
  else:    mp4=mp4[0].replace('"','').replace("'","")
  fsize=get_filesize_of_url_basic((mp4,3.5))
  file_id=os.path.splitext(os.path.split(preview)[1])[0][:-4]
  if sys.argv[-1] in ['debug']: print 'end: '+vname
  return (vname,mp4,fsize,file_id,preview)

def videobin_get_urls_from_logfile(logfile='',verbose=True):
  b=[i.strip() for i in open(logfile).readlines() if i.startswith('vb-')]
  topass_urls=[]
  for i in b:
    vname=i.split('::')[1].strip()
    file_id=i.split('::')[3].strip()
    vburl='https://videobin.co/'+file_id
    topass_urls.append(vname+' :: '+vburl)
  a=open('tmpfile','w')
  for i in topass_urls:a.write(i+'\n')
  a.close()
  
  raw_info=videobin_get_urls(infile='tmpfile')
  if os.path.exists('tmpfile'):os.remove('tmpfile')
  return raw_info
  
def videobin_get_urls(infile='',get_previews=False):
  b=[i.strip() for i in open(infile).readlines() if 'videobin.co' in i]
  b2=[i for i in b if '::' in i]
  urls=[]
  topass=[]
  if b2: #file is valid_links file
    urls=[i.split('::')[1].strip() for i in b]
    # ~ vnames=[os.path.splitext(i.split('::')[0].strip())[:-1] for i in b]
    vnames=[i.split('::')[0].strip().replace('.mp4','').replace('.mkv','') for i in b]
    topass=[vnames[j]+' :: '+urls[j] for j in range(len(urls))]
  else:
    topass=b

  if sys.argv[-1] in ['debug']: print 'before: '+str(topass)
  raw_info=[]

  r=imap_bar_serial(videobin_get_url_preview_single,topass,use_dummy=False)  
  r=[i for i in r if i]
  if get_previews:    a=open('links.txt','w')
  alog=open('.vblog','w')
  a2=open('linksfile.txt','w')
  for i in r:
    vname,mp4,fsize,file_id,preview=i
    if get_previews:     a.write(preview+'\n\tout='+vname+'.jpg\n')
    a2.write(mp4+'\n\tout='+vname+'.mp4\n')
    info='vb-0 :: '+vname+' :: %.2f mb' %(fsize)+' :: '+file_id
    raw_info.append(info)
    alog.write(info+'\n')
  if get_previews:    a.close()
  alog.close()
  a2.close()
  print 'wrote %d lines to .vblog' %(len(r))
  return raw_info

def ezapi(imdb_id=''):
  x=requests.get('https://eztv.io/api/get-torrents?imdb_id='+imdb_id).json()
  if x.has_key('torrents') and x['torrents']:
    a=open('torrents.txt','w');a.write('#!/bin/sh\n')
    for i in x['torrents']:
      a.write('## %s  ::  %.1f Mb\n%s\n' %(i['title'],float(i['size_bytes'])/(1024*1024),i['torrent_url']))
    a.close()
    print 'wrote %s torrents to ./torrents.txt' %(len(x['torrents']))
  else:
    print 'no torrents found via eztv api'
def get_directory_size(start_path='.',verbose=True):
  total=0
  info=''
  for dp,dn,fn in os.walk(start_path):
    for f in fn:
      fp=os.path.join(dp,f)
      if not os.path.islink(fp):
        total+=os.path.getsize(fp)
  total_mb=0;total_gb=0;
  # ~ mult=1024.
  mult=1000.
  total_mb=total/(mult*mult)
  if total_mb > mult: total_gb=total_mb/mult  
  if total_gb: 
    info='Size of %s/ : %.2f Gb' %(start_path,total_gb)
  elif total_mb: 
    info='Size of %s/ : %.2f Mb' %(start_path,total_mb)
  if verbose: print info
  return total,info
    
# ~ def list_folder_verystream(account=5):
  # ~ v=VeryStreamObject(account)
  # ~ v.list_folder(verbose=False)
  
def highlight(text,color=''):
  if type(text)==tuple:
    color=text[-1]
    text=' '.join(text[:-1])

  #~ color can be cyan,magenta,blue,yellow,green or red
  valid_colors=['','yellow','red','green','blue','magenta','cyan','cyan_fore','cyan_fore_bright','bright','dim','green_fore','red_fore','blue_fore','green_fore_bright','green_fore_dim','bright_darkbackground','yellow_fore','red_fore_bright','blue_fore_bright','magenta_fore_bright','magenta_fore','bright','test']
  try:assert(color in valid_colors)
  except:
    print 'in Highlight: invalid color',color,' given for text ',text
  if color=='':    return highlight_begin+text+highlight_reset
  if color=='test':    return highlight_begin+text+highlight_reset
  elif color in ['yellow']:    return highlight_begin_yellow+text+highlight_reset
  elif color in ['red']:    return highlight_begin_red+text+highlight_reset
  elif color in ['green']:    return highlight_begin_green+text+highlight_reset
  elif color in ['blue']:    return highlight_begin_blue+text+highlight_reset
  elif color in ['magenta']:    return highlight_begin_magenta+text+highlight_reset
  elif color in ['cyan']:    return highlight_begin_cyan+text+highlight_reset
  elif color in ['cyan_fore']: return colorama.Fore.CYAN+text+highlight_reset
  elif color in ['cyan_fore_bright']: return colorama.Fore.CYAN+colorama.Style.BRIGHT+text+highlight_reset
  elif color in ['bright']: return colorama.Style.BRIGHT+text+highlight_reset
  elif color in ['bright_darkbackground']: return colorama.Style.BRIGHT+colorama.Back.BLACK+text+highlight_reset
  elif color in ['dim']: return colorama.Style.DIM+text+highlight_reset
  elif color in ['green_fore']: return colorama.Fore.GREEN+text+highlight_reset
  elif color in ['green_fore_bright']: return colorama.Fore.GREEN+colorama.Style.BRIGHT+text+highlight_reset
  elif color in ['green_fore_dim']: return colorama.Fore.GREEN+colorama.Style.DIM+text+highlight_reset
  elif color in ['bright']: return colorama.Style.BRIGHT+text+colorama.Style.RESET_ALL
  elif color in ['blue_fore_bright']: return colorama.Fore.BLUE+colorama.Style.BRIGHT+text+colorama.Fore.RESET+colorama.Style.RESET_ALL
  elif color in ['magenta_fore_bright']: return colorama.Fore.MAGENTA+colorama.Style.BRIGHT+text+highlight_reset
  elif color in ['magenta_fore']: return colorama.Fore.MAGENTA+text+highlight_reset
  elif color in ['red_fore_bright']: return colorama.Fore.RED+colorama.Style.BRIGHT+text+highlight_reset
  elif color in ['yellow_fore']: return colorama.Fore.YELLOW+text+highlight_reset
  elif color in ['red_fore']: return colorama.Fore.RED+text+highlight_reset
  elif color in ['blue_fore']: return colorama.Fore.BLUE+text+highlight_reset
  else:    return highlight_begin+text+highlight_reset
  
def toggle_gnome_shell_extension(extname):
  status=os.popen('gnome-shell-extension-cl -s  '+extname).read().strip()
  if status=='disabled':    print os.popen('gnome-shell-extension-cl -e '+extname).read().strip()
  else:    print os.popen('gnome-shell-extension-cl -d '+extname).read().strip()

def change_desktop_wallpaper(randomize=True,picdir='/home/aniru/Pictures/',debug=False):
  if sys.argv[-1] in ['random','randomize']:randomize=True
  pics=os.listdir(picdir);pics.sort()
  if randomize:
    picrange=range(len(pics))
    picr=random.choice(picrange)
    picr+=2
    if picr>=len(pics): picr=len(pics)-1
    curpic=pics[picr]
    print 'chose picture: %s which is %d/%d' %(curpic,picr+1,len(pics))
    
  else: #incerement to next
    cmd='gsettings get org.gnome.desktop.background picture-uri'
    curpic=os.popen(cmd).read().strip()
    curpic=os.path.split(curpic)[1].replace('"','').replace("'",'')
    curindex=pics.index(curpic)
    if curindex==(len(pics)-1):curindex=0
    else:curindex+=1
    curpic=pics[curindex]
  
  cmd='gsettings set org.gnome.desktop.background picture-uri "file://'+picdir+curpic+'"'
  if debug:print cmd
  os.system(cmd)

def phub_make_montages_single(line):
  imbase=line.split('::')[0].strip()
  video_id=line.split('::')[1].strip()
  webp_quality=line.split('::')[2].strip()
  #make montage
  cmd='montage '
  
  #download images
  a=open(video_id+'-aria.txt','w')
  for j in range(1,17):
    url=imbase+str(j)+'.jpg'
    filename=video_id+'_%.2d' %(j)+'.jpg'
    a.write(url+'\n\tout='+filename+'\n')
  a.close()
  cmd2='aria2c -q -i '+video_id+'-aria.txt'
  x=os.system(cmd2)
  
  # ~ make montage
  for j in range(1,17):
    filename=video_id+'_%.2d' %(j)+'.jpg'
    if os.path.exists(filename): cmd+=filename+' ';   
  cmd+=' -geometry +0+0 htm/images/montages/'+video_id+'.png'
  if 'verbose' in sys.argv[-1]: print cmd
  x=os.popen3(cmd)
  x=x[1].read()
  
  # ~ convert to webp
  cmd='cwebp -quiet -m 6 -q '+webp_quality+' -resize  480 272 htm/images/montages/'+video_id+'.png -o  htm/images/montages/'+video_id[0]+'/'+video_id+'.jpg'
  os.system(cmd)
  

  
  #remove temp files
  if os.path.exists('htm/images/montages/'+video_id+'.png'):os.remove('htm/images/montages/'+video_id+'.png')
  if os.path.exists(video_id+'-aria.txt'): os.remove(video_id+'-aria.txt')
  if 'dontrem' not in sys.argv[-1] :
    for j in range(1,17):
      filename=video_id+'_%.2d' %(j)+'.jpg'
      if os.path.exists(filename): os.remove(filename);
    
  
def erofus(folder='https://www.erofus.com/comics/Kirtu-com-Free-Comix'):
  def check_if_last(x2):
    last=[os.path.split(i)[1] for i in x2]
    is_last=True
    for i in last:
      if not i.isdigit():
        is_last=False
        break
    return is_last
  htm=requests.get(folder).content
  soup=bs4.BeautifulSoup(htm,bsp)
  x=['https://www.erofus.com'+i['href'] for i in soup('a',attrs={'class':'a-click'}) if i.has_attr('href') and i.has_attr('title') and 'comics/' in i['href']]
  leafs=[];failed=[]
  p1=tqdm.tqdm(x,'1st level')
  p2=''
  for leaf in p1:
    p1.set_description(leaf)
    htm=requests.get(leaf).content
    soup=bs4.BeautifulSoup(htm,bsp)
    x2=['https://www.erofus.com'+i['href'] for i in soup('a',attrs={'class':'a-click'})]
    if check_if_last(x2):      leafs.extend(x2)
    else:
      #go deeper to 2nd level
      p2=tqdm.tqdm(x2,'2nd level')
      for subleaf in p2:
        p2.set_description(subleaf)
        htm=requests.get(subleaf).content
        soup=bs4.BeautifulSoup(htm,bsp)
        x3=['https://www.erofus.com'+i['href'] for i in soup('a',attrs={'class':'a-click'})]
        if check_if_last(x3):      leafs.extend(x3)
        else:
          #go more deeper to 3rd level
          for subsubleaf in tqdm.tqdm(x3,'3nd level'):            
            htm=requests.get(subsubleaf).content
            soup=bs4.BeautifulSoup(htm,bsp)
            x4=['https://www.erofus.com'+i['href'] for i in soup('a',attrs={'class':'a-click'})]
            if check_if_last(x4):      leafs.extend(x4)
            else:
              #something failed. write
              failed.extend(x3)
          
  print highlight('found %d leafs, now getting raw image urls' %(len(leafs)))
  if failed: print '%d entrries failed' %(len(failed))

  aria=[]
  for i in tqdm.tqdm(leafs):
    htm=requests.get(i).content
    soup=bs4.BeautifulSoup(htm,bsp)
    imurl='https://www.erofus.com'+soup('div',attrs={'id':'picture-full'})[0]('img')[0]['src']
    imdir='/'.join(i.split('/')[5:])
    imnum=os.path.split(imdir)[1]
    imdir=os.path.split(imdir)[0]
    imname='%.3d.jpg' %(int(imnum))
    aria.append((imurl,imname,imdir))
    
  # ~ a=open('leafs','w')
  # ~ for i in leafs: a.write(i+'\n')
  # ~ a.close()
  # ~ a=open('failed','w')
  # ~ for i in failed: a.write(i+'\n')
  # ~ a.close()
  a=open('linksfile.txt','w')
  for i in aria: a.write(i[0]+'\n\tout='+i[1]+'\n\tdir='+i[2]+'\n')
  a.close()
  # ~ print 'wrote %d entrries to ./leafs' %(len(leafs))
  # ~ if failed: print 'wrote %d entrries to ./failed' %(len(failed))
  
def phub_make_montages(infile='',processes=3):
  if sys.argv[-1].isdigit(): 
    processes=int(sys.argv[-1])
    print 'setting number of processes to ',processes
  if systype in ['cloudshell']:
    os.system('sudo apt-get install webp imagemagick -y')
  b=[i.strip() for i in open(infile).readlines() if i.strip()]
  os.system('mkdir -p htm/images/montages/')
  for j in range(10):
    os.system('mkdir -p htm/images/montages/'+str(j))
  x=imap_bar(phub_make_montages_single,b,processes,False)
  
def phub_get_video_urls(infile='',verbose=True):
  # ~ proc=5
  sleeptime=0.5
  # ~ if sys.argv[-1].isdigit():
  if sys.argv[-1].replace('.','').isdigit():
    # ~ proc=int(sys.argv[-1])
    sleeptime=float(sys.argv[-1])
    # ~ print 'getting all phub urls from ',infile,' using ',proc,' parallel processes'
    print 'sleeping for  %.1f sec between urls' %(sleeptime)
    
  b0=[i.strip() for i in open(infile).readlines() if i.strip() and not i.startswith('#')]
  if b0 and b0[0].startswith('ph-'): #is logfile
    bx=[]
    for i in b0:
      viewkey=i.split('::')[3].strip().split('___')[0]
      video_id=i.split('::')[3].strip().split('___')[1]
      bx.append('https://www.pornhub.com/view_video.php?viewkey='+viewkey+' , '+video_id)
    b0=bx
  else:
    b0=[i.strip() for i in b0 if 'view_video.php?viewkey' in i]
  b=[]
  for vid in b0:
    if vid.startswith('file:///'):vid=vid.replace('file:///','https://www.pornhub.com/')
    b.append(vid)
  plids_ex=[];b2=[]
  for i in b:
    try:
      pl_id=i.split(',')[1].strip()
      if pl_id not in plids_ex: 
        plids_ex.append(pl_id)
      b2.append(i)
    except: 
      # ~ print i;
      continue
    

  b=list(set(b));b.sort()
  r=''
  if systype in ['xshellz'] or ('par' not in sys.argv[-1]):
    print 'using serial mode '
    import subprocess
    r=[]
    video_urls=[i.split(',')[0].strip() for i in b]
    video_ids=[]
    for i in b:
      if len(i.split(','))>1: video_ids.append(i.split(',')[1].strip())
      else: video_ids.append('0000')
    video_ids_ytdl=[i.split('viewkey=')[1].split('&')[0] for i in video_urls]

    #make map from (detected) ids to "real" ids
    vmap={}
    for j in range(len(video_ids)):      vmap[video_ids_ytdl[j]]=(video_ids[j],video_urls[j])
    
    a=open('tmpfile','w')
    for i in video_urls: a.write(i+'\n')
    a.close()
    maxh=900
    yt_cmd='youtube-dl --ignore-errors --get-id --get-title --get-url -f "best[height<'+str(maxh)+']/best" --sleep-interval '+str(sleeptime)+' --socket-timeout 10 -a tmpfile 2> /dev/null'
    pbar=tqdm.tqdm(total=len(video_urls),desc='ytdl getting info..')
    alllines=[]
    # ~ process = subprocess.Popen(yt_cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,universal_newlines=True,shell=True)
    process = subprocess.Popen(yt_cmd, stdout=subprocess.PIPE,universal_newlines=True,shell=True)
    cnt=1;num_failed=0
    line=process.stdout.readline()
    if line.strip():alllines.append(line.strip())
    while line:
      # ~ print "l%.3d: %s" %(cnt,line)
      line=process.stdout.readline()
      if line.strip(): alllines.append(line.strip())
      else:
        num_failed+=1
        pbar.desc='failed('+str(num_failed)+')'
      cnt+=1
      if cnt%3==0:      pbar.update(1)
    pbar.close()
    try: assert((len(alllines)%3)==0) #should be multiple of 3
    except AssertionError:
      print 'lines output was not multiples of 3.exiting!!'
      print pprint.pprint(alllines)
      sys.exit(1)
    
    for j in range(len(alllines)/3):
      ph_url='';video_id_real=''
      vname=alllines[3*j]
      video_id_detected=alllines[(3*j)+1]
      if video_id_detected in vmap:
        video_id_real,ph_url=vmap[video_id_detected]        
      else:
        video_id_real='UNKNOWN'
      video_down_url=alllines[(3*j)+2]
      vname='PORN__'+vname+'-PH'+video_id_real+'.mp4'
      vname=re.sub('[^A-Za-z0-9_()-.#]+', '_', vname)
      r.append((video_down_url,vname,ph_url))
    # ~ with tqdm.tqdm(b) as pbar:
      # ~ for vv in pbar:      
        # ~ r2=phub_get_video_url(vv)
        # ~ if r2: r.append(r2)
        # ~ if r2 and r2[0] and r2[1]: cnt+=1
        # ~ pbar.desc='getting ph links (%d)' %(cnt)
        # ~ time.sleep(sleeptime)
  else:
    print 'finding linsk in parallel'
    r=imap_bar(phub_get_video_url,b,use_dummy=True)
  vurls=[i[0] for i in r if i]
  vnames=[i[1] for i in r if i]
  phurls=[i[2] for i in r if i]
  
  a=open('linksfile.txt','w');cnt=0
   
  for j in range(len(vurls)):
    vurl=vurls[j];vname=vnames[j]
    if type(vname)==unicode:  vname=vname.encode('ascii','replace')    
    if not (vurl and vname): continue
    a.write(vurl+'\n')
    cnt+=1
  a.write('----------------\n')
  for j in range(len(vurls)):
    vurl=vurls[j];vname=vnames[j]
    if type(vname)==unicode:  vname=vname.encode('ascii','replace')    
    if not (vurl and vname): continue
    a.write(vname+'\n')
  a.close()
  print 'found ',cnt,' valid phub links out of ',len(b)
  raw_info=[]
  if sys.argv[-1] not in ['nowr','nowrite','nomklog']:
    fsizes=imap_bar(get_filesize_of_url_basic,vurls,3,desc='getting size of phub links')
    sep=' \t:: ';a=open('.phlog','w')
    for j in range(len(vnames)):
      video_id=vnames[j].rsplit('-PH')[-1].split('.')[0].strip()
      viewkey=phurls[j].split('viewkey=')[1].split('&')[0].strip()
      video_marker=viewkey+'___'+video_id
      info='ph-0'+sep+vnames[j]+sep+'%.1f mb' %(fsizes[j])+sep+video_marker
      raw_info.append(info)
      a.write(info+'\n')
    a.close()
    print 'wrote %d log entries  to .phlog' %(len(fsizes))
    return raw_info
  
def phub_get_video_url(vid,hq=True,verbose=False):
  video_id_known=False
  if sys.argv[-1] in ['hq']: 
    hq=True;
    if verbose: print 'setting hq (720p) mode'
  if vid.startswith('file:///'):vid=vid.replace('file:///','https://www.pornhub.com/')
  dwnc='wget -q -U "Mozilla/5.0 (X11; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0 (Chrome)" '
  if os.path.exists(HOME+'/.python_scripts/cookies.txt'):
    # ~ dwnc+=' --load-cookies='+HOME+'/.python_scripts/cookies.txt "'
    dwnc+=' "'
  else: dwnc+=' "'
  dwnc+=vid+'" -O - '
  # ~ page_source=os.popen(dwnc).read()
  
  if ',' in vid:
    vv=vid.split(',')
    vid=vv[0].strip()
    video_id=vv[1].strip()
    video_id_known=True
    
  try:
    page_source=url_get_content(vid,proxy=None)
  except:
    print 'could not get page source for ',vid
    return
  if sys.argv[-1] in ['debug']: print 'len (page_source) :',len (page_source)
  re0=re.compile(r'var\s+flashvars_\d+\s*=\s*({.+?});')
  js=re0.findall(page_source)
  if not js: return None
  js=json.loads(js[0])
  video_urls={}
  
  #get video name
  if not video_id_known:
    video_id='UNKNOWN'
    if 'original' in js['image_url'].split('/'):
      # ~ video_id_index=js['image_url'].split('/').index('original')-1; # e.g. 'https://ci.phncdn.com/videos/201501/07/37129011/original/(m=eaAaGwObaaaa)(mh=BxJCaSX6bHiSJFW-)12.jpg'
      video_id=js['image_url'].rsplit('/')[-3]
  
  vname='PORN__'+js['video_title']+' -PH'+video_id+'.mp4'
  vname=vname.replace('@','a').replace('/','_').replace('*','_').strip()
  vname=re.sub('[^A-Za-z0-9_()-.#]+', '_', vname)
  vname=vname.replace(',','_')
  
  #find video urls
  for q in js['mediaDefinitions']:

    quality=q['quality']
    url=q['videoUrl']
    if url and quality:
      try:
        video_urls[quality]=url
      except:
        pass
        # ~ print 'got exception in mapping to "quality" for video ',vid
  if sys.argv[-1] in ['debug']: pprint.pprint(video_urls)
  if hq:
    if '720' in video_urls:      
      if verbose: 
        print '720p url:\n',video_urls['720']
      return video_urls['720'],vname.replace('.mp4','.720p.mp4'),vid
    elif '480' in video_urls:      
      if verbose: 
        print '480p url:\n',video_urls['480']
        if '720' in video_urls: print vname,' 720p url:\n',video_urls['720']
      return video_urls['480'],vname.replace('.mp4','.480p.mp4'),vid
    elif '360' in video_urls:
      if verbose: print vname,' no 480p url, 360p url:\n',video_urls['720']
      return video_urls['360'],vname.replace('.mp4','.360p.mp4'),vid
    elif '240' in video_urls:
      if verbose: print vname,' no 480p url, 240p url:\n',video_urls['240']
      return video_urls['240'],vname.replace('.mp4','.240p.mp4'),vid
  else:
    if '480' in video_urls:      
      if verbose: 
        print '480p url:\n',video_urls['480']
        if '720' in video_urls: print vname,' 720p url:\n',video_urls['720']
      return video_urls['480'],vname.replace('.mp4','.480p.mp4'),vid
    elif '360' in video_urls:
      if verbose: print vname,' no 480p url, 360p url:\n',video_urls['720']
      return video_urls['360'],vname.replace('.mp4','.360p.mp4'),vid
    elif '240' in video_urls:
      if verbose: print vname,' no 480p url, 240p url:\n',video_urls['240']
      return video_urls['240'],vname.replace('.mp4','.240p.mp4'),vid
    else:
      if verbose: print 'no lowq 240p url found for video'
    
def phub_find_previews(infile,verbose=True):
  a0=open(infile)
  b=a0.readlines();a0.close()
  videos_ids=b[0].split(',')
  videos_ids=[i.strip() for i in videos_ids if i]
  playlists=b[1].split(',')  
  playlists=[i.strip().replace('.htm','') for i in playlists if i]
  print 'trying to find ',len(videos_ids),' from ',len(playlists),' playlists'
  # ~ del b
  # ~ del a0
  d2={}
  with tqdm.tqdm(playlists,desc='building info dict') as pbar:
    for p in pbar:
      pl_url='https://pornhub.com/playlist/'+p
      cookies_load=' --load-cookies='+HOME+'/.python_scripts/cookies.txt '           
      cmd='wget -q -U "Mozilla/5.0 (X11; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0 (Chrome)" '+cookies_load+'  "'+pl_url+'" -O  - '
      plsource=os.popen(cmd).read()
      if len(plsource)<5000:
        print 'playlist ',pl_url,' too short ,probably error'
        continue
      soup=bs4.BeautifulSoup(plsource,bsp)
      image_elements=soup(class_='videoPreviewBg')
      for imelem in image_elements:
        im=imelem('img')[0]
        try:    d2[im['data-video-id']]=im['data-mediabook']
        except: continue
      if sys.argv[-1] in ['debug']:
        print d2.keys()[:10]
        print videos_ids[:10]
      pbar.desc='building info dict ('+str(len(d2))+'/'+str(len( set(d2.keys()).intersection(set(videos_ids)) ) )+') '
      time.sleep(1)
      if len(list(set(d2.keys()).intersection(set(videos_ids))))==len(videos_ids):
        print 'all videos found from currently decoded playlists!!'
        break
  # ~ print 'dict of video_id to previews has ',len(d2),'elements now'      
  
  a=open('links.txt','w');cnt=0
  for vid in videos_ids:
    if vid in d2:
      a.write(d2[vid]+'\n\tdir=htm/previews/'+vid[0]+'\n\tout='+vid+'.webm\n')
      cnt+=1
  print 'found ',cnt,' previews out of ',len(videos_ids)
  
def phub_find_playlists(vid,verbose=True):
  
  if os.path.exists(vid):
    if vid.endswith('.htm'):
      print 'processing ',vid,' as public playlists htm.getting playlists and names from this'
      soup=bs4.BeautifulSoup(open(vid).read(),bsp)
      x=soup('li',attrs={'id':re.compile('playlist_*')})
      infos=[]
      vids=[]
      for i in x:
        plname=i(class_='title')[0].text.strip()
        plurl=i('a',attrs={'class':'title'})[0]['href']
        infos.append((plurl,plname,0))        
    else:
      vids=[i.split(',')[0].strip() for i in open(vid).readlines() if not i.startswith('#') and 'view_video.php' in i]
    verbose=False
  else: vids=[vid]
  if vids: infos=[]
  with tqdm.tqdm(vids,desc='Getting pls ') as pbar:
    for vid in pbar:
      if vid.startswith('file:///'):vid=vid.replace('file:///','https://www.pornhub.com/')
      # ~ cookies_load=' --load-cookies='+HOME+'/.python_scripts/cookies.txt '
      cookies_load=''
      page_source=os.popen('wget -q -U "Mozilla/5.0 (X11; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0 (Chrome)" '+cookies_load+'  "'+vid+'" -O - ').read()
      soup=bs4.BeautifulSoup(page_source,bsp)
      
      try:
        x=soup('ul',attrs={'id':['videoPlayList','playlist-box']})[0]
        pls=x(class_='title')
        for pl in pls:
          pl_url=pl('a')
          if pl_url: 
            pl_url=pl_url[0]['href']
            if 'pornhub.com' not in pl_url: pl_url='https://pornhub.com'+pl_url
            pl_name=pl.text.strip()      
            vidcount='NaN'
            try:
              vidcount=pl.parent.parent(class_='thumbCount')
              if vidcount:
                vidcount=vidcount[0].text.strip().lower().replace('videos','').strip()
              else:
                vidcount=pl.parent.parent(class_='number')
                if vidcount:
                  vidcount=vidcount[0].text.strip().lower().replace('videos','').strip()
            except:
              if verbose: print str(traceback.print_exc())
              
            curinfo=(pl_url,pl_name,vidcount)
            if curinfo not in infos:infos.append(curinfo)        
        pbar.desc='Playlists ('+str(len(infos))+')'
      except:
        if verbose:
          print 'no playlists found for ',vid
          print str(traceback.print_exc())
    
  print 'found ',len(infos),' playlists containng these video(s)'
  pprint.pprint(infos)
  balready=[]
  if os.path.exists('phpl_ids'):
    balready=[i.strip() for i in open('phpl_ids').readlines()]
  if sys.argv[-1] in ['dwn','download']:
    # ~ print 'downloading playlist'
    # ~ a=open('pl.txt','w')
    for p in tqdm.tqdm(infos,desc='getting playlist'):
      playlist_id=os.path.split(p[0])[1]
      if playlist_id in balready: 
        print 'plname was pre-existing.continuing'
        continue
      pp=p[1]
      pp=re.sub('[^A-Za-z0-9_()-.#]+', '_', pp)
      plname=pp.replace(' ','_').replace('/','_')+'--'+os.path.split(p[0])[1]+'.htm'
      if type(plname)==unicode:    plname=plname.encode('ascii','ignore')
      if plname.startswith(('--','_')): plname='DUMMY'+plname
      plname=re.sub('[^A-Za-z0-9_()-.#]+', '_', plname)
      # ~ wrr=p[0]+'\n\tout='+plname+'\n'
      # ~ if type(wrr)==unicode:    wrr=wrr.encode('ascii','ignore')
      cookies_load=' --load-cookies='+HOME+'/.python_scripts/cookies.txt '     
      if os.path.exists(plname) and (float(os.stat(plname).st_size)/(1024.)>25): continue 
      cmd='wget -q -U "Mozilla/5.0 (X11; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0 (Chrome)" '+cookies_load+'  "'+p[0]+'" -O  "'+plname+'"'
      os.system(cmd)
      if float(os.stat(plname).st_size)/(1024.)<=25:
        print plname,' too short,probabaly failed.continuing'
        continue
      soup=bs4.BeautifulSoup(open(plname).read(),bsp)
      [i.decompose() for i in soup('script')];
      [i.parent.parent.parent.parent.parent.decompose() for i in soup(class_='privateOverlay')];
      footer=soup('section',attrs={'id':'footer'})
      if footer: footer[0].decompose();
      header=soup('header')
      if header: header[0].decompose() 
      image_elements=soup(class_='videoPreviewBg')
      aa=soup.new_tag('div')
      aa['class']='logo'
      soup(class_='rating')[0].insert_after(aa)
      for imelem in image_elements:
        im=imelem('img')[0]
        im['data-thumbs']='';
        im['data-thumb_url']=''
        im['data-path']=''
        im['data-mediumthumb']=''
        im['data-mediabook']=''
        imelem['data-related-url']=''      
      a=open(plname,'w');a.write(str(soup));a.close()

    
def mfgetsizefromfile(infile='',maxsize=None,uselist=False,sortbysize=False): #maxsize is max size to get
  b=[]  
  allinfo=''
  if not uselist:
    if not infile :
      if os.path.exists(sys.argv[-1]): infile=sys.argv[-1]
      elif not infile and os.path.exists('./l'): infile='./l'
      else: print 'no infile given';sys.exit(1)
    # ~ print 'getting sum of file sizes from ',infile
    b=open(infile).readlines()
  else:
    b=infile
  
  b0=[i.strip() for i in b if i.startswith(hosters)]
  b=[i.split('::')[2].strip() for i in b0]
  b2=[]
  for i in b:    
    i=i.strip().lower()
    ss=i
    # ~ ss=i.split()[0]
    if 'mb' in i:
      ss=ss.replace('mb','').strip();ss=float(ss) 
    elif 'gb' in i:
      ss=ss.replace('gb','').strip();ss=float(ss)*1024
    elif 'kb' in i:
      ss=ss.replace('kb','').strip();ss=float(ss)/1024.
    elif 'b' in i:
      ss=ss.replace('b','').strip();ss=float(ss)/(1024.*1024.)
    else:
      print 'neither gb nor mb not kb nro b in ',i,' .Exiting'
      sys.exit(1)
    # ~ ss=[float(i.split()[0].lower().replace('mb','')) ]
    b2.append(ss)
  if sortbysize:
    # ~ print 'here!'
    try: assert(len(b2)==len(b))
    except AssertionError:
      print 'could not get filesize for some log entries.exiting sort.no files written'
    b3=sorted(zip(b0,b2),key= lambda x:x[1])
    a=open('sorted','w')
    for i in b3: a.write(i[0]+'\n')
    a.close()
    print 'wrote %d sorted by size entries of logfile to ./sorted' %(len(b3))
    return
    
  if maxsize or maxsize==0:    
    cursum=0
    files=[]
    # ~ print 'b',b,'\nb2',b2,'\nb0',b0
    for j in range(len(b)):
      cursum+=b2[j]      
      if cursum>=maxsize:
        cursum-=b2[j]
        return files,cursum
      else: files.append(b0[j])
    return files,cursum
  else:
    totsum=sum(b2)  
    # ~ info5= 'sum of file sizes for file '+str(infile)
    info5= u'\u03A3fsize for '+str(infile)
    if totsum<1024: info5+=' : '+highlight('%d Mb' %(totsum))
    else: info5+=' : '+highlight('%.2f Gb' %(totsum/1024.))
    if totsum>0: info5+=' (%dl)' %(len(b0))
    # ~ print info5
    allinfo+=info5
    
          
    if infile.strip()=='cur' and os.path.exists('cur_dwn'):
      b3=[i.strip() for i in open('cur_dwn').readlines() if i.startswith(hosters)]
      if b3 and (b3[-1] in b0):
        # ~ print 'last line of cur_dwn in cur'
        brem=b0[b0.index(b3[-1])+1:]
        
        brem_sizes=[i.split('::')[2].strip() for i in brem]
        brem_sizes_float=[]
        if sys.argv[-1] in ['debug']:
          print 'brem_sizes: ',str(brem_sizes),len(brem_sizes)

        for i in brem_sizes:    
          i=i.strip().lower()
          # ~ except: print 'erro ion strip',i;sys.exit(1)
          ss=i
          # ~ ss=i.split()[0]
          if 'mb' in i:
            ss=ss.replace('mb','').strip();ss=float(ss) 
          elif 'gb' in i:
            ss=ss.replace('gb','').strip();ss=float(ss)*1024
          elif 'kb' in i:
            ss=ss.replace('kb','').strip();ss=float(ss)/(1024.*1024.)
          elif 'b' in i:
            ss=ss.replace('b','').strip();ss=float(ss)/(1024.*1024.*1024.)
          else:
            print 'neither gb nor mb not kb nor b in ',i,' .Exiting'
            sys.exit(1)
          
          brem_sizes_float.append(ss)
       
        totsum_rem=sum(brem_sizes_float)
        info3= 'remaining "cur" (after last of "cur_dwn") has size '+highlight('%.1f Gb' %(totsum_rem/1024.))
        if os.path.exists('encoded/'):
          encsize=os.popen('du --si -sh encoded/').read().split()[0].strip()
          info3+='. encoded/ has size '+highlight(encsize)
        # ~ print info3
        allinfo+='  ,  '+info3
       
        if sys.argv[-1] in ['del','printdel']:
          print 'writing "del" with entries to be deleted (till last of cur_dwn) and cur.2 with remaining entries'
          a2=open('cur.2','w');cnt=0
          for i in brem:
            a2.write(i+'\n')
            cnt+=1
          a2.close(); print 'cur.2 has ',cnt,' entries'
          
          bdel=b0[:b0.index(b3[-1])+1]
          a2=open('del','w');cnt=0
          for i in bdel: a2.write(i+'\n');cnt+=1
          a2.close(); print 'del has ',cnt,' entries'
      else:
        pass
        # ~ print 'last line of cur_dwn not found in cur'
    if systype in ['time4vps','cloudshell'] and infile.strip() in ['cur']:
      # ~ dirsize=os.popen('du  --si -s .').read().strip().split()[0].strip()
      # ~ print 'size of . is ',dirsize
      allinfo+=' , '+get_directory_size(start_path='.',verbose=False)[1]
      vid_files=[i for i in os.listdir('.') if i.endswith(video_extensions)]
      if vid_files: allinfo+=' with %d videos' %(len(vid_files))
  if type(allinfo)==unicode:    allinfo=allinfo.encode('ascii','replace')
  # ~ print unicode(allinfo)
  print allinfo

 
def herokutop():
  if systype not in ['hashbang']:    print 'this only works on hashbang'; return;
  num_heroku_accounts=len([i for i in os.listdir(HOME+'/.config/heroku/accounts') if i.startswith('h')])
  total_num_apps=len([i for i in os.listdir(HOME+'/work/h/') if os.path.isdir(HOME+'/work/h/'+i)])
  num_apps_per_account=5
  
  
  alllogsinfos=[]
  with tqdm.tqdm(total=total_num_apps,desc='gettings-h..') as pbar:
    for hka in range(num_heroku_accounts):
      os.system('heroku accounts:set h'+str(hka+1))
      for app_r in range(1,num_apps_per_account+1):
        app_num=(5*hka)+app_r
        if not os.path.exists(HOME+'/work/h/'+str(app_num)):continue
        os.chdir(HOME+'/work/h/'+str(app_num))
        loginfo=os.popen('heroku logs --dyno=worker -n 2').read()
        lines=loginfo.splitlines()
        memres=''
        if lines:
          l1=lines[0]
          if len(lines)>1:
            l2=lines[1]
            memres=[i for i in l2.split('#') if 'memory_rss=' in i]
            if memres:
              memres=memres[0]
              memres=memres.split('memory_rss=')[1].split()[0]
        if memres:
          alllogsinfos.append((app_num,memres))
        # ~ alllogsinfos.append((app_num,loginfo))
        pbar.set_description('h'+str(app_num)+'..');
        pbar.update(1)
        
  for i in alllogsinfos:
    print highlight('%.2d' %(i[0]),'green_fore')+' :: '+highlight(i[1],'green_fore_bright')
      
    
def print_help():
  help_dict={#should be a dict of option:(option_help,suboptions)
  '{url}':('supports dailymotion,xvideos,spankbang,okru.For sb 1https:// gives a 1-part aria download.For okru 2https:// uses hls .ts segments, and 1https:// downloads a sample (of about1.5min) instead of the whole video.For okru ,last arg of "ga" gives audio only,"gv" gives 144p video only,"sa" gives a sample only, "sampseg" lets the second last arg be number of samples(overriding default). ',''),
  'santabanta_maff <maff_file>':('process maff to find all santa banta pics (gallery and wallpaper).Written to links.txt',''),
  'parse_htm_get_vid_links/phgv/pgv <url/htm_file>':('gets all video links from url or local htm file',''),
  'update_git':('meant for alwaysdata.updates repo git(or tries to)',''),
  'watch_episodes_process/weps':('process series page of watch_episodes to generate result file in ./l which can be remote uploaded to verystream',''),
  'remote_upload_multiup/rum <infile> <multiup/mfire account number/sendspace_account/gdrive_account>':('uses multiup api to upload files from infile to multiup acc. infile should be like : all url------all names.Account can be number or "mf<account>"/"sn<account>".From 0-26 are mediaifre account.From 27- are gdrive.mediafire accounts greater than 26 MUST be referred as mf<account>,ditto for sendspace accounts.\n<infile> can be test/testupload in which case a small file(condor_testipload.mp4, 6 Mb) is uploader to the specified account to acheck whether multiup is working' ,''),
  'diffl <file-1> <file-2>':('removes part of file-2 from file-1.We assume file-1 "contains" file-2',''),
  'dupl <infile> ':('finds all duplicate entries in infile. assumes file in a log-file.if duplicates are found ,the unique file is written to ./l',''),
  'lwbplog <infile>':('checks all bp links in infile for validity.writes to ./l.\nAssumes <infile> is in logfile format',''),
  'ariadwn <sleepmin> <num-times>':('runs aria2c on linksfile.txt 7 times by default, with sleepmin of 0.3 minutes',''),  
  'autogui_clsh <account-number>':('uses chrome and pyautpgui to automate boosting cloudshell,given first tab is info file,second is gmail',''),  
  'autogui_mf':('uses firefox and pyautpgui to automate mf registration',''),  
  'jch/mfjchunk/mfjoinchunks <mf-logfile> {join}':('finds entries in logfile that are in split format (1.of.10 etc). and on server, downloads .Actually concatenates them on server if last arg is "join"',''),  
  'lwpg/lpg <infile>':('gets all porngo links for urls from infille. infile can be in logfile format also.creates .pglog',''),
  'hktop/hkt':('helper function to do a "top" like thing for heroku. only works on hashbang with herku accounts addon',''),
  'lwptrek/lptrex <infile>':('gets all porntrex links for urls from infille. infile can be in logfile format also.creates .ptrexlog',''),
  'lwpmaki/lwpornmaki <infile>':('gets all pornmaki links for urls from infille. infile can be in logfile format also.creates .pmklog',''),
  'lwptrekprev/lwptrekpreviews <infile>':('gets all porntrex previews for urls from infile and makes montages out of them. Output is in webp format (360p)',''),
  'lwvslog <infile>':('checks all vs links in infile for validity.writes to ./l.\nAssumes <infile> is in logfile format',''),
  'csh_spawn <start_gsh_account-end_gsh_account>':('when run on gsh://500 will spawn shells with range from sys.argv[-1]. \nwhen run on hashbang, periodically polls the "auto_boost" file on server. When recent change is detected to that file it starts the corresponding gsh:// in a new screen window',''),
  'watchepisodes/weps <infile>':('gets verystraeam links of all episodes for series from <infile>.writes to ./l. If no verystraeam ,then tries poenload and writes to l.2. There is a serial version (weps_serial)',''),
  'lwvslog_remote_upload <infile>':('creates linksfile.txt for remote upload for  all  vs links in logfile\nAssumes <infile> is in logfile format',''),
  'ruvs <infile> <account>':('remote uploads links to verystream account vs://<account>',''),
  'mgl <account>':('lest mega account',''),
  'upmega/lumega filename <account>':('upload filename to  mega account',''),
  'downmega <mega-log>':('download each entry from mega logfile',''),
  'db_download_all_files <account>':('writes all links from db://X to linksfile.txt',''),
  'vmheartbeat':('check dvshel heartbeart service',''),
  'clvs <infile>':('checks and renames verystream uploads from <infile>',''),
  'selenium_csh_boost <account>' :('tries to enable boost mode for <account> via selenium+chromeriver.installs packages if not available previously. Will use profile folder in work/profile/test{account} by default unless user gives "dont_use_profile_folder" in sys.argv',''),
  'lwvslog_remote_upload/lwvs2/lvs2 <infile>':('prepares linksfile.txt for remote upload purpose from verysteam logfile',''),
  'mincpu/mincpuusage':('sleeps till cpu-usage stays below 3% for 8 consecutive iterations. sleeps 2sec between iterations',''),  
  'serakon_get_pages <infile>':('gets serakon htm files with images for all urls in <infile>',''),
  'lwbp <infile>':('finds download links for all url in <infile> of bitporno.<infile> should be direct urls, not logfile format. if "wr" at end, then will create logfile .bplog',''),
  'lvs_webpage <infile>':('finds verystream links for all webpage url in <infile>.writes to ./l',''),
  'lwvs/lvs <infile>':('finds download links for all url in <infile> of verystream.<infile> should be direct urls, not logfile format. if "wr" at end, then will create logfile .vslog',''),
  'lwgoun/lgoun <infile>':('finds download links for all url in <infile> of gounlimited.<infile> should be direct urls, not logfile format. if "wr" at end, then will create logfile .gounlog',''),
  'lwsera <infile>':('finds  bitporno urls for all htm links in <infile> .<infile> usually will have serakon links',''),
  'vsex_get_pornstars <infile>':('finds  vsex.in pornstars for all htm links in <infile> .<infile> usually will have vsex.in links',''),
  'lwserapreviews/lwseraprev <infile>':('finds  and downloads serakon previews for all htm links in <infile> .<infile> usually will have serakon links. the webp_quality can be set by the "preview_webp_quality" key in .crop',''),
  'lwsblog/spankbang_logfile_get_direct_links':('get direct aria file from sbang log file (.sblog)',''),
  'check_links_multiup/clm <infile>':('infile should be from remote_upload_multiup. Writes a check_links.txt.2 which has only links that are ongoing.In case they were auto-submitted (by checking cloudconvert status) it also updates cloudconvert_log.pickle(when saving is succuessful).Take care not to use this along with checking cloudconvert process status or cloudconvert minutes as it may cause race condition and result in invalid cloudconvert_log.poickle',''),
  'get_user_xhamster <infile/xhamster-video-url>':('finds xhamster user for all urls (or single one) from infile. Only finds users with at least 20 videos',''),
  'ypp_get_user <yespornplease-video-url> {pr/proxy}':('finds yespornplease user from yespornplease-video-url. "pr" at end means proxy is uses. ',''),
  'toggle_gnome_shell_extension <extension-name>':('helper to toggle on/off extensions (like netspeed)',''),
  'xhamster_join_uservideos <infile>':('joins multiple htm files from xhamster users into a big single file',''),
  'get_filesize_of_url/gfsz <infile/url>':('Finds files of all links from infile (or single url) and writes to fsizes.txt',''),
  'clshell_cmd <cmd> <interval>':('meant to keep clous hell operational. Runs cmd every interval minutes.Default cmd is du.Interval is 2.5 min',''),
  'livescore {match-id} <update-interval>':('uses cricbuzz to get livescore for match of certain id. If no id given ,then chooses first valid match. defauly interval is 30 seconds',''),
  'transload <url> {fname}':('transloads url (with optional name) to gd:/22 or sl://34 via dropbox. Mainly usefujl for i.p. restricted things like openload',''),
  'tunnelbear_connect/tbear ':('helper to open vpn connection to location via openvpn . user tunnelbear or windscribe as needed','location'),
  'alwaysdata_get_xhamster_usercontents <infile>':('gets htm files of all users mentioned in infile.infile should be from get_user_xhamster() ',''),
  'stendoco ':('useless stub for encoding.com',''),
  'change_desktop_wallpaper':('helper for chaning wallpaper',''),
  'openload_upload_to_my_account <valid_links>':('upload all openload links from valid_links to openload accounts.File should be in format produced by xxxstreams_process_urls(). Cycles as needed (to avoid limit of 100 per day)',''),
  'correlate_previews_mf/correlate_logs/corr <vlist> <mflogfile>':('correlates files from <vlist> to <mflogfile>. writes ./l ',''),
  'corrdiff/cdiff/cdff <f1> <f1>':('correlates files from <vlist> to <mflogfile>. writes ./l. removes entries from ./l from f1.copies diff back to f1 ',''),
  'corr_sb/correlate_spankbang_urls <url-file> <sblogfile>':('correlates files from <url-file> to <sblogfile>. using their UIDS. writes to ./l ',''),
  'xxxstreams_process_urls <infile>':('checks all xxxstream.org urls from infile and writes valid ones( with working openload) to valid_links',''),
  'get_users_sb <infile>':('Finds spankbamng users of all urls from infile and writes to sbusers.txt',''),
  'xxxstream_get_pages <start-end-y/yp>':('gets all xxxstreams.org pages from range,"y" means also get base images."yp" gets preview images too.','spacial-tag-page'),
  'xxxstream_get_previews <infile> ':('gets xxxstreams.org preview images for all urls from infile.pr means use proxy,gf getfull (full preview not thumbsized).vb is for verbose','pr-gf-vb'),
  'ol_get_direct_links/old <infile>':('gets direct links for openload files from infile.Similar to sld/mfd/gdd. Uses youtube-dl',''),
  'bp_get_direct_links/bpd <infile>':('gets direct links for bitporno files from infile.Similar to sld/mfd/gdd/old. ',''),
  'ol/ol_listfiles <accounts>':('lists all openload files from <account>',''),
  'ol_purgefiles/olp <delfile>':('deletes all files from delfile',''),
  'db_listfiles <account>':('lists all dropbox files from <account>',''),
  'db_upload_file <infile> <account>':('uploads local file infile to db://account',''),
  'db_empty <account>':('empties db://account (permanent delete)',''),
  'update_proxies_list':('updates proxies list in ~/. Needed for some other functions (mainly cloud cnvert)',''),
  'phub_find_playlists/phpl <video-url>':('Finds playlists menioned in <video-url> for phub',''),
  'phub_make_montages <infile>':('Makes montages for all phub videos from <infile>. <infile> is created on local machine via ./phub.py anim',''),
  'phub_get_video_url <phub-video-url>':('Gets direct link and video name for phub-video-url',''),
  'transload_dropbox_to_mediafire <infile> <account>':('Transloads infile to mf://account via dropbox (random one from all dropboxes). Is faster than direct mf upload',''),
  'transload_to_openload <infile> <openload-account>':('Transloads all entries from infile to ol://<openload-account> by first downloading in /tmp/ .works on cloudshell ideally',''),
  'hpk_get_anim':('for homepornking',''),  
  'check_links_solidfiles/clso <infile> {arch/arch2}':('checks whether links from <infile> (usually check_links_solidfiles.txt) have finished uploading to solidfiles. Mentioning {arch/arch2} is necessary to rename files corrently in arch accounts',''),
  'cleanup_pickle_log':('removes redundant entries (error) from logfile (cloudconvert) to slim down log',''),
  'economist_dwn <print-edition-url>':('meant for server(ad).Downloads all articles and images of linked print edition url',''),
  'mediafire_acstatus/mfac':('gets status for all accounts',''),
  'update_git':('helper fn',''),
  'scheduler':('meant for serve.Runs a loop on ~/current.sh and writes to ~/scheduler_log',''),
  'remove_process_url_from_pickle/rpurl <process-url>':('removes process url from .cloudconvert_log.pickle',''),
  'move_to_dirs':('should be run in dir having .vlist,with first few lines as ##<subfolder> <numerical_id>, and files as "<filename>,<numerical_id>".It will copy files to that subfolder',''),
  'image_maff <.maff file>':('takes in a maff file with imagefap galleries inside,writes out corresposing plain html gallery pages.If last arg is "html/htm" then uses .htm files instead of maff. If sahtm gets sample(first 15) images from htm files. If its sa does sampe for maff ','htm/html,sahtm,sample/sa'),
  'mkpreview_video <infile>':('makes a preview video from video file using ffmpeg and mediainfo',''),
  'mediafire_purgefile/medp <filename>':('purges all files from mediafire with quickeys from <filename>',''),
  'mediafire_get_direct_link':('get direct link of mediafire file using phantomjs and selenium',''),
  'mediafire_queryfiles/medq':('queries mediafire and print all files (and their one-time download links) in root folder. nariman009121@gmail.com account is default.',''),
  'cloudconvert/clc <account_number> <ffmpeg_preset> <url> {video-name}':('Cloudconvert handler. account_number is in [0,1,2] for [firefox-esr,firefox,google-chrome] ,ffmpeg_preset is 180p,240p,270p,360p,pn(144p crf-28),pn1(180p crf-29). Url can be http link,spankbang , or yespornplease. For yespornplease video-name is a necessary last option. If video name has a extension (test.mp4 instead of just test) then the extension is used as the inputformat. Outputformat is always .mkv ',''),
  'findserver/fse <searchstring>':('reads ~/Videos/otherStuff/downloading/cloudconvert/others/0servers_files and does a pattern search on it.',''),
  'test':('random code testing.Dont use.changes regularly',''),
  'testconvert <infile>':('encodes <infile> with variety of hevc presets,crf, and size factors to check bitrate and encoding time.Defsault file is "sneaky.pete.s01e02.480p.mkv"',''),
  'clcf/clf/cloudconvert_linksfile <linksfile> <ffmpeg_preset>':('Reads all links in <linksfile> and tries to submit to available cloudconvert servers. linksfile can contain file-specific options(including ffpreset) that override defaults for that file.Format is similar to aria2 linksfile ',''),
  'cloudconvert/clc status/st/status_finished/stfin':('Cloudconvert handler for printing status of running/finished processes only."ac/acinfo" quieries all serivers using a dummy process.acprev returns status of previous query from logfile.for stfin if last option is "se" search is performed on all filenames with searchterm as sys.argv[2-].Eg .vd clc stfin trek se with search for trek among all completed files','<ac/acinfo,acprev> {se/sh/search}'),
  'scheduler/sch <number-of-loops>':('meant for pythonanywhere.com, does the whole loop of checking past processes,querying account for minutes, and submitting jobs (from current.sh)',''),
  'cl {url}':('delete cache file and redownload it,in case download url has changed',''),
  'li {listfile}':('download all urls from {listfile}',''),
   'licl {listfile}':('clear and redownload all urls from {listfile}',''),
    'sm {unique-vid-id}':('check metadata.pickle and print info for video with that id,if known',''),
    'vd transload_gdrive_to_openload <infile> <transload-gd-account> <openload-acccount>':('transload_gdrive_to_openload via gd://4 to ol://21 on remote system(defaults).\naccounts must be like gd4 and ol21',''),
    'split {2/3} {links-file}':('splits {links-file} into chunks, names them .1, .2 etc. If it is given as x-<n> then the source file is overwritten with chunk of index "x" of "n"',''),
    'split2/splits {2/3} {links-file}':('splits {links-file} into "equal-sized" chunks. If it is given as x-<n> then the source file is overwritten with chunk of index "x" of "n" ',''),
    'process_vids_metadata':('processes all sb htm in ~/.python_scripts/vids_metadata to update metadata.pickle in ~/.python_scripts',''),
    'js,j <3gp,3g,vp9,3gr,mkl,mkj>': ('creates a .jsonlist file that has info from all jsons. With 3g/3gp or vp9 prints jsons which have only 3gp/vp9.With mkl creates ytcr with youtube links for all jsons.With mkj creates a bash script dl with jsons downloads loaded.',''),
    'fz,fzextr {filename} {http_down_path} ':('to get paths from fztvseries, first option is file ,second is full http base path.If links are f type hdcine, second option may be omitted, but first line of file should be "hdcine".output in fzlinks ot hdcinelinks as appropriate',''),
    'revl {file}': ('reverses links in {file}',''),
    'bash_hist_fix':('create resized ~/.bash_history with cp,mv etc deleted',''),
    'dir_vid_meta':('uses .vlist in curdir and creates a .vidinfo file with all infos from metadata pickle,for suboption,only print user info','useronly'),
    'timesortlist/timesort <listfile>':('sorts listfile by time and writes to .vlist_timesorted',''),
    'prenamer {orig_vlist} {changed_vlist}':('changes names in directory to match using unique vid ids',''),
    'change_screen_brightness <increase/decrease> <amount>':('increase/decrease screen brigtness by amount (subject to max of 254,min of 1)',''),
    'process_spankbang_deleted_zip':('processing ./spankbang.deleted.csv and writing deleted videos to ~/.python_scripts/deleted_allvideos.pickle',''),
    'remove_spankbang_deleted_previews':('removing previews found in .vlist that have been deleted.writing delete_previews.sh',''),
    'symlink_first_image_in_directory':('create symlinks to first image.Meant for large bpg galleries.Needs a .vlist to exist with all images.',''),
    'ra,readaria':('reads given .aria2 file to get total file size,else reads all files in curdir','<aria-file>'),
    'ti,timer':('prints a timer(space for pause,backspace for reset)',''),
    'pdj,process_dailymotion_jsons':('If option given, then wgets the requied jsons, elsefinds all videos in jsons in curdir and writes them to dailymotion_videos_list.json in ~/Videos/Series/lists/','<wget_jsons username>'),
    'sd,search_dailymotion_vids,sdv {searchstring}':('search the dailymotion_videos.json for videos that satisfy conditions in {searchstring}',''),
    'ytpage/ytp <filename>':('process file(should be plain htm of youtube user page) to write links to ytlinks',''),
    'uw {url}':('download {url} using wget',''),
    'mark_file <filename> <numerical_index>':('marks(check meaning) <filename> based on <numerical_index>.Meant to be used with mpv keyboard presses.',''),
    'merge_subs':('merges subs for all .mp4 files in curdir.Requires every .mp4 file to have a mathing .srt file',''),
    'sort_mp4_by_length <remove_num>':('reads all mp4 in current dir.Depending on options will either put number sort,or remove it from filenames','remove_num/shortest_first'),
    'mklistfrompreviews <preview_dir> <linksfile>':('uses all preview vids from <dir> to find matching links in <linksfile>, wirte to ../reconstructed_links',''),
    'economist_dwn <print-edition-url>':('downloads full edition for zipping',''),
    'wget_put <infile>':("uses bw to get file,put to mf and remove it",''),
    'olu/Openload_Upload_file_direct <infile> <openload_account>':("uploads infile to ol://<openload-account> default account is 21",''),
    'fillaccounts(_dummy) <linksfile> <gd/solidfiles/slarch/slarch2>':('linksfile should be formatted by lwsb or lwh or lwyt.\n Fils out gdrive accounts cache(upoto max_temp_gdrive) for large files and solidfi accounts for small files(less than 500 MB). Default is mediafire tmp accounts.Can use solidfiles (and slarch/slarch2) or google drive.Dummy version will not actually submit files,so it good for testing',''),
    'lwypp <ypplinks>':('gets vshare video links for ypp videos.If last arg is "s" then uses serial mode else parallel(default). In paralllel mode if last arg is a number ,then that is number of processses/links used, then processes in parallel mode.Default parallel processes are 85 (since its craps out aftert 1 query usually)','s/<number>'),
    'lwvshr <ypplinks>':('gets direct video links for vshare videos ( input should be from lwypp)','s/<number>'),
    'lwxh <xhamster_videos_file>':('gets xhamster links  in parallel mode with 25 processes by default',''),
    'lwh/lwh2 <file>':('takes a plaintext links file and creates a valid input  for remote_upload for multiup.lwh2 reverses this,i.e. input is a file with separator (------) and output is file in aria format',''),
    'lwsb <sblinks_file>':('creates linksfile.txt (in multiup format) from sblinks. Does in serial mode if number of links if less than 50 and parallel mode otherwise. If last arg is number it is used for number of parallel processes (by  default are 4) ','<num_parallel_processes>'),
    'lwxv <xvideos_links_file>':('creates linksfile.txt (in multiup format) from xvlinks. Does in serial mode if number of links if less than 50 and parallel mode otherwise. If last arg is number it is used for number of parallel processes (by  default are 4) ','<num_parallel_processes>'),
    'lw <infile>':('gets valid links (from usable sites) from infile and writes results to linksfile.txt (in aria2c format).Use cache',''),
    'lwr/liclwr/licl <infile>':('gets valid links (from usable sites) from infile and writes results to linksfile.txt (in aria2c format).Clears cache before getting links',''),
    'gdl/gd <account>':('lists all gdrive files.If last arg is "a" then lists all gdrive upto max_temp_gdrive as well as sendspace accounts','a'),
    'gdp <delfile>':('deletes files from gdrive and sendspace accounts.<delfile> should be in valid format (produces by gdl or snl',''),
    'gddl <gdfile>':('gets direct download links (in aria2c) format for gdrive files .Files should be in valid format',''),
    'sn <account>':('list sendspace files for account',''),
    'snp <delfile>':('deletes sendspace files. Delfile should be in proper format(produced by snl)',''),
    'mf {searchterm} <account>':('lists mediafire files of account (video files only).Account can be number or "mf<number>".Searchterm can be like <a>_<b> where both a and b are regexp matched. .mfchunkskip (skip after 1st to this number) and .mfchunkmax (stop at this max chunk) can be used to control behaviour',''),
    'mfa <account>':('lists mediafire files of account(all files). .mfchunkskip (skip after 1st to this number) and .mfchunkmax (stop at this max chunk) can be used to control behaviour',''),
    'mediafire_renamefiles/medrf/mfr <account>':('renames mediafire files of account(all files)',''),
    'verystream_renamefiles/vsrename/vsr <account>':('renames verystream files of account(all files)',''),
    'mediafire_exhaustive_listing/mfle <account>':('exhaustive list of  mediafire files of account(all files including in subfolder)',''),
    'mediafire_acstatus/mfac <account>':('gets used/remaining in  mediafire accounts',''),
    'mfp <delfile>':('sends files from delfile to mediafire trash. Trash needs to be emptied manually',''),
    'mft <account>':('shows mediafire trash for account',''),
    'sl/sla  <account>':('prints list of files in solidfiles account. sla prints all (not just video).arch/arch2 using archival(2) accounts instead of base','{arch/arch2}'),
    'sld  <infile>':('gets download urls (in aria linksfile.txt format) for all solidfiles files from <infile> ',''),
    'slqueryall/slqa <account-range>':('prints list of files in all solidfiles account in account range.If range is notgiven does for all.arch/arch2 using archival(2) accounts instead of base','{arch/arch2}'),
    'mffa {searchterm} <account>':('same behaviour as "mf" but lists the download links as well.Searchterm can be like <a>_<b> where both a and b are regexp matched',''),
    'remote_upload_solidfiles/rus <infile> <solidfiles-account>':('remote upload links from <infile> to solidfiles-account. infile should be in proper format(urls+sepator+names). arch/arch2 version uses archival acounts','{arch/arch2}'),
    'local_upload_mediafire/lum/lumnoverbose  <infile> <mf-account>':('directly uploads infile to mf-account.  noverbose version is silent ',''),
    'local_upload_mediafire_python_noverbose/lum2/local_upload_mediafire_python  <infile> <mf-account>':('directly uploads infile to mf-account using python api.  noverbose version is silent ',''),
    'mfd <mffile>':('writes download links of all mf-files from <mffile>.Starts  aria on local machine',''),
    'medl <id> <account>':('gets direct link for mediafire file with id <id> from account <account>',''),
    'mediafire_list_temp_storage_accounts/mftemp <account-range>':('lists all mediafire files from accounts from range <account-range> range is in format start_account-end_account(-spacing) ,e.g. 10-20-2 for 11-15',''),
    'pf/purge/purgefiles <delfile>':('generically deletes files from infiles ,using first line of infile to chekc whether do use deleter for mediafire/google drive/solidfiles',''),
    'gend/generic_download_logfile <infile>':('generically gets downloads links of logfile entries from <infile>. Works serially. Works for '+str(hosters),''),
    'mediafire_get_space_remaining/mfspace <account>':('prints space remaining in mf://account in gb',''),
    'fsen/fsen2 <search_term>':('tries to find where the searchterm occured in others/0servers_files. useful to get which servers have the serie. fsen is for series, fsen2 for movies',''),
    'mfgetsizefromfile/gsff <infile>':('gets sum of sizes of all files mentioned in <infile> . If infile is "cur" and "cur_dwn" is present,then will give remaining space apart from cur_dwn as well. If last arg is "sortsize", it will write ./sorted with log-entries ffrom infile sorted by filesize(ascending)',''),
    'remove_process_url_from_pickle/rpurl <process_url>':('removes <process_url> from cloudconvert_log.pickle. Useful in case of damaged file or other errors',''),
    'wget_put <infile> ':('uses mediafire-shell to upload all files/links from infile to specified medifire account. if last arg is "d" then it download from url from infile/else assumes entry is local file','{mf-account},{d}'),
    'get_filesize_of_url/gfsz <url/file>':(' if url/file exists in curdir then gets all sizes of direct links and write to fsizes.txt . Else treats it as url and print size',''),
    '':('',''),
    'get_lowest_hls <.info.json file>':('uses youtube-dl to get lowest stream from hls,for use with ok.ru mainly',''),
    'replicate_symlink <pathfile> ':('replicate directory and file structure mentioned in <pathfile> ,default root_path_name is /media/ani/TRANSCEND/Videos/','root_path_name,only_first_image')
  };
  hk=help_dict.keys();hk.sort()
  for h in hk:
    hstr=help_dict[h][0]
    subopts=help_dict[h][1].strip()
    hs='  '+colorama.Fore.BLUE+h+'  '
    if subopts:	hs+=colorama.Fore.CYAN+subopts+'  '
    hs+=colorama.Fore.RESET+hstr
    print hs


  
    
def generate_random_words(consonants_only=False,length=6,count=5):
  
  import string
  VOWELS = "aeiou"
  CONSONANTS = "".join(set(string.ascii_lowercase) - set(VOWELS))
  NUMBERS='0123456789'
  def generate_word(length):
    word = ""
    for i in range(length):
      if i % 2 == 0:
        if i>0 and not consonants_only:
          word += random.choice(CONSONANTS+NUMBERS)
        else:
          word += random.choice(CONSONANTS)
      else:            word += random.choice(VOWELS)
    return word
  
  for i in range(count):
    wlen=random.choice(range(length-1,length+2))
    print generate_word(wlen)
  
# ~ def ranks(sample):
    # ~ """
    # ~ Return the ranks of each element in an integer sample.
    # ~ """
    # ~ indices = sorted(range(len(sample)), key=lambda i: sample[i])
    # ~ return sorted(indices, key=lambda i: indices[i])

# ~ def sample_with_minimum_distance(n=40, k=4, d=10):
    # ~ """
    # ~ Sample of k elements from range(n), with a minimum distance d.
    # ~ """
    # ~ sample = random.sample(range(n-(k-1)*(d-1)), k)
    # ~ sample1=[s + (d-1)*r for s, r in zip(sample, ranks(sample))]
    # ~ return sorted(sample1)
    
def parse_htm_get_vid_links(url,isfile=False,verbose=True,writelog=False):
  if sys.argv[-1] in ['v','verbose']:very_verbose=True
  else:very_verbose=False
  
  if isfile:
    s=open(url).readlines()  
    urls=[i.strip() for i in s if i.strip() and i.startswith(('http://','https://'))]
    if verbose: print 'trying to get all links for ',len(urls),' urls'
  else:
    urls=[url]
  
  if verbose and writelog:alog=open('links.txt','w')
  all_links=[]
  for url in urls:
    s=requests.get(url).content
    soup=bs4.BeautifulSoup(s,bsp)
    links=soup('a')    
    num_mov_links=0    
    url_base=os.path.split(url)[0]
    for link in links:
      if link.has_attr('href'):
        uu=link['href'].strip()
        if url_base not in uu:
          uu=url_base+'/'+uu
        if os.path.splitext(uu)[1] in ['.avi','.mp4','.mkv','.3gp']: #check for video
          all_links.append(uu)
          if very_verbose: print uu
          if verbose:
            if writelog:alog.write(uu+'\n')
            else: print uu
            num_mov_links+=1
    
    if verbose: print '## found '+str(num_mov_links)+' video links '
  if verbose and writelog:alog.close()
  return all_links[0]
  
  
def merge_subs(wdir='.'):
  a=os.listdir(wdir)
  a=[i for i in a if i.endswith('.mp4') or i.endswith('.mkv')];a.sort()
  for i in a:
    (ibase,ext)=os.path.splitext(i)
    ext=ext.strip()
    #~ print ibase,ext
    if os.path.exists(ibase+'.srt'):
      x=os.stat(i);
      if ext=='.mp4:' and os.path.exists(ibase+'.srt'):
        cmd='ffmpeg -i "'+i+'" -i "'+ibase+'.srt" -c:a copy -c:v copy -c:s mov_text t.mp4 -y&&mv -vf t.mp4 "'+ibase+'.mp4"'
        print cmd
        os.system(cmd)
      elif ext=='.mkv' and os.path.exists(ibase+'.srt'):
        cmd='ffmpeg -i "'+i+'" -i "'+ibase+'.srt" -c:a copy -c:v copy -c:s srt t.mkv -y&&mv -vf t.mkv "'+i+'"'
        print cmd
        os.system(cmd)
      os.utime(i,(x.st_atime,x.st_mtime))


def process_spankbang_deleted_zip(write_pickle=False):
  #to get zip file
  #~ a2 'https://spankbang.com/static_desktop/CSV/spankbang.deleted.zip'
  if not IS_LOCAL_MACHINE and not os.path.exists('spankbang.deleted.csv'):
    os.system('wget https://spankbang.com/static_desktop/CSV/spankbang.deleted.zip && unzip spankbang.deleted.zip')
  import csv
  if not os.path.exists('spankbang.deleted.csv'):
    print 'no file spankbang.deleted.csv found.Exiting.'
    sys.exit(1)
  dele=[]
  with open('spankbang.deleted.csv') as csvfile:
    cr=csv.reader(csvfile)
    for row in cr:
      dele.append(row)

  dele=set([i[0].split('/')[3] for i in dele])
  
  if write_pickle:
    a=open(HOME+'.python_scripts/deleted_allvideos.pickle','w')
    cPickle.dump(dele,a,protocol=2);a.close()
    print 'wrote ids to ~/.python_scripts/deleted_allvideos.pickle'
  dele=list(dele);dele.sort()
  if os.path.exists('spankbang.deleted.ids.txt'):
    print 'processing from old ids.txt file'
    dele2=open('spankbang.deleted.ids.txt','r').read().strip().split(',')
    dele2=[i.strip() for i in dele2 if i.strip()]
    newids=set(dele)-set(dele2)
    newids=list(newids);newids.sort()
    print 'new ids are of len ',len(newids),' instead of original ',len(dele),' or old ',len(dele2)
    dele=newids
  
  a=open('spankbang.deleted.ids.txt','w')
  for i in dele: a.write(i+',')
  a.close()
  print 'wrote ids to ./spankbang.deleled.ids.txt'


def remove_spankbang_deleted_previews():
  if not os.path.exists('.vlist'):
    print 'no .vlist found.exiting'
    sys.exit(1)
  print 'assuming .vlist contains all preview videos'
  b=open('.vlist').readlines()
  ids=[os.path.split(i)[1].rsplit('-')[-1].strip().split()[0].replace('.mp4','') for i in b]
  dele=cPickle.load(open(HOME+'.python_scripts/deleted_allvideos.pickle'))
  ids_deleted=dele.intersection(set(ids))
  if len(ids)==0:
    print 'no videos seem to be deleted.exiting'
    sys.exit(1)
  else:
    a2=open('delete_previews.sh','w')
    a2.write('#!/bin/bash\n')
    deleted_videos_cnt=0
    for i in b:
      idx=os.path.split(i)[1].rsplit('-')[-1].strip().split()[0].replace('.mp4','')
      if idx in ids_deleted:
        a2.write('rm "'+i.strip()+'"\n')
        deleted_videos_cnt+=1
    a2.close()
    if deleted_videos_cnt>0:
      print deleted_videos_cnt,' previews have already been deleted'
    else:
      print 'no deleted videos found.'

# ~ def checkProxy(proxy,timeout=0.25):
  # ~ if isinstance(proxy,tuple):    proxy,timeout=proxy
  
  
def devshell_heartbeat(showbasic=False):
  if sys.argv[-1] in ['basic']: showbasic=True
  url='https://ssh.cloud.google.com/devshell/vmheartbeat'
  external_ip = os.environ['DEVSHELL_IP_ADDRESS']
  x=requests.post(url, headers={"Devshell-Vm-Ip-Address": external_ip});
  if not showbasic: print highlight('RESPONSE CONTENT: '+x.content)
  if x.status_code in [200]: print highlight('STATUS CODE: '+str(x.status_code),'green')
  else: print highlight('STATUS CODE: '+str(x.status_code),'red')
  if not showbasic: 
    print highlight('HEADERS: ')
    for i in x.headers:
      print i,' : ',x.headers[i]
  if showbasic: #this is run by .screenrc. Check ffmpeg segfault as well
    pass
    # ~ x=os.popen3(ffmpeg_cmd_name);
    # ~ x1=x[1].read();x2=x[2].read()
    # ~ if 'man ffmpeg'  not in x2:     
      # ~ print highlight('some problem with ffmpeg.Running "uenc" to be safe!!' , 'yellow')
      # ~ os.system('cd ~/.python_scripts/bin&&rm -fv jumpli kmer gmoh ffmpeg&& wget https://raw.githubusercontent.com/grill02/dotf2/master/jumpli -O '+ffmpeg_cmd_name+' &&chmod a+x '+ffmpeg_cmd_name+' && ls -l  '+ffmpeg_cmd_name+' &&cd ~/work/')
    
  

def urllib_save_webp_previews(pageurl):
  try:
    pageurl,fast_proxy,getfull,verbose=pageurl
    content=url_get_content_simple((pageurl,fast_proxy)) 
    
    if not content :
      if verbose: print 'empty content for page ',pagenum,'. COntinuing'
      return
    import bs4
    soup=bs4.BeautifulSoup(content,bsp)
    images=soup('div',attrs={'class':'entry-content'})[0]('img')
  
    page_image=[i['src'] for i in images if i.has_attr('alt') and ('preview' not in i['alt'].lower()) and not i['src'].endswith('.th.jpg')]
    if len(page_image)>1 and verbose:    print 'more than 1 non-preview image for ',pageurl,' selecting 1st'
    if not page_image:
      if verbose: print 'could not find page basic image for ',pageurl,' all images:',str(images)
      return
    page_image=page_image[0]
    
    preview=[i['src'] for i in images if i.has_attr('alt') and ( ('preview' in i['alt'].lower()) or (i['src'].endswith('.th.jpg')) ) ]
    
    if len(preview)>1 and verbose:
      print 'more than 1 preview image for ',pageurl,' selecting 1st, all images:',str(images)
    if not preview:
      if verbose: print 'could not find any preview for ',pageurl,' all images:',str(images)
      return
    preview=preview[0]
    
    if getfull and 'imgcloud.pw/' in preview:
      try:
        content2=url_get_content_simple((preview,False))
        soup=bs4.BeautifulSoup(content2,bsp)
        preview0=soup(class_='image-viewer-main')
        if preview0:preview0=preview0[0]('img')
        else:
          if verbose: print 'unavble to ifind full preview image for ',pageurl
        if preview0:preview=preview0[0]['src']
        else:
          if verbose: print 'unavble to ifind full preview image for ',pageurl
      except:
        if verbose:
          print 'could not get full preview for ',pageurl
          print str(traceback.print_exc())
        pass
      
    from PIL import Image
    import urllib2
    
    imname=os.path.splitext(os.path.split(page_image)[1])[0]
    ims=preview
    
    opener = urllib2.build_opener();
    opener.addheaders = [('User-Agent', 'Mozilla/5.0')] 
    # ~ try:pim=Image.open(urllib2.urlopen(ims,timeout=timeout))
    pim=Image.open(opener.open(ims,timeout=10))
    nw=int(float(pim.size[0])/2.)
    nh=int(float(pim.size[1])/2.)   
    new_imloc='htm/pages/previews/'+imname+'.webp'    
    if getfull:
      pim = pim.resize((nw,nh), Image.ANTIALIAS)
      pim.save(new_imloc,quality=45,method=6)
    else:
      pim.save(new_imloc,method=6)
   
  except:
    if verbose: print 'execution for parallel save preview failed  for ',pageurl
    print str(traceback.print_exc())
    pass

def xxxstream_get_previews(urlfile='',proxy=False,getfull=False,verbose=True):
  fast_proxy=proxy
  if proxy:
    print 'using proxy'
    fast_proxy=getRandomProxy(max_timeout=0.1,max_tries=30)
  
  pageurls=[i.strip() for i in open(urlfile).readlines() if i.startswith(('http://xxxstreams.org/','https://xxxstreams.org/'))]
  os.system('mkdir -p htm/pages/previews')  
  inp=[(i,fast_proxy,getfull,verbose) for i in pageurls]
  x=imap_bar(urllib_save_webp_previews,inp,7,True)
    
def xxxstream_get_pages(base='http://xxxstreams.org/page/',start=1250,end=3000,timeout=8,proxy=False,save_images=False,get_previews=False,debug=False):
  try: 
    import PIL
  except ImportError:
    print 'PIL was not installed. installing now'
    os.system('sudo pip2 install Pillow')
  print 'getting pages in range from ',start,' to ',end
  fast_proxy=proxy
  if proxy:
    print 'using proxy'
    fast_proxy=getRandomProxy(max_timeout=0.1,max_tries=30)
  # ~ from PIL import Image
  # ~ import urllib2
  pagedir=''
  if '/category' in base or '/tag' in base or '/full-porn-movie-stream': 
    if base.endswith('/'):pagedir=base.rsplit('/')[-2]
    else:pagedir=base.rsplit('/')[-1]
    base+='/page/'
  prange=range(start,end)
  pages=[base+str(i)+'/' for i in prange]
  if 'debug' in sys.argv: print str(pages)
  inp=[(i,fast_proxy) for i in pages]
  contents=imap_bar(url_get_content_simple,inp,10,True)
  if not os.path.exists('htm/pages/images'): os.makedirs('htm/pages/images')
  imss=[]
  pageurls=[]
  page_images=[]
  if get_previews:
    print 'getting all pages urls...'
    
    for i in tqdm.tqdm(zip(contents,prange)):
      content,pagenum=i
      if not content:
        print 'empty content for page ',pagenum,'. COntinuing'
        continue
      soup=bs4.BeautifulSoup(content,bsp)
      articles=soup(class_='post')
      try:cur_pageurls=[xx(class_='entry-title')[0]('a')[0]['href'] for xx in articles]
      except:
        print 'could not get page urls for page number: ',pagenum
        continue
      images0=[xx('img') for xx in articles]
      images=[]
      for im in images0:
        if im:im=im[0]['src']
        else: im='unknown.jpg'
        images.append(im)
      pageurls.extend(cur_pageurls)
      page_images.extend(images)
    del contents;

  else:
    print 'rewriting html..'
    for i in tqdm.tqdm(zip(contents,prange)):
      content,pagenum=i
      if not content:
        print 'empty content for page ',pagenum,'. COntinuing'
        continue
      soup=bs4.BeautifulSoup(content,bsp)
      x=[i.decompose() for i in soup('script')]
      x=[i.decompose() for i in soup('meta')]
      x=[i.decompose() for i in soup(class_='menu-item')]
      ss=[j for j in soup('link',attrs={'rel':'stylesheet'}) if 'style.css' in j['href']]
      for j in ss:j['href']='index_files/style.4.9.8.css'
      images=soup('img')
      for im in images:
        ims=im['src']
        imss.append(ims)    
        imname=os.path.splitext(os.path.split(ims)[1])[0] 
        im['orig-src']=ims
        im['src']='images/'+imname+'.webp'
      if pagedir: os.system('mkdir -p htm/pages/'+pagedir)
      a=open('htm/pages/'+pagedir+'/'+str(pagenum).rjust(5,'0')+'.htm','w')
      a.write(str(soup).replace('</body>','\n<script type="text/javascript" src="index_files/common.js"></script>\n</body>'));a.close();
    del contents;
  if save_images: 
    print 'saving all referenced images as webp ..'
    x=imap_bar(urllib_save_webp,imss,7,False)
  if get_previews:
    # ~ find all preview images
    print 'getting actual page contents'
    inp=[(i,fast_proxy) for i in pageurls]
    contents=imap_bar(url_get_content_simple,inp,10,True)
    preview_urls=[]
    print 'getting preview images urls..'
    for i in tqdm.tqdm(contents):
      content=i
      if not content:
        print 'empty content for page ',pagenum,'. COntinuing'
        continue
      soup=bs4.BeautifulSoup(content,bsp)
      images=soup('img')
      previews=[i['src'] for i in images if i.has_attr('alt') and 'preview' in i['alt'].lower()]
      preview_urls.extend(previews)
    print 'got ',len(preview_urls),' preview images'
    if debug: print preview_urls
    del contents; 
    os.system('mkdir -p htm/pages/previews')
    x=imap_bar(urllib_save_webp,zip(preview_urls,page_images),7,False)
    
def url_get_content(url,proxy=None,user_agent=None):  
  if isinstance(url,tuple):    url,proxy,user_agent=url
  timeout=22
  debug=False
  try:
    if proxy:
      content=requests.get(url,timeout=timeout,proxies={'https':proxy}).content
    else:
      if  user_agent:
        content=requests.get(url,timeout=timeout,headers={'User-Agent': user_agent}).content
      else:
        content=requests.get(url,timeout=timeout).content
  # ~ except (requests.exceptions.ReadTimeout,requests.exceptions.ConnectionError):
  except :
    # ~ print 'request for content from ',url,' timed out after ',timeout,' seconds'
    if debug: print str(traceback.print_exc())
    content=''
  return content
  
def url_get_content_wget(url,proxy=None,user_agent=None):  
  if isinstance(url,tuple):    url,proxy,user_agent=url
  timeout=60
  content=os.popen('wget -q "'+url.strip()+'" -O -').read().strip()
  return content
def url_get_content_simple(url):
  url,proxy=url
     
  if proxy:
    content=requests.get(url,proxies={'https':proxy,'http':proxy}).content
  else:
    content=requests.get(url).content
  return content

def urllib_save_webp(ims):
  isprev=False;page_image=''
  if isinstance(ims,tuple):
    isprev=True
    preview_url,page_image=ims
    ims=preview_url
    
  from PIL import Image
  import urllib2
  
  imname=os.path.splitext(os.path.split(ims)[1])[0]
  if isprev:    imname=os.path.splitext(os.path.split(page_image)[1])[0]
  opener = urllib2.build_opener();
  opener.addheaders = [('User-Agent', 'Mozilla/5.0')] 
  # ~ try:pim=Image.open(urllib2.urlopen(ims,timeout=timeout))
  try:
    pim=Image.open(opener.open(ims,timeout=10))
    # ~ nw=int(float(pim.size[0])/2.)
    # ~ nh=int(float(pim.size[1])/2.)   
    base_height=240
    nw=int( (float(pim.size[0])/float(pim.size[0]))*base_height )
    nh=base_height
    
    if isprev:
      new_imloc='htm/pages/previews/'+imname+'.webp'
      pim.save(new_imloc,method=6)
    else:
      pim = pim.resize((nw,nh), Image.ANTIALIAS)
      new_imloc='htm/pages/images/'+imname+'.webp'
      pim.save(new_imloc,quality=45,method=6)
  except: pass
  
      

  

def download_file(url,local_filename):
    import requests.packages.urllib3
    requests.packages.urllib3.disable_warnings()
    r = requests.get(url, stream=True)
    f = open(local_filename, 'wb')
    file_size = int(r.headers['Content-Length'])
    chunk = 1
    num_bars = file_size / chunk
    bar =  progressbar.ProgressBar(maxval=num_bars).start()
    i = 0
    for chunk in r.iter_content():
        f.write(chunk)
        bar.update(i)
        i+=1
    f.close()
    return

  
def timesort(listfile):
  def timecmp(a,b):
    #~ return cmp(os.stat(a.strip()).st_mtime,os.stat(b.strip()).st_mtime)
    return cmp(os.stat(a.strip()).st_ctime,os.stat(b.strip()).st_ctime)
  b=open(listfile).readlines()
  b=[i for i in b if os.path.isfile(i.strip())]
  b.sort(cmp=timecmp,reverse=True)
  a=open('.vlist_timesorted','w')
  a.writelines(b);a.close()

def demanglewebstring(mys):
  return mys.replace('%2F','/').replace('%3F','?').replace('%3D','=').replace('%26','&').replace('%3A',':').replace('%22','"').replace('%252','%2')
  
def getvideourldailymotion(htmname,get240p=0,verbose=0):

 s=open(htmname).read()
 s2=s[s.find('var config =')+13:]
 s2=s2[:s2.find('}};')+2] # isolates the config string
 try:
   vids_meta=json.loads(s2)['metadata']
 except:
   #redownload
   return ('empty','JSON_UNREADABLE')
 video_title=vids_meta['title']+'-'+vids_meta['id']+'.mp4'
 vids_info=vids_meta['qualities']
 if verbose>0: print 'For '+video_title+' ,qualities available are: '+str(sorted(vids_info.keys()))

 try:video_url_144p=[i for i in vids_info['144'] if i['type']=='video/mp4'][0]['url'].strip()
 except:video_url_144p=''
 try:video_url_240p=[i for i in vids_info['240'] if i['type']=='video/mp4'][0]['url'].strip()
 except:video_url_240p=''
 try:video_url_360p=[i for i in vids_info['360'] if i['type']=='video/mp4'][0]['url'].strip()
 except:video_url_360p=''
 try:video_url_380p=[i for i in vids_info['380'] if i['type']=='video/mp4'][0]['url'].strip()
 except:video_url_380p=''
 try:video_url_480p=[i for i in vids_info['480'] if i['type']=='video/mp4'][0]['url'].strip()
 except:video_url_480p=''
 try:video_url_720p=[i for i in vids_info['720'] if i['type']=='video/mp4'][0]['url'].strip()
 except:video_url_720p=''


 if sys.argv[-1] in ['printlink','pl']:

   #~ video_title=video_title.replace('.mp4','.240p.mp4')
   print 'TITLE: '+highlight(video_title,'green')
   for qq in vids_info:
     print str(qq)
     # ~ if qq['type']=='video/mp4':
       # ~ print '%s : %s' %(qq,qq['url'])
   if video_url_144p: print 'URL: '+highlight(video_url_144p)
   if video_url_240p: print '240p URL: '+highlight(video_url_240p,'red')
   if video_url_360p: print '360p URL: '+highlight(video_url_360p,'red')
   if video_url_380p: print '380p URL: '+highlight(video_url_380p,'red')
   if video_url_480p: print '480p URL: '+highlight(video_url_480p,'red')
   if video_url_720p: print '720p URL: '+highlight(video_url_720p,'red')
   sys.exit(1)

 if get240p>0:
   if get240p==1:     return video_url_240p,video_title
   elif get240p==2:   return video_url_360p,video_title
   elif get240p==3:
     vurl=video_url_480p
     if not vurl:
       if video_url_380p:
         print highlight('no 480p .,using 380p','red')
         vurl=video_url_380p
       elif video_url_360p:
         print highlight('no 480p .no 380p,using 360p','red')
         vurl=video_url_360p
       elif video_url_240p:
         print highlight('no 480p .no 380p,no 360p.using 240p','red')
         vurl=video_url_240p       
     return vurl,video_title
   elif get240p==4:
     vurl=video_url_720p
     if not vurl:
       if video_url_480p:
         print highlight('no 720p .,using 480p','red')
         vurl=video_url_480p
       elif video_url_380p:
         print highlight('no 480p .,using 380p','red')
         vurl=video_url_380p
       elif video_url_360p:
         print highlight('no 480p .no 380p,using 360p','red')
         vurl=video_url_360p
       elif video_url_240p:
         print highlight('no 480p .no 380p,no 360p.using 240p','red')
         vurl=video_url_240p       
     return vurl,video_title
   else:
     print 'get240p should be in [0,1,2,3,4] for 144p,240p,360p,480p,720p only.Exiting'
     #~ print 'For '+video_title+' ,available qualities available are: '+str(sorted(vids_info.keys()))
     sys.exit(1)
 else:
   #get 144p video
   if '144' in vids_info:
     video_url=[i for i in vids_info['144'] if i['type']=='video/mp4'][0]['url'].strip()
     return video_url,video_title
   else:
     print 'ERROR: dailymotion video '+video_title+' does not have 144p format'
     print 'For '+video_title+' ,available qualities available are: '+str(sorted(vids_info.keys()))
     sys.exit(1)


def processurldailymotion(s,get240p=0,clean=0,writefile=0):
  #~ """get240=[0,1,2,3] means get 144p,240p,360p,480p etc. Note that some qualities might be unavailable
 get240p=0
 dailymotion_signifier='DAILYMOTION'
 aria_cmd='aria2c --log-level=info -x '+str(num_connections)+' -k 1M -s '+str(num_connections)+' -j '+str(num_connections)


 if sys.argv[-1] in ['240p']:
   get240p=1
   print 'getting 240p video'
 elif sys.argv[-1] in ['360p']:
   get240p=2
   print 'getting 360p video'
 elif sys.argv[-1] in ['480p']:
   get240p=3
   print 'getting 480p video'
 elif sys.argv[-1] in ['720p']:
   get240p=4
   print 'getting 720p video'

 if writefile>0:
   print 'WRITING to file, NOT downloading'
   a=open('linksfile.txt','a')
 #if its not embed, change to embed since embed htm has same data, but is half the size
 if '/embed/' not in s:
   dailymotion_video_id=s[s.rfind('/')+1:].split('_')[0] #like 'x3cwy5a'
   htmname=metabase+dailymotion_signifier+'_EMBED_'+os.path.split(s.strip())[-1]+'.htm'
   dailymotion_embed_url='http://www.dailymotion.com/embed/video/'+dailymotion_video_id
 else:
   dailymotion_embed_url=s
   htmname=metabase+dailymotion_signifier+'_EMBED_'+os.path.split(s.strip())[-1]+'.htm'
 if clean==1:
  #~ print 'removing old video htm file from vids_metadata'
  os.system('rm -vf "'+htmname+'"')

 #~ print htmname

 if os.path.exists(htmname): #means already in list,
  print htmname+' file was in cache'

 else: #means download htm first
  
  download_file(dailymotion_embed_url.replace('http://','https://'),htmname)
 
 # ~ print 'get240p:',get240p
 video_url,video_title=getvideourldailymotion(htmname,get240p=get240p,verbose=0)
 

 video_title=video_title.replace('/','_').strip()
 if dailymotion_downloader=='axel':
  dwncommand='axel -n '+str(num_connections)+' -a "'+video_url+'" -o "'+video_title+'"'
 else:
  dwncommand=aria_cmd+' "'+video_url+'" -o "'+video_title+'"'
 #~ print 'starting download '

 print dwncommand

 comment_str='Downloading: '+colorama.Back.BLACK+colorama.Fore.WHITE+colorama.Style.BRIGHT
 # ~ if get240p:comment_str+='240p '
 comment_str+=video_title+colorama.Back.RESET+colorama.Fore.RESET+colorama.Style.RESET_ALL+''
 print comment_str
 if writefile==0:
   if type(dwncommand)==unicode:    dwncommand=dwncommand.encode('ascii','replace')
   os.system(dwncommand)
 else:
   ws='##'+s+'\n'+video_url+'\n\tout='+video_title+'\n'
   if type(ws)==unicode:
    ws=ws.encode('ascii','replace')
   a.write(ws)
   a.close()
   return video_url,video_title

def processurlyoutube(s,clean=0):
  #fname is video id , so for https://www.youtube.com/watch?v=fO8cePZxRus , it would be fO8cePZxRus
 fname=s.split('watch?v=')[1]
 if '&index=' in fname: fname=fname.split('&')[0]
 youtube_signifier='YOUTUBE___'
 aria_cmd='aria2c --log-level=info -x '+str(num_connections)+' -k 1M -s '+str(num_connections)+' -j '+str(num_connections)


 htmname=metabase+youtube_signifier+fname+'.htm'
 if clean==1:  os.system('rm -vf "'+htmname+'"')
 if os.path.exists(htmname):   print htmname+' file was in cache'

 else: #means download htm first
  print 'downloading htm file to cache'
  htmdwncommand='aria2c "'+s.strip()+'" -d "'+metabase+'" -o "'+os.path.split(htmname)[1]+'"'
  print htmdwncommand
  os.system(htmdwncommand)

 #Default behaviour is to try for 278+249, if that fails then go for 17
 video_url1,video_title1,data1,success1=getvideourlyoutube(htmname,desired_fmt_itag='278', verbosity=2)
 video_url2,video_title2,data2,success2=getvideourlyoutube(htmname,desired_fmt_itag='249', verbosity=0)

 if 0: #(success1 and success2):
   print 'format 278+249 found, now downloading '
   dwncommand1=aria_cmd+' "'+video_url1+'" -o "'+video_title1+'"'
   #~ print dwncommand1;
   os.system(dwncommand1)
   dwncommand2=aria_cmd+' "'+video_url2+'" -o "'+video_title2+'"'
   #~ print dwncommand2;
   os.system(dwncommand2)
 else: #go for 3gp fmt 17
  print 'format 278+249 not found, now downloading 3gp format 17 '
  video_url,video_title,data,gpsuccess=getvideourlyoutube(htmname,desired_fmt_itag='17', verbosity=0)
  if gpsuccess:
   dwncommand=aria_cmd+' "'+video_url+'" -o "'+video_title+'"'
   #~ print dwncommand;
   os.system(dwncommand)


def remove_m_pstars(ps):
  try:
    import gender_guesser.detector
  except ImportError:
    os.system('pip2 install --user  gender-guesser')
    import gender_guesser.detector
    
  d=gender_guesser.detector.Detector()
  ps2=[]
  for p in ps:
    gg=d.get_gender(p.split()[0])
    if gg not in ['male','mostly_male']:ps2.append(p.replace(' ',''))
  ps2.sort()
  return ps2
      

  
def streamzcc_get_redirect_url_catch(streamz_url='',driver=''):
  try:
    return streamzcc_get_redirect_url(streamz_url='',driver='')
  except:
    print 'failed for url'+streamz_url
    return None
def streamzcc_get_redirect_url(streamz_url='',driver=''):
  # ~ streamz_url='https://streamz.cc/idGxpM2s4dnRjNG4wZXgw;40813bf777278ff4ffec2afdc60d7e7aS609'  

  from selenium import webdriver;from bs4 import BeautifulSoup as bs;import os
  
  # ~ options = webdriver.ChromeOptions();options.add_argument('--ignore-certificate-errors');options.add_argument("--headless")
  # ~ driver = webdriver.Chrome(chrome_options=options)  
  driver.get(streamz_url)
  htm=driver.page_source;soup=bs(htm,'lxml');
  url2=soup('video')
  if url2: url2=url2[0]['src'];
  else: return
  cmd='wget --max-redirect 0  '+url2
  x=os.popen3(cmd)[2].read();redirect_url=''
  if 'Location:' in x:
   x=x.splitlines();redirect_url=x[-2].split()[1]
  # ~ driver.close()
  if redirect_url: return redirect_url

###FOR MIXDROP.TO (only works on gsh500 roght now)
# ~ from selenium.webdriver.common.action_chains import ActionChains;from selenium import webdriver;from bs4 import BeautifulSoup as bs;import os

# ~ options=webdriver.ChromeOptions();options.add_argument('--ignore-certificate-errors');options.add_argument("--headless")
# ~ driver=webdriver.Chrome(chrome_options=options)

# ~ url='https://mixdrop.to/e/0v9zlev9tj47wr';driver.get(url);

# ~ elements=driver.find_elements_by_xpath("//*[contains(@id, 'videojs_html5_api')]");elem=elements[0]
# ~ actions=ActionChains(driver)
# ~ actions.move_to_element(elem).send_keys('\n').perform();
# ~ actions.move_to_element(elem).click().perform();actions.move_to_element(elem).click().perform();
# ~ htm=driver.page_source;soup=bs(htm,'lxml');
# ~ url2=soup('video')
# ~ if url2: url2=url2[0]['src'];

def split_in_buckets():
  b=[i.strip() for i in open('.vlist').readlines() if i.strip()]
  fb=[os.stat(i).st_size/(1024*1024.) for i in b]; #size in mb
  mfgetsizefromfile(infile='',maxsize=None,uselist=False)
  
      
    
def mixdrop_get_urls(infile,verbose=True):
  from selenium import webdriver;from selenium.webdriver.common.action_chains import ActionChains;
  from bs4 import BeautifulSoup as bs;import os
  if systype in ['cloudshell']:
    if  not os.path.exists('/usr/bin/chromium'):
      # ~ print 'installing chromedriver..'
      # ~ x=os.popen3('sudo apt-get install chromedriver xvfb -y')[1].read()
      print 'installing chromedriver..'
      # ~ x=os.popen3('sudo apt-get install chromedriver xvfb -y')[1].read()
      x=os.popen3('sudo apt-get install chromium xvfb -y')[1].read()
    else:
      os.system('sudo killall chromium chromedriver')
    options=webdriver.ChromeOptions();options.add_argument('--ignore-certificate-errors');options.add_argument("--headless")
    driver=webdriver.Chrome(chrome_options=options)

  
  mixdrop_urls=[i.strip() for i in open(infile).readlines() if 'mixdrop.to' in i]
  a=open('.mxdlog','w');cnt=0
  a2=open('linksfile.txt','w')
  raw_links=[];  res=[]
  for i in tqdm.tqdm(mixdrop_urls):
    vname=i.split('::')[1].strip()
    vurl=i.split('::')[3].strip()
    htm=os.popen('wget -q '+vurl+' -O -').read().strip()
    # ~ soup=bs4.BeautifulSoup(htm,bsp)
    # ~ mpt=[i.text for i in soup('script') if 'mp4' in i.text]
    # ~ mpt=[str(i) for i in soup('script') if 'mp4' in str(i)]
    # ~ if mpt:mpt=mpt[0]
    # ~ if sys.argv[-1] in ['debug']: print mpt
    # ~ if not mpt: continue
    # ~ mpt=mpt[mpt.find('MDCore|'):mpt.rfind('|vfile')]
    # ~ if sys.argv[-1] in ['debug']: print '----\n'+vurl+'\n'+mpt
    # ~ mpt=mpt.split('|')
    # ~ mpt2=mpt[mpt.index('thumbs'):]
    # ~ token=[i for i in mpt2 if i.isdigit() and len(i)==10]
    # ~ if not token: print 'could not find token for '+vurl
    # ~ else: token=token[0]
    # ~ token_index=mpt2.index(token)
    # ~ s_marker=''
    #other is one plus-minus
    # ~ if token==mpt2[-1]:s_marker=mpt2[-2]
    # ~ else:
      # ~ if len(mpt2[token_index-1])>len(mpt2[token_index+1]): s_marker=mpt2[token_index-1]
      # ~ else: s_marker=mpt2[token_index+1]
    # ~ link_url='https://a-'+mpt[1]+'.'+mpt[3]+'.net/v/'+mpt[2]+'.mp4?s='+s_marker+'&e='+token
    # ~ if sys.argv[-1] in ['debug']: print 'from bs4: '+link_url
    fsize=1.0;url2=''
    if systype in ['cloudshell']:
      try:
        driver.get(vurl)
      except:
        print 'error loading url: %s in chromedriver' %(vurl)
        continue
      elements=driver.find_elements_by_xpath("//*[contains(@id, 'videojs_html5_api')]");elem=elements[0]
      actions=ActionChains(driver)
      actions.move_to_element(elem).send_keys('\n').perform();
      actions.move_to_element(elem).click().perform();actions.move_to_element(elem).click().perform();
      htm=driver.page_source;soup=bs(htm,'lxml');
      url2=soup('video')
      if url2: url2=url2[0]['src'];
      if not url2.startswith('https://'):
        url2='https://'+url2
        url2=url2.replace('https:////','https://')
      # ~ if sys.argv[-1] in ['debug']: print url2
      # ~ fsize=get_filesize_of_url_basic(url2)
      fsize=get_filesize_of_url_wget_spider(url2)
      if sys.argv[-1] in ['debug']: print 'fsize spider:'+str(fsize)
    else:
      url2=link_url
    sep=' :: '
    raw_info='mxd-0'+sep+vname+sep+'%.1f Mb' %(fsize)+sep+vurl
    a.write(raw_info+'\n')
    
    a2.write(url2+'\n\tout='+vname+'\n')
    raw_links.append(raw_info)
    cnt+=1
  a.close()
  if systype in ['cloudshell']:
    try:driver.close()
    except: print 'error in closing driver';pass
  print 'wrote %d/%d links to .mxdlog' %(cnt,len(mixdrop_urls))
  return raw_links
    
def streamzcc_get_redirect_urls(infile,verbose=True):
  from selenium import webdriver;from bs4 import BeautifulSoup as bs;import os
  if systype in ['cloudshell'] and  not os.path.exists('/usr/bin/chromedriver'):
    os.system('sudo apt-get install chromedriver xvfb -y')
  
  options=webdriver.ChromeOptions();options.add_argument('--ignore-certificate-errors');options.add_argument("--headless")
  driver=webdriver.Chrome(chrome_options=options)
  
  streamz_urls=[];redirect_urls=[]
  streamz_urls=[i.strip() for i in open(infile).readlines() if 'streamz.cc' in i]
  if not streamz_urls:
    print 'no streamz.cc urls found!!';
    return
  if '::' in streamz_urls[0]:    streamz_urls=[(i.split('::')[-1].strip(),i.split('::')[1].strip()) for i in streamz_urls] #(url,vname)
  
  results=[]
  for i in tqdm.tqdm(streamz_urls):
    # ~ redirect_url=streamzcc_get_redirect_url_catch(i[0],driver)
    redirect_url=streamzcc_get_redirect_url(i[0],driver)
    if redirect_url:      results.append((i[1],redirect_url))
  driver.close()
  if results:
    a=open('linksfile.txt','w')
    for i in results: a.write(i[1]+'\n\tout='+i[0]+'\n')
    a.close()
    print 'found %d/%d valid urls in streamz.cc from %s' %(len(results),len(streamz_urls),infile)
      
def get_filesize_of_url_wget_spider(url):
  x=os.popen3('wget --spider "'+url+'"')[2].read().strip()
  x=x.splitlines()
  if 'remote file exists' in x[-1].lower():#non-existent file is not valid
    if sys.argv[-1] in ['debug']: print 'in remote exists'
    fs=x[-2]    
    if fs.lower().startswith('length') and fs.split()[-2].endswith('M)'):
      if sys.argv[-1] in ['debug']: print 'in parse len'
      fs=fs.split()[-2].replace('M)','').replace('(','')
      if fs.replace('.','').isdigit():
        if sys.argv[-1] in ['debug']: print 'in final'
        return float(fs)
  if sys.argv[-1] in ['debug']:  print str(x)
  return 0
  
def get_filesize_of_url_basic(url):
  timeout=6
  if isinstance(url,tuple):      url,timeout=url
  try:
    rg=requests.get(url,stream=True,timeout=timeout)
    fsize=float(rg.headers['Content-length'])/(1024**2) #in mb
    return fsize
  except :
    return 0
def serakon_get_url_single(url):
  try:
    verbose=True
    debug=False
    video_id=''
    
    uu=url[:]
    if uu.endswith('/'):uu=uu[:-1]
    if 'vsex.in' in url: studio=os.path.split(uu)[0].split('vsex.in/')[1].replace('/','_').replace('-','_')
    uu=os.path.split(uu)[1].replace('.html','').replace('.htm','')
    video_id=uu.split('-')[0]
    
    if 'vsex.in' in url:     video_id='VSX'+video_id+'  '+studio
    elif 'serakon.com' in url: video_id='SRK'+video_id
        
    if sys.argv[-1] in ['debug']: debug=True
    if os.path.exists(os.path.split(url)[1]):
      htm=open(os.path.split(url)[1]).read()
    else:
      htm=os.popen('wget -q "'+url+'" -O - ').read().strip()
    soup0=bs4.BeautifulSoup(htm,bsp)
    male_pornstars=[]
    pornstars=[i['href'].split('/pornstar/')[1].replace('/','').replace('+',' ').replace('%20',' ').strip() for i in soup0('a') if ('serakon.com/pornstar/' in i['href'] or 'vsex.in/pornstar/' in i['href'])]
    pornstars=remove_m_pstars(pornstars)
    pornstars.sort()
    pornstars=pornstars[:8]
    allpornstars='_'.join(pornstars)
    allpornstars=allpornstars
    if type(allpornstars)==unicode:  allpornstars=allpornstars.encode('ascii','replace')
    allpornstars=allpornstars.replace('%27','.')
    iff=[i for i in soup0('iframe') if (i.has_attr('allowfullscreen') or 'stream.dlc.' in i['src']) ] #first condition for bitporno, second for verystream
    if not iff:
      if debug: print 'could not get prelimnary iframe for ',url
      return '0FAILED'
    if len(iff)>1:
      # ~ if verbose: print 'got more than 1 iframe,choosing 1st for url: ',url
      print 'got more than 1 valid stream iframe,choosing 1st for url: ',url
    iff=iff[0]
    dlc=iff['src']
    if debug: print 'dlc"', dlc
    s=os.popen('wget -q "'+dlc+'" -O - ').read().strip()
    s=s[s.find('url=')+4:];s=s[:s.find('"')];stream_url=s
    if debug: print 'stream_url"', stream_url
    if 'bitporno' in stream_url:
      s=os.popen('wget -q "'+stream_url+'" -O - ').read().strip()
      soup=bs4.BeautifulSoup(s,bsp)
      bpl=soup('a',attrs={'href':re.compile('.*bitporno*')})
      if debug: print bpl
      if bpl:
        bpl_480=[i for i in bpl if 'q=480p' in i['href']]
        if bpl_480:
          stream_url_link=bpl_480[0]['href']
        else:      
          stream_url_link=bpl[0]['href']
    elif not stream_url.startswith('http') or 'hqq.tv' in stream_url:
      return 'FAILED'
    else:
      stream_url_link=stream_url
    # ~ else:    
      # ~ fichier_link=[i for i in soup0('iframe') if '1f.dlclink' in i['src'] ]
      # ~ if fichier_link:
        # ~ fichier_link=fichier_link[0]
        # ~ soup2=bs4.BeautifulSoup(requests.get(fichier_link).content,bsp)
        # ~ x=[i for i in soup2('a') if '1fichier.com' in i]
        # ~ if x: x=x[0].text
        # ~ return 'FAILED,'+x
      # ~ else:
      # ~ return 'FAILED'
    return stream_url_link,allpornstars,video_id
  except:
    return
  
def parse_config(verbose=True):
  import yaml,pprint
  #defaults
  prefs={'crf': 28, 'speed': 'medium','scale': None,'chunksize':None,'acodec': 'libopus','vcodec': 'hevcaq','abr': '20k','mf_account':None,'fmt':'mkv'}
  if os.path.exists('.crop'):    
    conf=yaml.safe_load(open('.crop'))
    for k in conf.keys():      prefs[k]=str(conf[k]).strip()
    if prefs.has_key('account') and prefs['account'] : prefs['mf_account']=prefs['account']
  elif os.path.exists('.crop2'):    
    conf=yaml.safe_load(open('.crop2'))
    for k in conf.keys():      prefs[k]=str(conf[k]).strip()
    if prefs.has_key('account') and prefs['account'] : prefs['mf_account']=prefs['account']
  
  if prefs['scale'] and 'x' not in prefs['scale']: #assume value is height only. deduce width(aspect ratio: 1.77)
    h=int(prefs['scale'])
    if h%2!=0: h+=1
    w=int(1.77777*h)
    if w%2!=0: w+=1
    #HACKS for particular resolutions
    if h in [208]: w=368;h=208; 
    elif h in [240]: w=424;h=240;    
    elif h in [270,272]: w=480;h=272; 
    elif h in [288,280]: w=512;h=288; #w=300 does'nt give proper multiple-of-16 h
    elif h in [320]: w=568;h=320; #w=300 does'nt give proper multiple-of-16 h
    elif h in [400,420,432]: w=768;h=432; #768 is divisible-by-16
    elif h in [480]: w=848;h=480; #856 is divisible-by-8
    elif h in [768]: w=1366;h=768; #856 is divisible-by-8
    
    prefs['scale']=str(w)+'x'+str(h)


  #alias speed and preset
  if 'preset' in prefs:
#    print 'warn: preset is to be called speed in .crop'.upper()
    prefs['speed']=prefs['preset']
  
  if verbose: pprint.pprint(prefs)
  return prefs
  
def serakon_get_preview_single(url):
  prefs=parse_config(verbose=False)
  verbose=False
  preview_webp_quality=9
  if 'vsex.in' in url: preview_webp_quality=8
  if prefs.has_key('preview_webp_quality'):
    preview_webp_quality=int(prefs['preview_webp_quality'])
  
  if os.path.exists(os.path.split(url)[1]):    htm=open(os.path.split(url)[1]).read()
  else:    htm=os.popen('wget -q "'+url+'" -O - ').read().strip()
  soup0=bs4.BeautifulSoup(htm,bsp)
  caption_url=[i for i in soup0('a') if i.has_attr('href') and ('capp.dlc.ovh' in i['href'] or 'capv.dlc.ovh' in i['href'])]
  if len(caption_url)==0:
    if verbose: print 'could not find caption for ',url
    return
  elif len(caption_url)>1:
    if verbose: print 'more than 1 captions for ',url,' .choosing first one'
    caption_url=caption_url[0]['href']
  elif  len(caption_url)==1:
    caption_url=caption_url[0]['href']
  
    
  imname=os.path.split(url)[1].replace('.html','').replace('.htm','')
  
  new_imloc='previews/'+imname+'.webp'      
  if os.path.exists(new_imloc) or (prefs.has_key('no_webp') and prefs['no_webp'] in ['1']):
    return (caption_url,imname)
  
  from PIL import Image
  import urllib2
  # ~ from StringIO import StringIO
  
  
  opener = urllib2.build_opener();
  opener.addheaders = [('User-Agent', 'Mozilla/5.0')] 
  # ~ try:pim=Image.open(urllib2.urlopen(ims,timeout=timeout))
  try:
    pim=Image.open(opener.open(caption_url,timeout=10))
  except:
    return (caption_url,imname)
  
  # ~ response = requests.get(caption_url)
  # ~ pim=Image.open( StringIO(response.content) )
  
  if 'vsex.in' in url:
    nw=1152;nh=720;
    pim = pim.resize((nw,nh), Image.ANTIALIAS)
  elif (float(pim.size[1]) > 1500) : #means 2CD
    nw=int(float(pim.size[0])/2.)
    nh=int(float(pim.size[1])/2.)   
    pim = pim.resize((nw,nh), Image.ANTIALIAS)
    
  pim.save(new_imloc,quality=preview_webp_quality,method=6)
  # ~ else:
    # ~ pim.save(new_imloc,method=6)
  return (caption_url,imname)
  
  
def serakon_get_previews(infile,verbose=True):
  prefs=parse_config(verbose=False)
  if prefs.has_key('no_webp') and prefs['no_webp'] in ['1']:
    print highlight('only writing linksfile.txt with .jpg links.not converting to webp'.upper(),'red')
  b=[i.strip() for i in open(infile).readlines() if i.startswith(('http://','https://')) and i.strip().endswith(('.htm','.html')) and ('serakon.com' in i or 'vsex.in' in i)]
  b=list(set(b))
  os.system('mkdir -p previews')
  num_proc=5
  if prefs.has_key('num_proc'):  num_proc=int(prefs['num_proc']) ;print 'using %d parallel processes' %(num_proc)
  res0=imap_bar(serakon_get_preview_single,b,num_proc,False,desc='finding serakon_vsex previews')
  res=[i for i in res0 if i and i not in ['FAILED','0FAILED']]
  if prefs.has_key('no_webp') and prefs['no_webp'] in ['1']:
    a=open('linksfile.txt','w')
    for i in res:
      caption_url,imname=i
      a.write(caption_url+'\n\tout='+imname+'.jpg\n\tdir=previews_jpg\n')
    a.close()
  print 'wrote %d preview webp images to ./previews/' %(len(res))
  
  
def pornmaki_get_url(url):
  soup=bs4.BeautifulSoup(requests.get(url).content,bsp)
  x=[i for i in soup('script') if 'var url' in i.text]
  if x:
    x=x[-1].text
    x=x[x.index('file:')+6:]
    vurl=x[:x.index('"')]
    vname=os.path.split(url)[-1].replace('.html','')    
    video_id=vname.rsplit('-')[-1].replace('PMK','')
    vname='-'.join(vname.rsplit('-')[:-1])
    video_size=get_filesize_of_url_basic(vurl)
    vname='PORN__'+vname
    return (vurl,vname,video_id,video_size)
  else:
    return None
def pornmaki_logfile_get_urls(infile,verbose=True):
  b=[i.strip() for i in open(infile).readlines() if i.startswith('pmk-')]
  a=open('tmpfile','w')
  for i in b:
    vname=i.split('::')[1].strip().replace('-PMK','-').replace('PORN__','')
    video_id=i.split('::')[3].strip()
    vurl='http://pornmaki.com/video/'+vname+'.html'
    a.write(vurl+'\n')
  a.close()
  pornmaki_get_urls('tmpfile')
  if os.path.exists('tmpfile'): os.remove('tmpfile')
  
def pornmaki_get_urls(infile,verbose=True):
  b=[i.strip() for i in open(infile).readlines() if 'pornmaki.com' in i]
  res=imap_bar(pornmaki_get_url,b,4,use_dummy=False,desc='getting pmaki links')
  res=[i for i in res if i]
  if sys.argv[-1] not in ['nomklog']:    a2=open('.pmklog','w')
  sep=' :: ';
  a=open('linksfile.txt','w')
  for i in res:
    vurl,vname,video_id,video_size=i
    a.write(vurl+'\n\tout='+vname+'-PMK'+video_id+'.mp4\n')
    if sys.argv[-1] not in ['nomklog']: 
      info='pmk-0'+sep+vname+'-PMK'+video_id+sep+'%.1f Mb' %(video_size)+sep+video_id
      a2.write(info+'\n')
  a.close()
  if sys.argv[-1] not in ['nomklog']: a2.close()
    
def serakon_get_urls(infile,verbose=True):
  try:
    import gender_guesser.detector
  except ImportError:
    os.system('pip2 install --user  gender-guesser')
    import gender_guesser.detector
  b=[]
  b0=[i.strip() for i in open(infile).readlines() if i.startswith('goun-x') and '-SRK' in i]
  if b0: #means its .gounlog  file.make serakon links
    for i in b0:
      name=os.path.splitext(i.split('::')[1].strip())[0]
      sera_id=name.rsplit('-SRK')[1]
      serakon_url='http://serakon.com/'+sera_id+'-'+ '---'.join(name.split('---')[:-1]).replace('PORN__','').replace('_','-')+'.html'
      b.append(serakon_url)
  else:  
    b=[i.strip() for i in open(infile).readlines() if i.startswith(('http://','https://')) and i.strip().endswith(('.htm','.html'))]
  b=list(set(b))
  res0=imap_bar(serakon_get_url_single,b,3,False,desc='finding serakon links')
  res=[i for i in res0 if i not in ['FAILED','0FAILED']]
  print 'found %d streaming links out of %d\nwriting to ./l' %(len(res),len(res0))
  a=open('./l','w')
  if len(res)!=len(res0): a2=open('failed','w');failed_cnt=0
  towr=[]
  infos=[]
  for j in range(len(res0)):
    i=res0[j]    
    if sys.argv[-1] in ['debug']: print i
    if i and i!='0FAILED' and i!='FAILED':         
      pstars=i[1]
      video_id=i[2]
      i=i[0]      
      burl=os.path.split(b[j])[1].replace('.html','').replace('.htm','')
      burl=' '.join(burl.split('-')[1:])
      a.write(i+' , ' +burl+' , '+pstars+' , '+video_id+'\n')
      sera_vname='PORN__'+burl.strip()+'---'+pstars+'-'+video_id+'.mp4'
      sera_vname=re.sub('[^A-Za-z0-9_()-.#]', '_', sera_vname)
      towr.append('youtube-dl -o "'+sera_vname+'" "'+i+'"')
      infos.append((sera_vname,i))
      # ~ if 'verystream' in i or 'goun' in i:                
        # ~ a.write(i+' , ' +burl+' , '+pstars+' , '+video_id+'\n')
      # ~ else:
        # ~ a.write(i+'\n')
    # ~ if i and i!='FAILED':    a.write(i+'\n')
    # ~ elif i and i.startswith('FAILED'):
      # ~ if len(i.split(','))>1:
        # ~ fichier_link=i.split(',')[1]
        # ~ a2.write(b[j]+'\n\t'+fichier_link+'\n')
      # ~ else      
      # ~ a2.write(b[j]+'\n')
    elif len(res)!=len(res0): a2.write(b[j]+'\n');failed_cnt+=1
    else: a.write('##FAILED '+b[j]+'\n')
  a.close()
  if len(res)!=len(res0): a2.close()
  # ~ print 'written %d failed links to ./failed' %(failed_cnt)
  if sys.argv[-1] in ['mklog','vs','vslog','wr']:
    # ~ print highlight('passing entries in ./l to verstream extractor (basically: vd lwvs ./l)')
    # ~ os.system('vd lwvs ./l fastlog')
    # ~ print highlight('passing entries in ./l to gounlimited extractor (basically: vd lwgoun ./l)')    
    # ~ os.system('vd lwgoun ./l')
    a=open('.ytdllog','w');a2=open('.stczlog','w');a3=open('.mxdlog','w')
    #sort by provider
    infos=sorted(infos,key=lambda jj: jj[1]);
    cnt1=0;cnt2=0;cnt3=0
    for i in infos:
      if 'streamz.cc' in i[1]:    a2.write('ytdl-0 :: '+i[0]+' :: 1 Mb :: '+i[1]+'\n');cnt1+=1
      elif 'mixdrop.to' in i[1]:    a3.write('ytdl-0 :: '+i[0]+' :: 1 Mb :: '+i[1]+'\n');cnt2+=1
      else:                       a.write('ytdl-0 :: '+i[0]+' :: 1 Mb :: '+i[1]+'\n'); cnt3+=1
    a.close();a2.close();a3.close()
    a=open('ytdl.sh','w')
    for i in towr: a.write(i+'\n')
    a.close()
    print 'wrote %d lines to ytdl.sh\nto .stczlog: %d, .mxdlog: %d , .ytdllog: %d' %(len(towr),cnt1,cnt2,cnt3)
    
def serakon_get_page_single(url):
  verbose=True
  # ~ if os.path.exists(HOME+'.python_scripts/cookies.txt'):
    # ~ htm=os.popen('wget --load-cookies='+HOME+'/.python_scripts/cookies.txt -q "'+url+'" -O - ').read().strip()
  # ~ else:
    # ~ htm=os.popen('wget -q "'+url+'" -O - ').read().strip()
  htm=os.popen("curl -s --user-agent 'Mozilla/5.0 (X11; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0 (Chrome)'  '"+url+"' ").read().strip()
  soup=bs4.BeautifulSoup(htm,bsp)
  title=soup.title
  if 'vsex.in' in url:
      basename=url.split('/page/')[0]
      basename=os.path.split(basename)[1]
      pagenum=url.split('/page/')[1].replace('/','')            
      title=basename+'_'+pagenum
      # ~ print basename,pagenum,title
  else:
    if title:    title=title.text
    else:
      if 'serakon.com//page/' in url or 'serakon.com/page/' in url:
        if url.endswith('/'):pagenum=url[:-1]
        title='BASE__'+os.path.split(pagenum)[1]
    
  if os.path.exists(title+'.htm'):
    # ~ if len(title.split('__'))>1 and title.split('__')[1].strip():
      # ~ title=title.split('__')[0]+'__'+str( int(title.split('__')[1].strip())+1 )
    # ~ else: title+='__2'
    return
    
  if type(title)==unicode:  title=title.encode('ascii','replace').replace('?','_').replace('/','_')
  # ~ ims=soup('img',attrs={'src':re.compile('.*dlc*')})
  if 'vsex.in' in url:
    ims=soup('img')
    if not ims:
      print 'NO IMAGES!!'
    ims=[i for i in ims if i and i.has_attr('src') and '/uploads/posts' in i['src']]
  else:
    ims=soup('img',attrs={'class':'fiximg'})
  to_get=[]
  x=[l for l in soup('link',attrs={'href':re.compile('.*styles*')})]
  for i in x:    i['href']='css/styles.css'
  
  
  
  for im in ims:
    im_href=im.parent['href']
    thumb_image_name=os.path.split(im_href)[1].replace('.html','').replace('.htm','')+'.webp'
    if '.jpg' not in im['src']: continue    
    if not os.path.exists('images/'+thumb_image_name):
      thumb_image_source_url=im['src']
      if thumb_image_source_url.startswith('/'):
        if 'vsex.in' in url:
          thumb_image_source_url='http://vsex.in/'+thumb_image_source_url
        else:  
          thumb_image_source_url='http://serakon.com/'+thumb_image_source_url
      to_get.append((thumb_image_source_url,thumb_image_name))
    im['src']='images/'+thumb_image_name
    # ~ im['src']='images/'+os.path.split(im['src'])[1].replace('.jpg','.webp')
    
  from PIL import Image
  import urllib2
  from StringIO import StringIO
  a=open(title+'.htm','w');a.write(str(soup));a.close()
  for i in to_get:
    im_url,thumb_image_name=i
    # ~ imname=os.path.split(i)[1]   
    
    # ~ try:    
    # ~ opener = urllib2.build_opener();
    # ~ opener.addheaders = [('User-Agent', 'Mozilla/5.0')] 
    # ~ try:pim=Image.open(urllib2.urlopen(ims,timeout=timeout))
    # ~ pim=Image.open( StringIO(opener.open(i,timeout=10)) )
    response = requests.get(im_url)
    pim=Image.open( StringIO(response.content) )
    nw=int(float(pim.size[0])/2.)
    nh=int(float(pim.size[1])/2.)   
    # ~ new_imloc='images/'+imname.replace('.jpg','.webp')
    new_imloc='images/'+thumb_image_name
    pim.save(new_imloc,quality=35,method=6)
    # ~ except:
      # ~ if verbose:
        # ~ print sys.exc_info()
        # ~ print 'could not save iamge ',i ,' as webp'
      
  
 
def serakon_get_pages(url,verbose=True):
  isfile=False
  urls=''
  if not os.path.exists('images'):os.system('mkdir -pv images')
  if os.path.exists(url):
    isfile=True
    print 'treating ',url,' as file'
    urls=[i.strip() for i in open(url).readlines() if ('serakon.com/' in i or 'vsex.in' in i)]
    url=list(set(urls));urls.sort()
  else: urls=[url]
  x=imap_bar(serakon_get_page_single,urls,3,False,desc='getting serakon pages')
  
def vsex_get_pornstars_single(url):
  verbose=True
  if os.path.exists(HOME+'.python_scripts/cookies.txt'):
    htm=os.popen('wget --load-cookies='+HOME+'/.python_scripts/cookies.txt -q "'+url+'" -O - ').read().strip()
  else:
    htm=os.popen('wget -q "'+url+'" -O - ').read().strip()
  # ~ htm=os.popen("curl -s --user-agent 'Mozilla/5.0 (X11; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0 (Chrome)'  '"+url+"' ").read().strip()
  soup=bs4.BeautifulSoup(htm,bsp)
  pp=[i['href'] for i in soup('a') if 'vsex.in/pornstar' in i['href']]
  pornstars=[]
  for i in pp:
    if i.endswith('/'):i=i[:-1]
    ps=os.path.split(i)[1]
    pornstars.append(ps.lower())
  if pornstars:
    pornstars.sort()
    pornstars_str='_'.join(pornstars)
    return pornstars_str
  else: return ''
  
def vsex_get_pornstars(url,verbose=True):
  isfile=False
  urls=''
  # ~ if not os.path.exists('images'):os.system('mkdir -pv images')
  if os.path.exists(url):
    isfile=True
    print 'treating ',url,' as file'
    urls=[i.strip() for i in open(url).readlines() if 'vsex.in' in i]
    url=list(set(urls));urls.sort()
  else: urls=[url]
  x=imap_bar(vsex_get_pornstars_single,urls,3,False,desc='getting vsex pornstars')
  a=open('result.txt','w')
  for j in range(len(urls)):    a.write(urls[j]+' , '+x[j]+'\n')
  a.close()
  print highlight('found pornstars for %d/%d vsex pages' %( len(urls),len([i for i in x if i]) ))
    
def ariadwn(num=7,referrer_set=False):
  
  import subprocess,signal
  if sys.argv[-2] not in ['python','vd','ariadwn']: 
    try:sleepmin=float(sys.argv[-2])
    except:sleepmin=0.3
  if sys.argv[-1].isdigit(): num=int(sys.argv[-1])
  print highlight('will  run aria on linksfile.txt  %d times with sleepmin of %.1f'     %(num,sleepmin))
  for  j in  range(num):
    print highlight('running aria for %d-th time' %(j+1),'red_fore')
    # ~ cmd=HOME+'/.python_scripts/bin/aria2c --console-log-level=warn  --check-certificate=false --auto-file-renaming=false -s 5 -x 5 -j 10  -i linksfile.txt'
    cmd=''
    if referrer_set:
      cmd='aria2c --console-log-level=warn --referer=* --check-certificate=false --auto-file-renaming=false -s 5 -x 5 -j 10  -i linksfile.txt'
    else:
      cmd='aria2c --console-log-level=warn  --check-certificate=false --auto-file-renaming=false -s 5 -x 5 -j 10  -i linksfile.txt'
    # ~ print cmd
    # ~ process=subprocess.Popen(cmd)
    # ~ process=subprocess.Popen(cmd, shell=True, stdout=PIPE)
    process=subprocess.Popen(cmd, shell=True,preexec_fn=os.setsid)
    # ~ process=subprocess.Popen([HOME+'/.python_scripts/bin/aria2c','--console-log-level=warn  --check-certificate=false --auto-file-renaming=false -s 5 -x 5 -j 10  -i linksfile.txt'])
    # ~ outs, errs = process.communicate()
    time.sleep(sleepmin*60.)
    # ~ process.send_signal(signal.SIGINT)
    # ~ outs, errs = process.communicate()
    os.killpg(os.getpgid(process.pid), signal.SIGINT)
    if j>0 and not [i for i in os.listdir('.') if i.endswith('.aria2')]:
      print highlight('seems all downloads finished.exiting!'.upper(),'red')
      break
    time.sleep(1.25)

def gounlimited_logfile_get_direct_links(infile='cur_dwn',verbose=True):
  b=[i.strip() for i in open(infile).readlines() if i.startswith('goun-')]
  a=open('tmpfile','w')
  # ~ base='https://www.bitporno.com/e/'
  base='https://gounlimited.to/'
  for i in b:
    file_id=i.split('::')[3].strip()
    if file_id.endswith('.mp4'):file_id=file_id.replace('.mp4','')
    vurl=base+'embed-'+file_id+'.html'
    vname=i.split('::')[1].strip()
    a.write(vurl+','+vname+'\n')
  a.close()
  if sys.argv[-1] in ['debug']:
    os.system('cat tmpfile')
  #create aria file
  raw_info=gounlimited_get_video_urls('tmpfile',verbose=False)
  os.remove('tmpfile')
  return raw_info


def gounlimited_get_video_url_single(vurl):
  # ~ try:
    if sys.argv[-1] in ['debug']:
      print 'in gounlimited_get_video_url_single() url is ',vurl

    findname=False
    x=vurl.split(',')
    u=x[0].strip()
    vname=x[1].strip()
    if vname in ['UNKNOWN']:      findname=True;vname=''
    try:pstars=x[2].strip()
    except: pstars=''
    try:video_id=x[3].strip()
    except: video_id=''    
      
    if pstars=='UNKNOWN': pstars=''

    video_page_source=os.popen('wget -U "Mozilla/5.0 (X11; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0 (Chrome)" -q "'+u+'" -O -').read()
    soup=bs4.BeautifulSoup(video_page_source,bsp)
    # ~ x=[i.text for i in soup('script') if 'mp4' in i.text][0]
    if findname: vname=soup(class_='the_title')[0].text
    # ~ source_mp4_index=x.split('|').index('mp4')
    # ~ init_part=x.split('|')[source_mp4_index+2].strip()
    # ~ init_part=[i for i in x.split('|') if i.startswith('fs') and i[2:].isdigit()]
    # ~ if not init_part:
      # ~ print 'could not find init_part'
      # ~ return
    # ~ init_part=init_part[0]
    # ~ next_part=x.split('|')[source_mp4_index+1].strip()
    # ~ video_source_url='https://'+init_part+'.gounlimited.to/'+next_part+'/v.mp4'
    video_source_url=os.popen('youtube-dl --get-url "'+vurl+'"').read().strip()
    if not vname.startswith('PORN__'): vname='PORN__'+vname
    if not vname.endswith('.mp4'): vname+='.mp4'
    if pstars: vname=vname.replace('.mp4','---'+pstars+'.mp4')
    if video_id: vname=vname.replace('.mp4',' -'+video_id+'.mp4')
    vname=re.sub('[^A-Za-z0-9_()-.#]+', '_', vname)
    vname=vname.replace(',','_').replace('www_0xxx_ws_','')
    file_id=''
    if 'embed-' in u:      file_id=os.path.split(u)[-1].replace('embed-','').replace('.html','')
    elif 'html' in u:      file_id=u.rsplit('/')[-2].replace('embed-','').replace('.html','')
    elif u.strip().endswith(video_extensions):      file_id=u.rsplit('/')[u.rsplit('/').index('gounlimited.to')+1]
    else:      file_id=os.path.split(u)[1].strip()
    video_size=get_filesize_of_url_basic(video_source_url)
    if vname in ['PORN__Video.mp4']: return None; #file on server is a dummy
    return vname,video_source_url,file_id,video_size
  # ~ except:
    # ~ return
def porngo_logfile_get_direct_links(infile=''):
  b=[i.strip() for i in open(infile).readlines() if i.startswith('pg-')]
  a=open('tmpfile','w')
  # ~ base='https://www.bitporno.com/e/'
  base='https://www.porngo.com/videos/'
  for i in b:
    video_id=i.split('::')[3].strip()
    vurl=base+video_id.replace('___','/')+'/'
    vname='---'.join(i.split('::')[1].strip().rsplit('---')[:-1]).strip()
    vname=vname.replace('PORN__','')
    if '___' not in video_id:vurl+=vname+'/'
    a.write(vurl+'\n')
  a.close()
  if sys.argv[-1] in ['debug']:
    os.system('cat tmpfile')
  #create aria file
  raw_info=porngo_get_video_urls('tmpfile',verbose=False)
  if os.path.exists('tmpfile'):os.remove('tmpfile')
  return raw_info
  
def porngo_get_video_url_single(url):
  try:    return porngo_get_video_url_single_nocatch(url)
  except: return None
  
def porngo_get_video_url_single_nocatch(url):
  sess=requests.Session()
  soup=bs4.BeautifulSoup(sess.get(url).content,bsp)
  x= soup('a',attrs={'class':'video-links__link'})
  sites=[i['href'].split('/')[-2] for i in x if 'sites/' in i['href']]
  models=[i['href'].split('/')[-2] for i in x if 'models/' in i['href']]
  # ~ print 'title for '+url+' is '+str(soup('title'))
  vname=soup('title')[0].text.replace('- PornGO.com','')
  vname=('/'.join(vname.rsplit('/')[:-1])).strip()
  vname+='---'
  if sites: vname+='_'.join(sites)
  vname+='_'
  if models: vname+='_'.join(models)
  video_id=url.split('/')[-3]+'___'+url.split('/')[-2]
  
  
  # ~ video_links=soup(class_='video-links__list')[-1](class_='video-links__link')
  video_links=[i for i in soup(class_='video-links__link') if 'porngo.com/get_file/' in i['href']]
  # ~ for i in video_links[0]:
  quality=video_links[0].text.strip().split()[0].strip() #choose lowest
  vname+=' -PG'+video_id.split('___')[0]+'.'+quality+'.mp4'
  vname=re.sub('[^A-Za-z0-9_()-.#]+', '_', vname)
  vname=vname.replace('+','')
  if not vname.startswith('PORN__'): vname='PORN__'+vname;
  vurl=video_links[0]['href']
  xx=sess.head(vurl)
  if  xx.status_code in [302] and 'Location' in xx.headers:
    vurl=xx.headers['Location']
    # ~ print vurl
  # ~ else: print 'status_code:',str(xx.status_code)
  # ~ else: print xx.status_code,str(xx.headers)
  video_size=get_filesize_of_url_basic(vurl)
  
  return (vurl,vname,video_size,video_id)
  
def porngo_get_video_urls(infile,verbose=True):
  # ~ sortfile=True
  if not os.path.exists(infile):
    print 'given file ',infile,' not found'
    return
  b0=[i.strip() for i in open(infile).readlines() if i.startswith('pg-')]
  if b0:
    return porngo_logfile_get_direct_links(infile=infile)
  b=[i.strip() for i in open(infile).readlines() if 'porngo.com' in i.strip()]
  b=list(set(b))
  num_proc=4
  if systype in ['heroku']:num_proc=2
  if sys.argv[-1] in ['nocatch']:
    res0=imap_bar_serial(porngo_get_video_url_single_nocatch,b,num_proc,False,desc='getting porngo urls')
  else:
    res0=imap_bar(porngo_get_video_url_single,b,num_proc,False,desc='getting porngo urls')
  res=[]
  failed=[]
  for j in range(len(b)):
    if res0[j]:res.append(res0[j])
    else: failed.append(b[j])
  print 'successfully found %d/%d video urls' %(len(res),len(res0))
  raw_info=[]
  if failed:
    af=open('failed','w');
    for i in failed: af.write(i+'\n')
    af.close()
    print 'wrote %d links to ./failed' %(len(failed))
  if sys.argv[-1] not in ['nowr','nomklog','nolog']: #creates a log to remove inactive links
    if verbose: print 'creating .pglog file '
    a=open('.pglog','w')
    sep='\t:: '
    for i in res:
      vurl,vname,video_size,video_id=i
      info='pg-x'+sep+vname+sep+'%.1f' %(video_size)+' mb'+sep+video_id
      # ~ print info
      raw_info.append(info)
      a.write(info+'\n')
    a.close()
  if verbose: print 'creating linksfile.txt (aria style)'
  a=open('linksfile.txt','w')
  for i in res:
    vurl,vname,video_size,video_id=i
    if vurl:
      info=vurl+'\n\tout='+vname+'\n'
      a.write(info)
  a.close()
  return raw_info
  

def gounlimited_get_video_urls(infile,verbose=True):
  sortfile=True
  if not os.path.exists(infile):
    print 'given file ',infile,' not found'
    return
  b0=[i.strip() for i in open(infile).readlines() if i.startswith('goun-')]
  if b0:
    return gounlimited_logfile_get_direct_links(infile=infile)
  if sys.argv[-1] in ['dummy']:
    print highlight('Using dummy mode to get filenames.not actually checking verstream links','green_fore_bright')
  elif sys.argv[-1] in ['fastlog']:
    print highlight('Using fastlog mode to get file sizes.download urls are not accurate','green_fore_bright')
  
  b=[i.strip() for i in open(infile).readlines() if 'gounlimited' in i.strip()]
  b2=[i for i in b if '::' in i]
  if b2: #generated by xxxstreams_process_urls()
    b3=[i.split('::')[1].strip()+' , '+i.split('::')[0].strip() for i in b]
    b=b3
  else:
    b=[i.strip()+' , UNKNOWN' for i in b]
  b=list(set(b))
  res0=[]
  # ~ res0=imap_bar_serial(gounlimited_get_video_url_single,b,4,False,desc='getting gounlimited urls')
  valid=0
  with tqdm.tqdm(b,desc='getting gounlimited urls') as pbar1:
    for i in pbar1:
      xx=gounlimited_get_video_url_single(i)
      if xx:
        valid+=1;res0.append(xx)
        pbar1.set_description('gounlimited urls('+str(valid)+')')
        
  # ~ res=[i for i in res0 if i ]
  res=res0;raw_info=[]
  print 'successfully found %d/%d video urls' %(len(res0),len(b))
  if sys.argv[-1] not in ['nowr','nomklog','nolog']: #creates a log to remove inactive links
    if verbose: print 'creating .gounlog file '
    a=open('.gounlog','w')
    sep='\t:: '
    for i in res:
      vname,video_source_url,file_id,video_size=i
      info='goun-x'+sep+vname+sep+'%.1f' %(video_size)+' mb'+sep+file_id
      raw_info.append(info)
      # ~ print info
      a.write(info+'\n')
    a.close()
  if verbose: print 'creating linksfile.txt (aria style)'
  a=open('linksfile.txt','w')
  
  for i in res:
    vname,video_source_url,file_id,video_size=i
    if video_source_url:
      info=video_source_url+'\n\tout='+vname+'\n'
      a.write(info)
  a.close()
  return raw_info
  
def prenamer(orig_vlist,changed_vlist):
  def searchfile(fname,changed_list): #changed_list of full i.e. 0LONG/General/<fname>
   fbasename=os.path.splitext( os.path.split(fname)[1] )[0].strip() #just the base name of file
   fbasename_id=fbasename.rsplit('-')[-1].split()[0].strip()
   #~ print fbasename
   for i in range(len(changed_list)): #loop through changed,
     basename=os.path.splitext( os.path.split(changed_list[i])[1] )[0].strip()
     basename_id=basename.rsplit('-')[-1].split()[0].strip()
     if fbasename_id==basename_id:
       #~ print 'found', basename
       return changed_list[i]
   return '__NOTFOUND__'

  a=open('changelog.sh','w')
  b1=open(sys.argv[2]).readlines()
  b1e=[os.path.split(i)[1] for i in b1]
  b1d=[os.path.split(i)[0] for i in b1]

  b2=open(sys.argv[3]).readlines()
  b2e=[os.path.split(i)[1] for i in b2]
  b2d=[os.path.split(i)[0] for i in b2]

  common_files_set= set(b1).intersection(set(b2))
  changed_files_set=set(b1)-common_files_set
  changed_files_list=list(changed_files_set);changed_files_list.sort()

  changed_files_set2=set(b2)-common_files_set
  changed_files_list2=list(changed_files_set2);changed_files_list2.sort()

  missing_files=[]
  new_dirs=set()
  cmds=[]
  for f in changed_files_list: #f is moved to a different directory or removed altogether
    #~ print f
    f2=searchfile(f,changed_files_list2)
    if f2=='__NOTFOUND__':
      missing_files.append(f)
    else:
      f2d=os.path.split(f2)[0];      new_dirs.add(f2d)
      cmd='mv -v "'+f.strip()+'" "'+f2.strip()+'"\n'
      #~ print cmd
      cmds.append(cmd)


  a.write('## CREATE DIRS::\n\n')
  new_dirs_list=list(new_dirs);new_dirs_list.sort()
  for i in new_dirs_list: a.write('mkdir -pv "'+i+'"\n')
  a.write('## MOVE FILES::\n\n')
  for i in cmds: a.write(i)
  a.write('## MISSING FILES::\n\n')
  for i in missing_files: a.write('rm -v "'+i.strip()+'"\n')
  a.write('\n\n\n##Clean up empty directories\nfind . -type d -empty -delete\n')
  a.close()



def video_satisfies_search_conditions(v,conditions_list):
  vname=v['title'].lower()
  vlen=float(v['duration'])/60.
  vaspect=float(v['aspect_ratio'])
  v_formats=v['available_formats']
  vid_user=(v['owner.username'].lower(),v['owner.screenname'].lower(),v['owner'])
  v_id=v['id']
  condition_satisfied=True

  for condition in conditions_list:
    if not condition_satisfied: break

    if condition[:2]=='0u':#means user eg: mehulladvashop or MBL for Wynonna Earp S02E08-x5vb5v6.mp4
      #~ print 'in 0u'
      condition_satisfied=False
      search_user=condition[2:].strip().lower()
      #one of vid_user[0],vid_user[1] must match search_user
      if (search_user in vid_user[0]) or (search_user in vid_user[1])or (search_user in vid_user[2]):
	condition_satisfied=True

    elif condition[:2]=='0l': #means length in minutes
      #~ print 'in 0l'
      condition_satisfied=False
      search_len=int(condition[3:])
      if condition[2]=='g': #means vid should be greater than
	if vlen>search_len:
	  condition_satisfied=True
      elif condition[2]=='l': #means vid should be less than
	if vlen<=search_len:
	  condition_satisfied=True
      else:
	print 'option in search can be lg(greater than) or ll(less than equal to)'

    elif condition[:2]=='0a':#stands for aspect ratio, to diffrentiate 176x108 from 176x144
      #~ print 'in 0a'
      condition_satisfied=False
      if condition[2]=='g':
	if vaspect<1.5:	condition_satisfied=True
      elif condition[2]=='l':
	if vaspect>=1.5: condition_satisfied=True
      elif condition[2]=='0':
	if 'l2' in v_formats:  condition_satisfied=True
      else:
	print 'option in search can be 0ag(aspect ratio less than 1.5, eg 176x144) or 0al(aspect ration greater than 1.5 eg 176x108). It can be 0a0 which only selects the 144p videos'

      #~ print 'in 0a',condition_satisfied

    else: #means option should be in video url or video name
      #~ print 'in else'
      condition_satisfied=False
      #~ condition=condition.lower()
      if (condition.lower() in vname) or (condition in v_id):
	condition_satisfied=True

  #~ print 'outside' ,condition_satisfied
  return condition_satisfied

def search_dailymotion_vids():
  start_time=time.time()
  if len(sys.argv)>3:
    if sys.argv[2] in ['v','verbose']:
      verbose=1;writefile=0;searchterm=sys.argv[3];
    elif sys.argv[2] in ['vwf','verbose_writefile']:
      print 'Writing verbose results to /tmp/dailymotion_search_results'
      verbose=1;writefile=1;searchterm=sys.argv[3]
    elif sys.argv[2] in ['wf','writefile']:
      print 'Writing results to /tmp/dailymotion_search_results'
      verbose=0;writefile=1;searchterm=sys.argv[3]
  else:
    verbose=0;searchterm=sys.argv[2];writefile=0

  if writefile>0:
    a_wf=open('/tmp/dailymotion_search_results','w')
    a_wf_plaintext=open('/tmp/dailymotion_search_results_plaintext','w')

  #~ a2=open('/media/aniru/Data/Videos/Series/lists/dailymotion_videos_list.json','r')
  #~ videos_list=json.load(a2);a2.close()
  a2=open('/media/aniru/Data/Videos/otherStuff/downloading/dailymotion_jsons/dailymotion_videos_list.pickle','r')
  videos_list=cPickle.load(a2);a2.close()
  #~ videos_list.sort(cmp=lambda a,b: cmp(a['title'].lower(),b['title'].lower()))

  conditions_list=[] #list of search terms
  if '_' in searchterm: conditions_list=searchterm.split('_')
  else: conditions_list=[searchterm]

  results=filter(lambda v:video_satisfies_search_conditions(v,conditions_list),videos_list)
  #~ results=[]
  #~ for v in videos_list:
    #~ if video_satisfies_search_conditions(v,conditions_list):
      #~ results.append(v)

  if verbose:print 'searching took ',time.time()-start_time,' seconds'
  def mycmp_v(a,b):
    if a['owner.screenname']==b['owner.screenname']:      return cmp(a['title'],b['title'])
    else:      return cmp(a['owner.screenname'].lower(),b['owner.screenname'].lower())

  results.sort(cmp=mycmp_v)

  key='num '+colorama.Back.BLACK+colorama.Style.NORMAL+'video_id'+colorama.Back.RESET+' '+colorama.Style.DIM+colorama.Fore.BLACK+'owner.screenname  '+colorama.Fore.RESET+':'+colorama.Style.RESET_ALL+colorama.Fore.CYAN+'duration(min)'+colorama.Fore.BLUE+colorama.Back.BLACK+colorama.Style.BRIGHT+'(176x144)'+colorama.Back.RESET+colorama.Style.RESET_ALL+colorama.Fore.RED+'(no 144p)'+colorama.Fore.RESET+' :: '+colorama.Fore.RESET+colorama.Style.BRIGHT+'video_title'+colorama.Style.RESET_ALL
  if verbose: key+=' : aspect_ratio : created_time'#: user_id'
  if len(results)>0:
    ts=key+colorama.Style.DIM+colorama.Back.RESET+'\n                                                                                      '+colorama.Back.RESET+colorama.Style.RESET_ALL

    if writefile:
      if type(ts)==unicode:ts=ts.encode('ascii','replace')
      a_wf.write(ts+'\n')
    else:print ts
  else:
    ts='nothing found in database '
    #~ print ts
    if writefile:
      if type(ts)==unicode:ts=ts.encode('ascii','replace')
      a_wf.write(ts+'\n')
    else:print ts
  result_cnt=0
  for i in results:
    result_cnt+=1
    info_str=str(result_cnt).rjust(3,'0')+' '
    info_str+=colorama.Back.BLACK+colorama.Style.NORMAL+i['id']+colorama.Back.RESET+'  '+colorama.Style.DIM+colorama.Fore.BLACK+i['owner.screenname']
    if verbose:
      info_str+='('+i['owner']+')'
    info_str+=colorama.Fore.RESET+colorama.Style.RESET_ALL+' '
    #~ info_str_plain=i['id']+'\t##  '+i['owner.screenname']+' '
    info_str_plain='dailymotion.com/video/'+i['id']+'\t##  '+i['owner.screenname']+' '
    if 'l2' not in i['available_formats']:
      info_str+=colorama.Fore.RED
    else:
      if i['aspect_ratio']<1.5:info_str+=colorama.Style.BRIGHT+colorama.Fore.BLUE+colorama.Back.BLACK
      else:info_str+=colorama.Fore.CYAN

    info_str+=str(int(i['duration'])/60)+colorama.Fore.RESET+colorama.Back.RESET+colorama.Style.RESET_ALL
    info_str+='  '+colorama.Style.BRIGHT+i['title']+colorama.Style.RESET_ALL
    info_str_plain+=str(int(i['duration'])/60)+'  '+i['title']
    if verbose:
      print 'available formats: '+str(i['available_formats'])
      time_str=time.ctime(int(i['created_time']))
      tss=time_str.split()
      time_str=' '.join([tss[1],tss[2],tss[4]])
      info_str+=' : '+str(i['aspect_ratio'])[:4]+colorama.Fore.RESET+' : '+time_str#+' : '+i['owner']
      info_str_plain+=' : '+str(i['aspect_ratio'])[:4]+' : '+time_str#+' : '+i['owner']
    if 'l2' in i['available_formats'] and i['aspect_ratio']<1.5:info_str_plain+='  HQ'
    if writefile:
      ts=info_str
      if type(ts)==unicode:ts=ts.encode('ascii','replace')
      a_wf.write(ts+'\n')
      ts=info_str_plain
      if type(ts)==unicode:ts=ts.encode('ascii','replace')
      a_wf_plaintext.write(ts+'\n')
    else:
      ts=info_str
      if type(ts)==unicode:ts=ts.encode('ascii','replace')
      print ts
  ts='\nTotal results:'+str(len(results))
  if len(results)>0:print ts
  if writefile:
    a_wf.write(ts)
    a_wf_plaintext.write('\n\n##'+ts.strip())
    a_wf.close()

  
def lavinmovie_search(movie=False):
  if 'mov' in sys.argv[-1]:
    print 'searching for movies'.upper()
    movie=True
  searchterm=sys.argv[2]
  if searchterm in ['pp'] and IS_LOCAL_MACHINE:
    searchterm=sys.argv[3]
    pdir='/media/aniru/Data/p'
    ss=searchterm.split('_')
    b=open(pdir+'/.vlist').readlines();b=[i.strip() for i in b]
    ff=[]
    for i in b:
      # ~ i=i.lower()
      if i.startswith('./'):i=i[2:]
      found=True
      for j in ss:
        j=j.lower() 
        if j not in i.lower():
          found=False
          break
      if found:ff.append(pdir+'/'+i+'\n')
    print 'found ',len(ff),' matches for ',searchterm
    if ff:
      a=open('/tmp/vlist','w');a.writelines(ff);a.close()
      cmd='/usr/local/bin/mpv --playlist=/tmp/vlist  --shuffle --osd-level=3 --osd-color=1/0.1 1>/dev/null 2>/dev/null'
      # ~ cmd='/usr/local/bin/mpv'
      # ~ args=' --playlist=/tmp/vlist --glsl-shaders=~~/prescalers/n128.hook --shuffle --osd-level=3 --osd-color=1/0.1 --volume=110'
    # ~ print cmd
      # ~ os.execl(cmd,'')
      os.popen2(cmd)
    return
    
  if not movie:
    print 'trying to find lavinmovie,upload8 serial link for ',searchterm
    sites=['http://dl.lavinmovie.net/series/','http://dl2.lavinmovie.net/series/','http://dl5.lavinmovie.net/Series/','http://dl4s.lavinmovie.net/Series/','http://dl6.lavinmovie.net/Series/','http://dl7.lavinmovie.net/Series/','http://dl8.lavinmovie.net/Series/','http://dl9.lavinmovie.net/Series/','http://dl10.lavinmovie.net/Series/','http://mc1.movie3enter.com/files/d4/serial1/','http://mc2.movie3enter.com/files/serial/mc1/','http://dl5.heyserver.in/serial/','http://dl8.heyserver.in/serial/','http://79.127.126.110/Serial/','http://dl.meliupload.com/mersad/serial/','http://dl1.zardl.info/Series/']
  else:
    print 'trying to find lavinmovie movie link for ',searchterm
    # ~ for svdl go to http://dl2.svdl.ir/Serial/KH/A%20Series%20of%20Unfortunate%20Events/S01/720x265 and then to parent
    sites=['http://dl6.lavinmovie.net/Movies/2018/','http://dl.lavinmovie.net/movie/2018/','http://dl2.lavinmovie.net/movie/2018/','http://dl4.lavinmovie.net/movie/2018/','http://dl5.lavinmovie.net/Movies/2018/','http://dl7.lavinmovie.net/Movies/2018/','http://dl6.lavinmovie.net/Movies/2019/','http://dl.lavinmovie.net/movie/2019/','http://dl2.lavinmovie.net/movie/2019/','http://dl4.lavinmovie.net/movie/2019/','http://dl5.lavinmovie.net/Movies/2019/','http://dl7.lavinmovie.net/Movies/2019/','http://dl8.lavinmovie.net/Movies/2018/','http://dl8.lavinmovie.net/Movies/2019/'
    ,'http://dl9.lavinmovie.net/Movies/2019/','http://dl9.lavinmovie.net/Movies/2018/']
  
  allsearchelem=searchterm.split('_')
  searchterm_re=re.compile('[^\}]*'.join(['']+allsearchelem))
  
  
  b=[]
  #~ widgets = ['Getting lavinmovie sites:',progressbar.Percentage(),progressbar.Bar('>'), ':: ', progressbar.ETA()]
  #~ pbar=progressbar.ProgressBar(widgets=widgets,maxval=len(sites)).start() #cant use float so use int(1000*percent) as approx same
  #~ cnt=0
  print 'getting lavinmovie sites'
  contents=''
  if systype in ['xshellz']:
    print 'using thread mode on xshellz'
    contents=imap_bar(url_get_content, sites, 3,True)
  else:
    contents=imap_bar(url_get_content, sites, len(sites))
  # ~ nPool=multiprocessing.dummy.Pool(len(sites))
  # ~ contents=nPool.map(url_get_content,sites)
  print 'got sites'
  
  for j in range(len(sites)):
    soup=bs4.BeautifulSoup(contents[j],bsp)
    aa=[i['href'] for i in soup('a')]# if i.has_attr('title')]
    aa=[sites[j]+i for i in aa if not i.startswith(sites[j])]
    b.extend(aa)
    #~ cnt+=1;pbar.update(cnt)
  b=[i.strip() for i in b if searchterm_re.match(i.lower()) ]
  b=list(set(b));b.sort();
  for i in b:
    print '%s\n' %(i)
    #~ if '/movie/' in i or '/Movies/' in i: continue
    #~ soup=bs4.BeautifulSoup(requests.get(i).content,bsp)
    #~ aa=[j['href'] for j in soup('a') if j.has_attr('title')]
    #~ aa=[i+j for j in aa if not j.startswith(i)]
    #~ for k in aa: print k
    #~ print '-----------'
  # ~ nPool.close();nPool.join()
  
def get_dailymotion_video_user(debug=False):
  vid=sys.argv[-1]
  print 'trying to get user for video ',vid
  if 'dailymotion.com' in vid: vid=os.path.split(vid)[1]
  x=requests.get('https://api.dailymotion.com/video/'+vid).json()
  if debug: print str(x)
  print 'title:%s\nuser:%s' %(x['title'],x['owner'])
  return
def process_dailymotion_jsons():
  #~ Interesting fields in api call for dailymotion
  #~ ?fields=aspect_ratio,available_formats,created_time,description,duration,id,owner,owner.fullname,owner.screenname,owner.username,owner.videos_total,thumbnail_180_url,title,url,
  #~ for filter
  #~ longer_than
  #SAMPLE CALL (for all videos loonger than 8 minutes of <username>, of <page_num> with these fields
  #~ https://api.dailymotion.com/videos?owner=<username>&page=<page_num>&limit=100&fields=aspect_ratio,available_formats,created_time,description,duration,id,owner,ownerunname,owner.username,owner.videos_total,thumbnail_180_url,title,url&longer_than=8
  # ~ os.chdir('/media/aniru/Data/Videos/Series/lists/dailymotion_jsons')
  os.chdir('/media/aniru/Data/Videos/otherStuff/downloading/dailymotion_jsons')
  if len(sys.argv)>2 and sys.argv[2]in ['wj','wget_jsons']:
    if len(sys.argv)<4:
      print 'need to give username too'
    else:
      username=sys.argv[3]
      if 'dailymotion.com/' in username: username=os.path.split(username)[1].strip()
      current_page_num=1
      print 'wget..ting jsons for '+colorama.Fore.RED+username+colorama.Fore.RESET
      query='wget "https://api.dailymotion.com/videos?owner='+username+'&page='+str(current_page_num)+'&limit=100&fields=aspect_ratio,available_formats,created_time,description,duration,id,owner,owner.screenname,owner.username,owner.videos_total,thumbnail_180_url,title,url" -O '+username+'_1.json'
      #~ print colorama.Style.DIM+query
      os.system(query)
      #~ print colorama.Style.RESET_ALL
      #now check if there are more videos after this page
      while json.load(open(username+'_'+str(current_page_num)+'.json'))['has_more']:
	current_page_num+=1
	query='wget "https://api.dailymotion.com/videos?owner='+username+'&page='+str(current_page_num)+'&limit=100&fields=aspect_ratio,available_formats,created_time,description,duration,id,owner,owner.screenname,owner.username,owner.videos_total,thumbnail_180_url,title,url" -O '+username+'_'+str(current_page_num)+'.json'
	#~ print query
	os.system(query)

      #last page has total number of videos for user
      try:
	total_videos=json.load(open(username+'_'+str(current_page_num)+'.json'))['total']
	print 'User ',username,' has ',str(total_videos),' videos in ',str(current_page_num),' pages'
      except:
	print 'Could not find total videos from last page of '+username+'!!!'
	sys.exit(1)

  else:
    print 'Adding new videos from jsons in folder to database '
    #~ os.chdir('')
    a=os.listdir('.');
    a=[i for i in a if '.json' in i ];a.sort()
    a2=open('/media/aniru/Data/Videos/otherStuff/downloading/dailymotion_jsons/dailymotion_videos_list.pickle','r')

    start_time=time.time()
    try:resl=cPickle.load(a2);
    except:
      print 'could not load database'
      sys.exit(1)
    a2.close()
    initial_number_of_videos=len(resl)
    resl_ids=[j['id'] for j in resl]
    resl_ids_set=set(resl_ids)


    for ii in range(len(a)):#for each json file
      i=a[ii]
      try:
	af=open(i)
	r=json.load(af)
	af.close()
      except:
	fsize=os.stat(i).st_size
	if fsize==0:
	  os.remove(i)
	  print 'deleted zero size file ',i
	else:
	  print 'error processing ',i,' filesize:',
	  sys.exit(1)
      tmp_d={}
      for v in r['list']:tmp_d[v['id']]=v
      rem=set(tmp_d.keys()).difference(resl_ids_set)
      for v_id in rem:	resl.append(tmp_d[v_id])

    a2=open('/media/aniru/Data/Videos/otherStuff/downloading/dailymotion_jsons/dailymotion_videos_list.pickle','w')
    cPickle.dump(resl,a2,protocol=2);a2.close()

    end_time=time.time()
    time_taken=int(end_time-start_time)
    print '\nAdded ',len(resl)-initial_number_of_videos,' new videos to database from ',len(a),' .json files. Totally ',len(resl),' videos in database now'
    print 'Time taken: %d min %d sec'  %(time_taken/60,time_taken%60)



def move_to_dirs():
  b=open('.vlist').readlines()
  a2=open('cmdlist','w')
  dirs=[i for i in b if i[:2]=='##']
  for d in dirs:os.system('mkdir -p "'+d.strip()+'"')
  dirsd={}
  for i in dirs:
    try:
      numerical_id=int(i.strip().split()[-1])
    except:
      print 'could not find numerical id for comment ',i,' :exiting'
      sys.exit(1)

    subfolder_name=i.split()[0].replace('##','')
    dirsd[numerical_id]=subfolder_name
  b=[i for i in b if  i[0]!='#'] #ignore comments
  for i in b:
    try:
      numerical_id=int(i.rsplit(',')[-1].strip())
    except:
      print 'could not get numerical id for ',i,' :skipping'
      continue
    filename=','.join(i.rsplit(',')[:-1]).strip()
    subdirname=dirsd[numerical_id]
    cmd='mv "'+filename+'" "'+subdirname+'"'
    a2.write(cmd+'\n');
    #~ os.system(cmd)


def modify_screen_brightness():
  br_file='/sys/class/backlight/amdgpu_bl0/brightness'
  if not os.path.exists(br_file): br_file='/sys/class/backlight/amdgpu_bl1/brightness'
  if not os.path.exists(br_file): br_file='/sys/class/backlight/intel_backlight/brightness'
  cur_brightness=int(os.popen('cat '+br_file).read().strip())


  if sys.argv[2]=='increase':
    new_brightness=cur_brightness+int(sys.argv[3])
    if new_brightness>255:
      print 'Maximum brightness reached, not changing'
    else:
      os.system('sudo su -c "echo '+str(new_brightness)+' >'+br_file+'"')
      print 'Increased brightness from ',cur_brightness,' to ',new_brightness
  elif sys.argv[2]=='decrease':
    cur_brightness=int(os.popen('cat '+br_file).read().strip())
    new_brightness=cur_brightness-int(sys.argv[3])
    if new_brightness<1:
      print 'Minimum brightness reached, not changing'
    else:
      os.system('sudo su -c "echo '+str(new_brightness)+' >'+br_file+'"')
      print 'Reduced brightness from ',cur_brightness,' to ',new_brightness
  else:
    try:
      new_brightness=int(sys.argv[2])
      cur_brightness=int(os.popen('cat '+br_file).read().strip())
      if new_brightness<1 or new_brightness>255:
        print 'value must be between 1 and 255'
        return
      else:
        os.system('sudo su -c "echo '+str(new_brightness)+' >'+br_file+'"')
        print 'Changed brightness from ',cur_brightness,' to ',new_brightness
    except:
      print 'option must be one of <increase/decrease/absolute_value>'


def start_mpv():
  myproc=os.popen3('mpv --idle --input-ipc-server=/tmp/mpvsocket.myimview --config-dir="/home/aniru/.mpv/myimview" --osd-level=3 --loop --glsl-shaders="~~/prescalers/shaders/FSRCNNX2.glsl" --image-display-duration=10')  
  os.system("touch "+TMP_MPV_IMAGEVIEWER_LOCKFILE)
  # ~ os.system("sendkey.py 'Pale' 'y'")
  return
def mpv_is_running():
  mpvcheckcmd='ps aux|grep mpvsocket.myimview'
  if os.path.exists(TMP_MPV_IMAGEVIEWER_LOCKFILE) or len(os.popen(mpvcheckcmd).readlines())>2:
    return True
  else:
    return False
def cleanup_tmpfile():  
  if os.path.exists(TMP_MPV_IMAGEVIEWER_LOCKFILE): 
    print 'DELETING TMP_LOCKFILE!!'
    os.remove(TMP_MPV_IMAGEVIEWER_LOCKFILE)
  return
import atexit
atexit.register(cleanup_tmpfile);

def find_all_images_in_subdirs(base='.',write_vlist=True):
  xr=[]
  for root,dirs,files in os.walk(base,topdown=True,followlinks=False):
    [xr.append(os.path.join(root,f)) for f in files if (f.endswith(image_extensions) ) ]
  xr.sort()
  if write_vlist:
    a=open('.vlist','w')
    for i in xr: a.write(i+'\n')
    a.close()
  return xr
  
def mpvimageview(command='',curpath='',filename=''):
  fname=''
  print 'in mpvimageview, git cmd: ',command
  if not command:

    im=[]
    #find first viable image file
    if not os.path.exists('.vlist'):
      im=find_all_images_in_subdirs(write_vlist=False)
    else:
      im=[i.strip() for i in open('.vlist').readlines() if i.strip()]
      
    if not im:
      print 'got raw command and no iamge found in any subdir. exiting!!'
      sys.exit(1)
    fname=im[0];command='loadfile'
  if command.endswith(image_extensions):
    fname=command;command='loadfile'

  if command=='globalimagerandom':
    os.chdir('..');
    im=[]
    if os.path.exists('.vlist'): im=[i.strip() for i in open('.vlist').readlines() if i.strip()]
    else: im=find_all_images_in_subdirs(write_vlist=False)
    
    if not im:
      print 'got raw command and no iamge found in any subdir. exiting!!'
      sys.exit(1)
    im=os.path.abspath(random.choice(im))
    fname=im;command='loadfile'
    rdir=os.path.abspath(im)[0]
    rdir=im.replace('/media/aniru/Data/pim/avif/','')
    cmd= 'echo \'{ "command": ["set_property_string", "osd-status-msg","Changed to random_dir: '+rdir+'"] }\' | socat - /tmp/mpvsocket.myimview'
    os.system(cmd)
    os.system("touch "+TMP_MPV_IMAGEVIEWER_LOCKFILE)

  # ~ import SocketServer
  # ~ class MyTCPHandler(SocketServer.BaseRequestHandler):
    # ~ def handle(self):

  
  if mpv_is_running():
    if command in ['nextimage','nextimagerandom']:
      print highlight('insider nextimage, got curpath: '+curpath+', filename: '+filename)
      curdir=os.path.split(curpath)[0]
      if not curdir: curdir='.'
      curdir=os.path.abspath(curdir)
      curdir_files=[i for i in os.listdir(curdir) if i.endswith(image_extensions)]
      curdir_files.sort()
      next_file='';
      skip_osd=False
      if filename in curdir_files:
        next_index=curdir_files.index(filename)+1
        #pick random index if needed
        if command in ['nextimagerandom']: next_index=random.choice(range(len(curdir_files)))
        if next_index>=len(curdir_files):
          cmd= 'echo \'{ "command": ["set_property_string", "osd-status-msg","This is the LAST image"] }\' | socat - /tmp/mpvsocket.myimview'
          print cmd;os.system(cmd)
          skip_osd=True
          next_index=len(curdir_files)-1
        next_file=curdir_files[next_index]

      if not skip_osd:
        cmd= 'echo \'{ "command": ["set_property_string", "osd-status-msg","'+next_file+'"] }\' | socat - /tmp/mpvsocket.myimview'
        if type(cmd)==unicode:      cmd=cmd.encode('ascii','replace')
        print cmd;os.system(cmd)
      
      cmd='echo \'{ "command": ["loadfile","'+curdir+'/'+next_file+'"] }\' | socat - /tmp/mpvsocket.myimview'
      if type(cmd)==unicode:      cmd=cmd.encode('ascii','replace')
      print cmd;os.system(cmd)
      
    elif command=='previmage':
      curdir=os.path.split(curpath)[0]
      if not curdir: curdir='.'
      curdir=os.path.abspath(curdir)
      curdir_files=[i for i in os.listdir(curdir) if i.endswith(image_extensions)]
      curdir_files.sort()
      prev_file=''
      skip_osd=False
      if filename in curdir_files:
        prev_index=curdir_files.index(filename)-1
        if prev_index<0:
          cmd= 'echo \'{ "command": ["set_property_string", "osd-status-msg","This is the FIRST image"] }\' | socat - /tmp/mpvsocket.myimview'
          print cmd;os.system(cmd)
          skip_osd=True
          prev_index=0
        prev_file=curdir_files[prev_index]
      if not skip_osd:
        cmd= 'echo \'{ "command": ["set_property_string", "osd-status-msg","'+prev_file+'"] }\' | socat - /tmp/mpvsocket.myimview'
        if type(cmd)==unicode:      cmd=cmd.encode('ascii','replace')
        print cmd;os.system(cmd)
     
      cmd='echo \'{ "command": ["loadfile","'+curdir+'/'+prev_file+'"] }\' | socat - /tmp/mpvsocket.myimview'
      if type(cmd)==unicode:      cmd=cmd.encode('ascii','replace')
      print cmd;os.system(cmd)
      
    elif command=='loadfile':
      cmd='echo \'{ "command": ["loadfile","'+fname+'"] }\' | socat - /tmp/mpvsocket.myimview'
      print cmd;os.system(cmd)
    elif command=='showosd':
      curdir=os.path.split(curpath)[0]
      curdir_name=os.path.split(curdir)[1]
      image_files=[i for i in os.listdir(curdir) if i.endswith(image_extensions)];image_files.sort()
      filename_index=''
      if filename in image_files:
        filename_index=image_files.index(filename)+1
      osd_text=filename+' -- %d of %d -- dir: %s' %(filename_index,len(image_files),curdir_name)
      print 'osd_text: '+osd_text
      cmd= 'echo \'{ "command": ["set_property_string", "osd-status-msg","'+osd_text+'"] }\' | socat - /tmp/mpvsocket.myimview'
      print cmd;x=os.popen3(cmd);
      #reload file
      cmd='echo \'{ "command": ["loadfile","'+curpath+'"] }\' | socat - /tmp/mpvsocket.myimview'
      print cmd;os.system(cmd)
      
    elif command=='nextdir':
      # ~ cmd= 'echo \'{ "command": ["set_property_string", "osd-status-msg","in early next_dir"] }\' | socat - /tmp/mpvsocket.myimview';
      # ~ print cmd;os.system(cmd);
      curdir=os.path.split(curpath)[0]
      if not curdir: curdir='.'
      curdir=os.path.abspath(curdir)
      # ~ print 'curdir: ',curdir
      curdir_name=os.path.split(curdir)[1]
      # ~ print 'curdir_name: ',curdir_name
      curdir_pardir_base=os.path.split(curdir)[0]
      # ~ print 'curdir_name: ',curdir_name,'curdir_pardir_base: ',curdir_pardir_base
      #make list of dirs in parent dir
      next_dir=''
      pardir_dirs=[i for i in os.listdir(curdir_pardir_base) if os.path.isdir(curdir_pardir_base+'/'+i)]
      pardir_dirs.sort()
      # ~ print 'pardir_dirs: ',pardir_dirs
      image_files=[]
      if not image_files:
      # ~ if curdir_name in pardir_dirs:
        next_dir_index=pardir_dirs.index(curdir_name)+1
        if next_dir_index>=len(pardir_dirs): next_dir_index=len(pardir_dirs)-1
        next_dir=curdir_pardir_base+'/'+pardir_dirs[next_dir_index]
        #check that nextdir has image files
        image_files=[i for i in os.listdir(next_dir) if i.endswith(image_extensions)];image_files.sort()
        fname=next_dir+'/'+image_files[0]
        # ~ if not image_files:
        cmd= 'echo \'{ "command": ["set_property_string", "osd-status-msg","got next_dir'+next_dir+'"] }\' | socat - /tmp/mpvsocket.myimview'      
        print cmd;os.system(cmd)
        
      cmd='echo \'{ "command": ["loadfile","'+fname+'"] }\' | socat - /tmp/mpvsocket.myimview'
      print cmd;os.system(cmd)
    elif command=='prevdir':
      # ~ cmd= 'echo \'{ "command": ["set_property_string", "osd-status-msg","in early next_dir"] }\' | socat - /tmp/mpvsocket.myimview';
      # ~ print cmd;os.system(cmd);
      curdir=os.path.split(curpath)[0]
      if not curdir: curdir='.'
      curdir=os.path.abspath(curdir)
      # ~ print 'curdir: ',curdir
      curdir_name=os.path.split(curdir)[1]
      # ~ print 'curdir_name: ',curdir_name
      curdir_pardir_base=os.path.split(curdir)[0]
      # ~ print 'curdir_name: ',curdir_name,'curdir_pardir_base: ',curdir_pardir_base
      #make list of dirs in parent dir
      prev_dir=''
      pardir_dirs=[i for i in os.listdir(curdir_pardir_base) if os.path.isdir(curdir_pardir_base+'/'+i)]
      pardir_dirs.sort()
      # ~ print 'pardir_dirs: ',pardir_dirs
      image_files=[]
      if not image_files:
      # ~ if curdir_name in pardir_dirs:
        prev_dir_index=pardir_dirs.index(curdir_name)-1
        if prev_dir_index<0: prev_dir_index=0
        prev_dir=curdir_pardir_base+'/'+pardir_dirs[prev_dir_index]
        #check that nextdir has image files
        image_files=[i for i in os.listdir(prev_dir) if i.endswith(image_extensions)];image_files.sort()
        fname=prev_dir+'/'+image_files[0]
        # ~ if not image_files:
        cmd= 'echo \'{ "command": ["set_property_string", "osd-status-msg","got prev_dir'+prev_dir+'"] }\' | socat - /tmp/mpvsocket.myimview'      
        print cmd;os.system(cmd)
        
      cmd='echo \'{ "command": ["loadfile","'+fname+'"] }\' | socat - /tmp/mpvsocket.myimview'
      print cmd;os.system(cmd)
  else:
    start_mpv()
    sleepsec=0.3
    print 'started mpv..sleeping for '+str(sleepsec)+'s'
    time.sleep(sleepsec)
    if fname: mpvimageview(fname)
    else: mpvimageview(command)

  # ~ HOST, PORT = "localhost", portnum
  # ~ SocketServer.TCPServer.allow_reuse_address = True
  # ~ print 'def',default_string
  # ~ server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)
  # ~ server.serve_forever()


def mark_file(curpath,filename,numerical_index):
  mark_properties_dir=  {
    0:'foo',
    1:'foo',
    2:'bar',
    3:'foo',
    4:'foo',
    5:'foo',
    6:'foo'
  }
  # ~ print 'in mark_file() got : ',curpath,filename,numerical_index
  if numerical_index==0:
    xpath=str(numerical_index)
    curdir=os.path.split(curpath)[0]
    if not curdir: curdir='.'
    # ~ print 'curdir: '+curdir
    x=[i for i in os.listdir(curdir) if os.path.isdir(curdir+'/'+i) and i[0].isdigit()]
    x.sort()
    info=''    
    for i in x:      info+=i[:10]+','
    if not info: info='NO_DIRS'
    cmd2='echo show-text \"'+info+'\" | socat - /tmp/mpvsocket.mine'
    # ~ print cmd2
    os.system(cmd2)
    # ~ curdir=os.path.split(curpath)[0]
    # ~ fname_base=os.path.splitext(filename)[0]
    # ~ chapter_file=os.path.split(curpath)[0]+'/'+fname_base+'_chapter.xml'
    # ~ if os.path.exists(chapter_file):
      # ~ cmd='mkvpropedit "'+os.path.split(curpath)[0]+'/'+filename+'" -c "'+chapter_file+'"'
      # ~ print cmd
      # ~ os.system(cmd)
    # ~ cmd2='echo show-text \"chapters_written_to_file\" | socat - /tmp/mpvsocket'
    # ~ print cmd2
    # ~ os.system(cmd2)
  elif numerical_index in range(1,15):
    curdir=os.path.split(curpath)[0]
    if not curdir: curdir='.'
    xpath=str(numerical_index)
    x=[i for i in os.listdir(curdir) if os.path.isdir(curdir+'/'+i) and i.startswith(xpath+'_')]
    if x:
      newpath=curdir+'/'+x[0]
    else:
      newpath=curdir+'/'+xpath
    if newpath.startswith('/'): newpath='.'+newpath
    if not os.path.exists(curpath): return
    if not os.path.exists(newpath): os.system('mkdir -p "'+newpath+'"')
    cmd='mv "'+curpath+'" "'+newpath+'"'
    print curpath+' -> '+highlight(newpath,'blue')
    os.system(cmd)
    info='   -->'+newpath
    cmd2='echo show-text \"'+info+'\" | socat - /tmp/mpvsocket.mine'
    # ~ print cmd2
    os.system(cmd2)
  elif numerical_index in ['nextim','nextimage']:
    print 'trying to skip to next image'
    #touch/create lckfile since if we get this, it means mpv is already started
    os.system("touch "+TMP_MPV_IMAGEVIEWER_LOCKFILE)
    mpvimageview('nextimage',curpath,filename)
  elif numerical_index in ['nextimagerandom']:
    print 'trying to skip to next image'
    #touch/create lckfile since if we get this, it means mpv is already started
    os.system("touch "+TMP_MPV_IMAGEVIEWER_LOCKFILE)
    mpvimageview('nextimagerandom',curpath,filename)
  elif numerical_index in ['globalimagerandom']:
    print 'trying to skip to next image'
    #touch/create lckfile since if we get this, it means mpv is already started
    os.system("touch "+TMP_MPV_IMAGEVIEWER_LOCKFILE)
    mpvimageview('globalimagerandom',curpath,filename)
  elif numerical_index in ['previm','previmage']:
    print 'trying to skip to prev image'
    #touch/create lckfile since if we get this, it means mpv is already started
    os.system("touch "+TMP_MPV_IMAGEVIEWER_LOCKFILE)
    mpvimageview('previmage',curpath,filename)
  elif numerical_index in ['showosd']:
    os.system("touch "+TMP_MPV_IMAGEVIEWER_LOCKFILE)
    mpvimageview('showosd',curpath,filename)
  elif numerical_index in ['firstim']:
    os.system("touch "+TMP_MPV_IMAGEVIEWER_LOCKFILE)
    mpvimageview('firstim',curpath,filename)
  elif numerical_index in ['lastim']:
    os.system("touch "+TMP_MPV_IMAGEVIEWER_LOCKFILE)
    mpvimageview('lastim',curpath,filename)
  elif numerical_index in ['nextdir']:
    print 'trying to go to next dir in parent director'
    #touch/create lckfile since if we get this, it means mpv is already started
    os.system("touch "+TMP_MPV_IMAGEVIEWER_LOCKFILE)
    mpvimageview('nextdir',curpath,filename)
  elif numerical_index in ['prevdir']:
    print 'trying to go to prev dir in parent director'
    #touch/create lckfile since if we get this, it means mpv is already started
    os.system("touch "+TMP_MPV_IMAGEVIEWER_LOCKFILE)
    mpvimageview('prevdir',curpath,filename)

    
def imfap_get_organizer_pages(infile=''):
  b=[i.strip() for i in open(infile).readlines() if 'imagefap.com' in i]
  toget=[]
  os.system('mkdir -pv htm')
  for h in tqdm.tqdm(b,desc='getting htm pages'):
    soup=bs4.BeautifulSoup(requests.get(h).content,bsp)
    
    page_title=os.path.split(h)[1]
    pagenum=0
    if '&page=' in page_title: pagenum=int(page_title.split('&page=')[1].strip())+1
    page_title=page_title.split('?')[0]
    if pagenum: page_title+=str(pagenum)    
    
    stylesheet=soup('link',attrs={'rel':'stylesheet'})[0]
    stylesheet['href']='index_files/style.css'
    ims=soup('img',attrs={'src':re.compile('https.*mini.*jpg')})
    for im in ims:
      toget.append(im['src'])
      im['src']='images/mini/'+os.path.split(im['src'])[1]
    
    str_soup=str(soup)
    str_soup=str_soup.replace('</body>','\n<script type="text/javascript" src="index_files/common.js"></script>\n</body>')
    a2=open('htm/'+page_title+'.htm','w');a2.write(str_soup);a2.close()
  toget=list(set(toget))
  if toget:
    a=open('linksfile.txt','w')
    for i in toget:      a.write(i+'\n\tdir=htm/images/mini/\n')
    a.close()
    print 'getting images'
    os.system('aria2c -q -i linksfile.txt&&rm -rfv linksfile.txt')
    os.chdir('htm/images/mini/')
    os.system('enc mlim')
    print 'making webp..'
    os.system('enc ul webp')    
    os.system('mv -f encoded/*.jpg .')
  
def process_image_maff(sample_only=False,htm_files=False):
  if not htm_files:
    maff_file=sys.argv[2]
    if maff_file.endswith('.maff'):
      print 'processing maff file ',maff_file
    else:
      print 'interpreting ',maff_file,' as input file with gallery urls.getting "thumb" images'
  #~ from bs4 import BeautifulSoup
  import re
  if os.path.exists('tmp_image_maff'):os.system('rm -r tmp_image_maff/')
  use_requests=False
  if not htm_files:
    if maff_file.endswith('.maff'):
      cmd='unzip -q -d tmp_image_maff "'+maff_file+'"';os.system(cmd)
      html=os.popen('find tmp_image_maff -iname "index.html"|sort').readlines()
    else:
      b=[i.strip() for i in open(maff_file).readlines() if '/gallery/' in i]
      b2=[]
      for i in b:
        if i.startswith('file://'): i=i.replace('file://','https://www.imagefap.com/')
        if not i.endswith('?view=2'): i+='?view=2'
        b2.append(i)
      html=b2
      use_requests=True
  else:
    html=os.listdir('.');html=[i for i in html if i.endswith('.htm')];html.sort()
  a=open('linksfile.txt','w')
  with tqdm.tqdm(html,desc='getting thumb images') as pbar:
    for h in pbar:
      h=h.strip()
      if htm_files:print h
      if use_requests:
        soup=bs4.BeautifulSoup(requests.get(h).content,bsp)
      else:
        soup=bs4.BeautifulSoup(open(h).read(),bsp)
      #~ soup2=BeautifulSoup(open(h.replace('.html','.rdf')).read(),bsp)
      #~ if not htm_files:
        #~ gallery_url=soup2('maf:originalurl')[0]['rdf:resource']
      gallery_name=soup('title')[0].text.replace('Porn pics of ','').replace(' (Page 1)','').replace(' (Page ','').replace('Porn Pics & Porn GIFs','').strip()
      if gallery_name.endswith(')'):gallery_name=gallery_name[:-1]
        #~ gallery_name=soup2('maf:title')[0]['rdf:resource'].replace('Porn pics of ','').replace(' (Page ','')
      uploader_name=soup("font",attrs={'color':"#CC0000",'size':"3"})
      if uploader_name:        uploader_name=uploader_name[0].text.replace('Uploaded by ','')
      else: uploader_name=''
      out_dir_name=gallery_name.replace('/','_')
      #~ if uploader_name:out_dir_name+=' - '+uploader_name
      out_html_name=out_dir_name+'.htm'
      pbar.desc=out_dir_name
  
      images=soup("img",attrs={'alt':re.compile('Free porn pics*')})
      if sample_only:images=images[:15]
      a.write('## GALLERY: '+out_html_name.replace('.htm','')+' : '+str(len(images))+' pics \n\n')
      cnt=0
      for im in images:
        im_src=im['src']
        if 'fap.to/' in im_src:
          im['src']=im['src'].replace('urn:download-error:','')
          # ~ image_index=im['alt'].replace('Free porn pics of '+gallery_name,'').replace('pics','').strip()
          # ~ image_index=image_index.split('of')[0].strip()
          image_index='%.4d' %(cnt)
          image_url=im['src']
          if image_url.endswith('.gif'):continue
          try:image_name=im.parent.parent.parent.parent('font',attrs={'color':"#000000",'face':"verdana"})[0].text
          except:continue
          out_jpg_name=image_index+'__'+image_name
          if not out_jpg_name.endswith('.jpg'):out_jpg_name+='.jpg'
          if not os.path.exists(out_dir_name+'/'+out_jpg_name):
            wrc=image_url+'\n\tdir='+out_dir_name+'\n\tout='+out_jpg_name+'\n';
            if type(wrc)==unicode:wrc=wrc.encode('ascii','replace')
            a.write(wrc)
            cnt+=1
  
        if 'index_files/' in im_src or 'NOTFOUND.svg' in im_src:
          im['src']='NOTFOUND.svg'
          image_index=im['alt'].replace('Free porn pics of '+gallery_name,'').replace('pics','').strip()
          image_index=image_index.split('of')[0].strip()
          a.write('#skipped image '+image_index+'\n')
  
      if not (htm_files or use_requests):
        a_outhtm=open(out_html_name,'w');
        a_outhtm.write(str(soup));a_outhtm.close()
        print 'written ',out_html_name
  if not htm_files:os.system('rm -rf tmp_image_maff')

  
def imap_bar(func, args, n_processes = 10,use_dummy=True,desc=''):
  if use_dummy:  p = multiprocessing.dummy.Pool(n_processes);
  else:  p = multiprocessing.Pool(n_processes);
  res_list = []
  with tqdm.tqdm(total = len(args),desc=desc) as pbar:                                            
    for i, res in tqdm.tqdm(enumerate(p.imap(func, args))):       
      pbar.update()                                                                                           
      res_list.append(res)
  pbar.close();  p.close();p.join();
  return res_list;

def imap_bar_no_tqdm(func, args, n_processes = 10,use_dummy=True):
  if use_dummy:  p = multiprocessing.dummy.Pool(n_processes);
  else:  p = multiprocessing.Pool(n_processes);
  res_list = []
  for i, res in enumerate(p.imap(func, args)):       
    res_list.append(res)
  p.close();p.join();
  return res_list;

def imap_bar_serial(func, args, n_processes = 10,use_dummy=True,sleep_interval=0,desc=''):  
  res_list = []
  with tqdm.tqdm(total = len(args),desc=desc) as pbar:                                            
    for i in  args:
      pbar.update()                                                                                           
      res_list.append(func(i))
      time.sleep(sleep_interval)
  pbar.close();
  return res_list;
  
    
def test():
  pass


  
def autogui_mf(num_times=1):
  import pyautogui,pyperclip,random,string
  VOWELS = "aeiou"
  CONSONANTS = "".join(set(string.ascii_lowercase) - set(VOWELS))
  NUMBERS='0123456789'
  def generate_word(length=5,consonants_only=True):
    word = ""
    for i in range(length):
      if i % 2 == 0:
        if i>0 and not consonants_only:
          word += random.choice(CONSONANTS+NUMBERS)
        else:
          word += random.choice(CONSONANTS)
      else:            word += random.choice(VOWELS)
    return word

  
  sleepsec=1
  baseimpath='/home/aniru/Documents/Software/test/pyautogui/mediafire_reg_nada/'

  

  for kk in range(num_times):
    x=pyautogui.locateOnScreen(baseimpath+"nada_copy_email.png",confidence=0.9,region=(450,220,400,70));
    pyautogui.click(x)
    email_address = pyperclip.paste()
    pyautogui.PAUSE=1
   
    pyautogui.hotkey('ctrl','t')    
    pyautogui.hotkey('ctrl','l')
      # ~ type('i',KeyModifier.CTRL+KeyModifier.ALT);
    pyperclip.copy('https://www.mediafire.com/upgrade/registration.php?pid=66');pyautogui.hotkey('ctrl','v');
    pyautogui.hotkey('enter')
    while not pyautogui.locateOnScreen(baseimpath+"choose_plan_create_account.png",confidence=0.9,region=(530,140,300,75)):
     time.sleep(sleepsec)
      # ~ type(Key.F5);
  # ~ #    while exists("1570241415479.png"): sleep(sleepsec)
      # ~ sleep(15)
    time.sleep(sleepsec*2)
    loc0=pyautogui.locateCenterOnScreen(baseimpath+"choose_plan_create_account.png",confidence=0.9,region=(530,140,300,75))
    # ~ loc=Region(loc0.x,loc0.y+140,25,25) 
    pyautogui.click(loc0.x,loc0.y+160);
    pyperclip.copy(generate_word(4));pyautogui.hotkey('ctrl','v');    pyautogui.hotkey('tab');
    pyperclip.copy(generate_word(5));pyautogui.hotkey('ctrl','v');    pyautogui.hotkey('tab');
    pyperclip.copy(email_address); pyautogui.hotkey('ctrl','v');     
    pyautogui.hotkey('tab');time.sleep(sleepsec/2);pyautogui.hotkey('tab');
    pyautogui.hotkey('space');  
    # ~ type(Key.PAGE_DOWN);type('i',KeyModifier.CTRL+KeyModifier.ALT);
    pyautogui.hotkey('pagedown');
      
      
    time.sleep(sleepsec/2)
    loc=pyautogui.locateOnScreen(baseimpath+"captcha_check_box.png",confidence=0.9)
    if loc:      pyautogui.click(loc);
    else: print 'could not find capcha checkbox.exiting';sys.exit(1)
    while not pyautogui.locateOnScreen(baseimpath+"captcha_passed.png",confidence=0.9): time.sleep(sleepsec)
    # ~ while not exists("1544018387430.png"): sleep(sleepsec)
    while not pyautogui.locateOnScreen(baseimpath+"last_create_account.png",confidence=0.9): time.sleep(sleepsec)
    
    # ~ click("1544018387430.png")
    pyautogui.click(pyautogui.locateOnScreen(baseimpath+"last_create_account.png",confidence=0.9))
      # ~ while not exists("1544018469314.png"): sleep(0.35)
    while not pyautogui.locateOnScreen(baseimpath+"reg_success.png",confidence=0.9,region=(200,200,1000,650)): time.sleep(0.35)
      # ~ type('w',KeyModifier.CTRL)
    pyautogui.hotkey('ctrl','w');
      # ~ while not exists("1544019635021.png"):sleep(sleepsec)
    while not pyautogui.locateOnScreen(baseimpath+"nada_verify_email_mediafire.png",confidence=0.9,region=(310,270,600,250)): time.sleep(sleepsec)
    
    
    pyautogui.click(pyautogui.locateOnScreen(baseimpath+"nada_verify_email_mediafire.png",confidence=0.9,region=(310,270,600,250)))
      
      # ~ while not exists("1544018617226.png"):sleep(sleepsec);
    while not pyautogui.locateOnScreen(baseimpath+"nada_having_trouble.png",confidence=0.9,region=(300,250,700,450)): time.sleep(sleepsec)    
      # ~ loc0=find("1544018617226.png").getTarget()
    loc0=pyautogui.locateCenterOnScreen(baseimpath+"nada_having_trouble.png",confidence=0.9,region=(300,250,700,450))
    # ~ loc=Region(loc0.x,loc0.y+95,10,10)
    pyautogui.PAUSE=0
    pyautogui.click(loc0.x,loc0.y+100);pyautogui.click(loc0.x,loc0.y+100);pyautogui.click(loc0.x,loc0.y+100);
    pyautogui.PAUSE=1    
    pyautogui.hotkey('ctrl','c');
    mf_verify_link=pyperclip.paste();
    # ~ pyautogui.hotkey('ctrl','t');pyautogui.hotkey('ctrl','l');
    # ~ pyautogui.typewrite(mf_verify_link);pyautogui.hotkey('enter');
    
    pyautogui.click(loc0.x,loc0.y+100,button='right');pyautogui.hotkey('t');pyautogui.hotkey('ctrl','pagedown');
     
    aout=open('/home/aniru/mf_accounts','a+')
    aout.write("'"+email_address+"',\n");aout.close();
      # ~ while not exists("1544018885306.png"):sleep(sleepsec)
    while not pyautogui.locateOnScreen(baseimpath+"mediafire_verify_email_page.png",confidence=0.9,region=(400,450,450,150)): time.sleep(sleepsec)
    
    ##VERIFY EMAIL FOR MEDIAFIRE
      # ~ click("1544018885306.png");
    pyautogui.click(pyautogui.locateOnScreen(baseimpath+"mediafire_verify_email_page.png",confidence=0.9,region=(400,450,450,150)));
  
      # ~ while not exists("1544019127860.png"): sleep(0.35)
    while not pyautogui.locateOnScreen(baseimpath+"mediafire_verify_email_page_successful.png",confidence=0.9,region=(150,200,1000,550)): time.sleep(sleepsec)
    time.sleep(0.75)
    pyautogui.hotkey('ctrl','w')     
    
    ##REMOVE COOKIES
    pyautogui.hotkey('ctrl','pagedown')
    loc=pyautogui.locateCenterOnScreen(baseimpath+"firefox_cookies_match.png",confidence=0.9,region=(400,230,400,100))
    pyautogui.click(loc.x,loc.y+118)
    pyautogui.hotkey('delete');pyautogui.hotkey('delete');pyautogui.hotkey('delete');
    pyautogui.hotkey('ctrl','pagedown')
    
    
    ##ADD NEW INBOX,REMOVE OLD ONE
    pyautogui.click(pyautogui.locateOnScreen(baseimpath+"nada_add_inbox.png",confidence=0.9,region=(0,220,320,150)))
      # ~ click("1541495528625.png");
    
    while not pyautogui.locateOnScreen(baseimpath+"nada_add_inbox_accept.png",confidence=0.9,region=(430,320,500,300)): time.sleep(sleepsec)
      
      # ~ click("1541495549036.png")
    pyautogui.click(pyautogui.locateOnScreen(baseimpath+"nada_add_inbox_accept.png",confidence=0.9,region=(430,320,500,300)))    
    time.sleep(2*sleepsec)

  pyautogui.alert(text='pyautogui run finished',title='Done!')
  


def process_autojobs_file(autojobs_file='autojobs',minimum_job_entries=100,dont_upload=False,add_rem=False):
  locked_value='hk99'; #this lockfile value means bookeeping has locked autojobs
  autojobs_lock_sleep_seconds=15  
  lockfile=autojobs_file+'_lockfile'
 
  extra_jobs_file='extra_'+autojobs_file
  extra_jobs=[];#this is list of possible additional jobs
  if os.path.exists(extra_jobs_file):
    extra_jobs=[i.strip() for i in open(extra_jobs_file).readlines() if i.strip().startswith(hosters)  and ',,' in i and not i.strip().endswith('DONE')]
    
  #before cleaning up autojobs, use lock mechanism to make sure we are the only ones modifying file
  curl_ftp_download(lockfile)
  aa=open(lockfile);autojobs_lock=aa.read().strip();aa.close()

  lock_iter_count=0
  wait_pbar=''
  while autojobs_lock not in ['0']:#means some other process is accessing server.wait for sleep_seconds    
    # ~ print lockfile+' is locked by '+ highlight(autojobs_lock,'red_fore_bright')+'.\tsleeping for '+str(autojobs_lock_sleep_seconds)+' seconds'
    if not wait_pbar: wait_pbar=tqdm.tqdm(total=1e5)
    wait_pbar.update(0)
    wait_pbar.desc=lockfile+' locked by '+highlight(autojobs_lock,'red_fore_bright')
    wait_pbar.refresh()
    
    time.sleep(autojobs_lock_sleep_seconds)
    curl_ftp_download(lockfile)
    aa=open(lockfile);autojobs_lock=aa.read().strip();aa.close()
    lock_iter_count+=1
    if lock_iter_count>=240: 
      print 'have been re-doing lock checks for '+str(240*autojobs_lock_sleep_seconds)+' seconds now.Force-zeroing lockfile'
      os.system("echo 0 > "+lockfile)
      curl_ftp_upload(lockfile)
      curl_ftp_download(lockfile)
      aa=open(lockfile);autojobs_lock=aa.read().strip();aa.close()
      # ~ sys.exit(1)      

  if wait_pbar:
    try:wait_pbar.close()
    except: pass
  #reached here means we are unique.lock autojobs
  a=open(lockfile,'w');a.write(locked_value);a.close();
  if not dont_upload: curl_ftp_upload(lockfile)

  

  #special case. this is for adding remaining jobs from rem_autojobs to autojobs_file
  if add_rem:
    curl_ftp_download2('done')
    xx=os.popen3('vd cdff rem .mflogall')[1].read();#its possible some jobs are duplicates already finished
    xx=os.popen3('rm -f rem_autojobs;vd corrau done rem')[1].read();
    if os.path.exists('rem_autojobs') and [i for i in open('rem_autojobs') if i.strip()]:#do only  if rem_autojobs is non-empty
      os.system('cp rem_autojobs tmpfile');#needed since its possible we have to add to "rem_autojobs" jobsfile
      #get file to process
      curl_ftp_download(autojobs_file)
      num_rem_entries=len([i for i in open('tmpfile') if i.startswith(hosters)])
      print 'inserting '+str(num_rem_entries)+' remaining jobs into '+str(autojobs_file)
      xx=os.popen3('enc autojobs_insert_logfile_noverbose tmpfile '+autojobs_file)[1].read()
      # ~ os.system('enc autojobs_insert_logfile tmpfile '+autojobs_file)
      curl_ftp_upload(autojobs_file)
      xx=[os.remove(ff) for ff in ['tmpfile',autojobs_file] if os.path.exists(ff)]
        
    #remove lock and return      
    a=open(lockfile,'w');a.write('0');a.close();
    if not dont_upload: curl_ftp_upload(lockfile);
    os.remove(lockfile)
    return

  #get file to process
  curl_ftp_download(autojobs_file)
  #do cleanup   
  #remove finished+joined entries (present in .mflogall) from autojobs_files
  xx=os.popen3('vd cdff '+autojobs_file+' .mflogall')[1].read()

  #strip all "DONE" entries from autojobs_file and move them to "done"
  aa=open(autojobs_file);  x=aa.readlines();aa.close()
  bb=[i.strip() for i in x if not i.strip().endswith('DONE')];#this is progress-entries+job_entries
  job_entries=[i.strip() for i in x if i.strip().startswith(hosters) and not i.strip().endswith('DONE')]
  done_entries=[i.strip() for i in x if i.strip().endswith('DONE')]
  progress_entries=[i.strip() for i in bb if i.startswith(heroku_clones) and i[2:4].isdigit()]

  #add new entries from extra_autojobs to autojobs if needed (and if possible)
  if (len(job_entries)<minimum_job_entries):
    if extra_jobs:
      extra_entries_needed=minimum_job_entries-len(job_entries)
      extra_jobs=list(set(extra_jobs)-set(bb));#make sure the extra_jobs are not already present in file
      extra_jobs.sort()
      #add extra-entries to autojobs_file
      for i in extra_jobs[:extra_entries_needed]: bb.append(i)
      #remove the added entries from extra-file
      aa=open(extra_jobs_file,'w')
      for i in extra_jobs[extra_entries_needed:]:  aa.write(i+'\n')
      aa.close()
  
      print autojobs_file+' had '+highlight(str(len(job_entries)),'red_fore')+', adding '+highlight(str(len(extra_jobs[:extra_entries_needed])),'green')+' extra-entries from extra_file: '+extra_jobs_file
    else:
      print highlight('needed to add addition entries to autojobs_file: '+autojobs_file+' but the extra_'+autojobs_file+' file has no valid jobs!!','red')
  else:
    print autojobs_file+' had '+str(len(job_entries))+' job-entries remaining, which is more than minimum_job_entries: '+str(minimum_job_entries)
  aa=open(autojobs_file,'w');
  for i in bb:aa.write(i+'\n')
  aa.close()

  #find the (starting-part) of filenames that are being processed in this autojobs_file
  filenames_being_processed=[i.split()[1].split(':')[0].strip().replace('.mp4','.mkv') for i in progress_entries]
  filenames_being_processed=list(set(filenames_being_processed));filenames_being_processed.sort();#remove duplicates
  filenames_being_processed_tuple=tuple(filenames_being_processed);
  #remove entries from ./rem that start with the "filenames_being_processed_tuple"
  aa=open('rem');rem_jobs_orig=[i.strip() for i in aa.readlines()];aa.close()
  rem_jobs=[i for i in rem_jobs_orig if not i.split('::')[1].strip().startswith(filenames_being_processed_tuple)]
  if len(rem_jobs_orig)>len(rem_jobs):
    # ~ print 'removing '+highlight(str(len(rem_jobs_orig)-len(rem_jobs)),'red_fore')+' in-progress entries from ./rem' 
    aa=open('rem','w')
    for i in rem_jobs: aa.write(i+'\n');
    aa.close()
    

  
  #upload processed autojobs
  if not dont_upload:  curl_ftp_upload(autojobs_file)    

  #remove lock
  a=open(lockfile,'w');a.write('0');a.close();
  if not dont_upload: curl_ftp_upload(lockfile);
  os.remove(lockfile)

  #append all DONE entries not already present to 'done'. This can be done outside lock
  curl_ftp_download2('done')
  done_entries_old=[]
  if os.path.exists('done'):
    # ~ print 'size of done %s' %(os.popen('du -sh done').read())
    done_entries_old=[i.strip() for i in open('done').readlines() if i.strip().endswith('DONE')]    
  else:
    print 'could not download "done" from ftp2(thorondor).assuming empty "done" file'
  done_entries_new=list(set(done_entries_old).union(set(done_entries)))
  # ~ print '"done" had %d entries from before. adding %d new ones' %(len(done_entries_old),len(done_entries_new)-len(done_entries_old))
  done_entries_new.sort()
  aa=open('done','w')
  for i in done_entries_new: aa.write(i+'\n')
  aa.close()

  xx=os.popen3('vd cdff done .mflogall')[1].read()

  if not dont_upload: curl_ftp_upload2('done')
  


def bookkeep(tmp_mf_account=447,upload_joined_mf_account=477,partial_mflogall_account=504,margin_mf=2,interval=60.,insert_remaining_jobs=True,insert_first=False):

  print 'running bookkeeping operation every '+str(interval)+' minutes'


  loop_cnt=0;firstprint=True
  while 1>0:
    start_time=time.time()

    #check if upload account has enough space, else increment (in Mb)
    space_remaining_in_join_account_margin=150; #if less than this (mb) then increment
    upload_joined_mf_account_changed=False;old_upload_joined_mf_account=upload_joined_mf_account
    space_remaining_in_join_account=float(os.popen('vd mfspace '+str(upload_joined_mf_account)).read().strip());  
    while space_remaining_in_join_account<space_remaining_in_join_account_margin:
      upload_joined_mf_account+=1;upload_joined_mf_account_changed=True;
      space_remaining_in_join_account=float(os.popen('vd mfspace '+str(upload_joined_mf_account)).read().strip());  
    if upload_joined_mf_account_changed:
      info='changed the upload account from mf://%d to mf://%d, which has %.2f Gb space remaining' %(old_upload_joined_mf_account,upload_joined_mf_account,space_remaining_in_join_account/1024.)
      print highlight(info,'red_fore_bright')
  
    #commands
    end_acc=upload_joined_mf_account+margin_mf
    if upload_joined_mf_account<partial_mflogall_account: end_acc=partial_mflogall_account+margin_mf
    join_cmd='vd mftemp '+str(tmp_mf_account)+'-'+str(upload_joined_mf_account+margin_mf)
    if os.path.exists('partial_mflogall'):
      join_cmd='vd mftemp '+str(partial_mflogall_account+1)+'-'+str(end_acc)+';cat partial_mflogall >> .mflogall;vd mf d '+str(tmp_mf_account)+'|grep "USED:";cat .mflog'+str(tmp_mf_account)+' >> .mflogall;vd gdl '+str(gd_account_pp)+'|grep "used/";cat .gdlog'+str(gd_account_pp)+' >> .mflogall;rm -f .mflog'+str(tmp_mf_account)+';rm -f .gdlog'+str(gd_account_pp)
      
    join_cmd+=';vd jchlimited .mflogall  j'+str(upload_joined_mf_account)+';vd gsff tojoin;rm -f *mkv;'
    if os.path.exists('partial_mflogall'):
      join_cmd+='vd mftemp '+str(partial_mflogall_account+1)+'-'+str(upload_joined_mf_account+margin_mf)+';cat partial_mflogall >> .mflogall;'
    else:
      join_cmd+='vd mftemp 458-'+str(upload_joined_mf_account+margin_mf)+';'
    
    misc_cmd='wc done extra_*;cp .mflogall /tmp;rm -f autojobs rem_autojobs .mflog*;mv -f /tmp/.mflogall .'

    #Print commands
    if firstprint:
      print highlight('running commands:\n\t%s\n\t%s\n' %(join_cmd,'vd cdff autojobs .mflogall'),'bright')
      firstprint=False

    #run base command that joins and upload completed files 
    os.system(join_cmd);

    #process autojobs files to remove "done" and add extra jobs as needed
    for ajj in ['autojobs','rem_autojobs']: #,'autojobs3']:    
      process_autojobs_file(autojobs_file=ajj)
    #remove extra files
    remove_extra_cmd='vd gdl 115|tail&&vd jch .gdlog115 r&&vd corr rem .mflogall &&vd corr .gdlog115 l b&&vd pf l f&&rm -f .gdlog115'
    x=os.popen3(remove_extra_cmd)[1].read()

    #now ./rem has all entries that are NOT under progress in any autojobs file. Add them to aj3 iff "insert_remaining_jobs"
    if insert_first:
      if insert_remaining_jobs and (loop_cnt%10)==0: #run once every 5 iterations
        process_autojobs_file('autojobs',add_rem=True)
        loop_cnt=0
    else:
      if insert_remaining_jobs and (loop_cnt>0 and (loop_cnt%7)==0): #run once every 5 iterations
        process_autojobs_file('autojobs',add_rem=True)
        loop_cnt=0
        
    #finishing up commands
    os.system(misc_cmd)
    #SKIP UPLOAD TO SAVE TRAFFIC
    # ~ if os.path.exists('extra_autojobs'): curl_ftp_upload2('extra_autojobs')
    # ~ if os.path.exists('extra_rem_autojobs'): curl_ftp_upload2('extra_rem_autojobs')
    
    #remove time taken from command from sleep_time
    time_taken_for_cmd=(time.time()-start_time)/60.; #in minutes
    real_interval=interval-time_taken_for_cmd; 
    if real_interval<=0.5: real_interval= 0.5

    pbar=tqdm.tqdm(range(int(20.*real_interval)),desc='waiting till next bookkeeping')
    for i in pbar: time.sleep(3)
    pbar.close()
    loop_cnt+=1
    #save autojobs and rem_autojobs for backup
    curl_ftp_download('autojobs')
    curl_ftp_download('rem_autojobs')

def cshcmd(cmd='du -sh .',interval=0.3):
  if len(sys.argv)>2: cmd = sys.argv[2]
  pbar_mode=False
  if sys.argv[-1].replace('.','').replace('m','').isdigit():
    if sys.argv[-1].endswith('m' ):
      pbar_mode=True
      interval=float(sys.argv[-1].replace('m',''))
    else:
      interval=float(sys.argv[-1])
  print 'running cmd ',cmd,' every ',interval,' minutes'
  while 1>0:
    start_time=time.time()
    os.system(cmd)
    time_taken_for_cmd=(time.time()-start_time)/60.; #in minutes

    real_interval=interval-time_taken_for_cmd; #remove time taken from coimmand from sleep_time
    if real_interval<=0.5: real_interval= 0.5

    if pbar_mode:
      pbar=tqdm.tqdm(range(int(20.*real_interval)),desc='waiting till next cshcmd')
      for i in pbar: time.sleep(3)
      pbar.close()
    else:
      time.sleep(60.*real_interval)
    
  

      
def mediafire_get_direct_link2(qkey='',verbose=False):
  url='https://www.mediafire.com/file/'+qkey+'/'
  soup=bs4.BeautifulSoup(requests.get(url).content,bsp)
  try:
    dlink=soup(class_='download_link')[0]('a')[0]['href']
    if verbose:      print dlink
    return dlink
  except:
    if verbose:      print 'could not find dlink'
    return None

def mediafire_get_direct_link1(account=0, qkey=None,verbose=False,from_file=False,start_aria=False,debug=True):
  if len(sys.argv)>2 and  sys.argv[-1].isdigit():
    account=sys.argv[-1]
  
  
    
  if from_file:
    infile=qkey
    try: assert(infile)
    except:
      print 'need to give valid infile'
      return None
    b=open(infile).readlines()
    b=[i.strip() for i in b if len(i.strip())>0 and not i.startswith('#')]
    account=int(b[0].split('::')[0].split('-')[1].strip()) #first
    email=mediafire_accounts[int(account)]
    
    signature=hashlib.sha1(email+'nazgul028842511').hexdigest();  
    login_url='https://www.mediafire.com/api/1.4/user/get_session_token.php?email='+email+'&password=nazgul0288&application_id=42511&response_format=json&signature='+signature
    
    x=requests.post(login_url).json()
    if x['response']['result'].lower()!='success':
      print 'error in login,exiting'
      print x['response']
      return  
    session_token=x['response']['session_token']
    
    a=open('linksfile.txt','w')
    current_account=account
    num_links=0
    for i in b:
      qkey=i.split('::')[3].strip()
      current_account=i.split('::')[0].split('-')[1].strip() #
      if current_account!=account:
        if verbose:   print 'changing account from '+str(account)+' to '+str(current_account)
        account=current_account
        email=mediafire_accounts[int(account)]
        signature=hashlib.sha1(email+'nazgul028842511').hexdigest();  
        login_url='https://www.mediafire.com/api/1.4/user/get_session_token.php?email='+email+'&password=nazgul0288&application_id=42511&response_format=json&signature='+signature
        
        x=requests.post(login_url).json()
        if x['response']['result'].lower()!='success':
          print 'error in login,exiting'
          print x['response']
          return  
        session_token=x['response']['session_token']
        
      try:
        get_link_url='https://www.mediafire.com/api/1.4/file/get_links.php?session_token'+session_token+'&response_format=json&link_type=direct_download&quick_key='+qkey
        x=requests.post(get_link_url).json()['response']
        if x['links'][0].has_key('direct_download'):
          dlink=str(x['links'][0]['direct_download'])
          a.write('## '+i+'\n'+dlink+'\n')
          num_links+=1
          if verbose: print highlight(dlink,'green')        
        else:
          print 'could not get direct download link for quickkey ',qkey
          dlink=None
      except:
        print 'Exception in getting direct download link for qkey, ',qkey,' from account ',account,' with email ',email
        print sys.exc_info()[:1]
        continue
    
    a.close()
    if verbose: print 'written linksfile.txt with '+str(num_links)+' links'
    
    #~ start aria
    if start_aria and IS_LOCAL_MACHINE:
      cmd='aria2c -j 4 -i linksfile.txt'
      if verbose: print cmd
      os.system(cmd)
  else:
    try: assert(qkey)
    except:
      print 'need to give valid qkey'
      return None
    
    account=int(account)
    email=mediafire_accounts[account]
    
    signature=hashlib.sha1(email+'nazgul028842511').hexdigest();  
    login_url='https://www.mediafire.com/api/1.3/user/get_session_token.php?email='+email+'&password=nazgul0288&application_id=42511&response_format=json&token_version=2&signature='+signature
    
    x=requests.post(login_url).json()
    print 'x:',x
    if x['response']['result'].lower()!='success':
      print 'error in login,exiting'
      print x['response']
      return  
    session_token=x['response']['session_token']
    secret_key_mod=int(x['response']['secret_key']) % 256
    mf_time=x['response']['time']
    
    
    # ~ try:
    uri='/api/1.3/file/get_links.php'
    query='link_type=direct_download&quick_key='+qkey+'&response_format=json&session_token'+session_token
    signature_base=(str(secret_key_mod) + mf_time + uri + '?' + query).encode('ascii')
    query += '&signature=' + hashlib.md5(signature_base).hexdigest()
    headers={"Content-Type":'application/x-www-form-urlencoded'}
  
    
    get_link_url=('https://www.mediafire.com'+uri+'?'+query).encode('utf-8')
    print 'get_link_url: ',get_link_url
    #/api/1.3/file/get_links.php?link_type=direct_download&session_token'+session_token+'&response_format=json&quick_key='+qkey
    x=requests.post(get_link_url,headers=headers).json()['response']
    print x
    if debug: print x
    if x['links'][0].has_key('direct_download'):
      dlink=str(x['links'][0]['direct_download'])
      if verbose: print highlight(dlink,'green')        
    else:
      print 'could not get direct download link for quickkey ',qkey
      dlink=None
    return dlink
    

def wget_put(infile,account=0,only_upload=False):

  assert(infile)
  b=open(infile).readlines()
  b=[i.strip() for i in b if len(i.strip())>0 and not i.startswith('#')]
  print 'trying for '+str(len(b))+' links'
  for j in range(len(b)):
    #~ print j,' th entry'
    l=b[j]
    if not only_upload:
      wgetc='wget "'+l+'"'
      print wgetc
      os.system(wgetc)
      lfile=os.path.split(l)[1]
      if not os.path.exists(lfile):
        print lfile,' not found.Exiting'
        sys.exit(1)
    else:
      lfile=b[j].strip()
      if not os.path.exists(lfile):        print 'specified file '+lfile+' does not exists.Continuing';continue;
      else: print 'tring to upload '+lfile+' to mf://'+str(account)
  
    putc='mediafire-shell -u '+mediafire_accounts[account]+' -p nazgul0288 -c "put '+lfile+'"'
    print putc
    os.system(putc)
    os.system('rm -fv '+lfile)
  return
    
  


def get_cookies(cj, ff_cookies):
  con = sqlite3.connect(ff_cookies)
  cur = con.cursor()
  cur.execute("SELECT host, path, isSecure, expiry, name, value FROM moz_cookies")
  for item in cur.fetchall():
    c = cookielib.Cookie(0, item[4], item[5],
          None, False,
          item[0], item[0].startswith('.'), item[0].startswith('.'),
          item[1], False,
          item[2],
          item[3], item[3]=="",
          None, None, {})
    print c
    cj.set_cookie(c)
  
  
def santabanta_maff(infile):
  #~ from bs4 import BeautifulSoup
  if os.path.exists('tmph'):os.system('rm -r tmph/')
  cmd='unzip -q -d tmph "'+infile+'"';  os.system(cmd)
  html=os.popen('find tmph -iname "index.html"|sort').readlines()
  a=open('links.txt','w')
  for h in html:
    h=h.strip()
    soup=bs4.BeautifulSoup(open(h).read(),bsp)
    title=soup.title.text
    print title
    wallpaper_page=False
    celeb=''
    if ( 'photos and pictures' not in title.lower()) and ('santabanta' not in title.lower()) :
      print 'skipping non-santabanta page '+title
    else:
      if 'wallpaper' in title.lower(): 
        wallpaper_page=True
        celeb=soup(class_='heading-div-2')[0].text.strip()
      else:
        celeb=soup(class_='content-div-heading-2')[0].text.strip()
        
    images=soup('img')
    toget=[]
    for im in images:
      ims=im['src']
      if 'index_files' in ims: continue
      ims=ims.replace('urn:download-error:','')   
      
      if wallpaper_page:
        ims=ims.replace('/medium1/','/full1/').replace('media.santabanta','media1.santabanta')
        if im.has_attr('alt'):ima=im['alt']
        else: ima=None
        if '.jpg' in ims:        toget.append((ims,ima))          
      else:
        ims=ims.replace('_th.jpg','.jpg')
        if '.jpg' in ims:        toget.append(ims)
        
    #~ if wallpaper,sort by alt,only get max alt
    if wallpaper_page:
      dd={}
      for i in toget:
        if i[1] in dd:          dd[i[1]].append(i[0])
        else:          dd[i[1]]=[i[0]]
      try:maxims=sorted(dd,cmp=lambda a,b:cmp( len(dd[a]),len(dd[b]) ),reverse=True )[0]
      except:
        continue
      toget=dd[maxims]
    if type(title)==unicode: title=title.encode('ascii','replace')

    a.write('## '+title+'\n')
    if wallpaper_page:      [a.write(i+'\n\tdir=wallpapers/'+celeb+'\n') for i in toget]
    else:      [a.write(i+'\n\tdir=galleries/'+celeb+'\n') for i in toget]
    

  a.close()
  os.system('rm -rf tmph')
        

def watch_episodes_process(infile='',verbose=False):
  if sys.argv[-1] in ['verbose']: verbose=True
  b=[i.strip() for i in open(infile).readlines() if 'watchepisodeseries' in i and not i.startswith('#')]
  results=[];  results_openload=[]  
  toprint=[];failed=[];
  timeout=30
  with tqdm.tqdm(b,desc='series:') as pbar1:
    
    for i in pbar1:
      min_season=1
      if '##' in i:        
        min_season=int(i.split('##')[1].strip())
        i=i.split('##')[0].strip()
      soup=bs4.BeautifulSoup(requests.get(i,timeout=timeout).content,bsp)
      episodes=soup(class_='el-item')
      series_name=soup('h2')[0].text.strip().replace(' Episodes','').replace("'",'').replace(",",'').replace(' ','.')
      pbar1.desc=series_name
      pbar1.refresh()
      episodes_found=0
      #find links for each episode (each season)
      with tqdm.tqdm(episodes,desc='episode:') as pbar2:
        for episode in pbar2:
          season=episode(class_='season')[0].text.strip().lower().replace('season ','')        
          number=episode(class_='episode')[0].text.strip().lower().replace('episode ','')
          name=episode(class_='name')[0].text.strip()
          name=name.replace(' ','.')
          replace_chars=["'",",","?",":","!","+","/"]
          for replace_char in replace_chars:          name=name.replace(replace_char,'')
          season=int(season);number=int(number)
          episode_given_name='%s.S%.2dE%.2d.%s' %(series_name,season,number,name)
          if verbose: print episode_given_name
          pbar2.desc='S%.2dE%.2d (%d)' %(season,number,episodes_found)
          pbar2.refresh()
          if season<min_season: continue

          
          hlink=episode('a')[0]['href']
          
          #go to individual episode page
          soup2=bs4.BeautifulSoup(requests.get(hlink,timeout=timeout).content,bsp)
          streamlinks=soup2(class_='ll-item')
          link_found=False
          
          #first try verystream
          verystream_links_pages=[link for link in streamlinks if 'verystream.com' in link(class_='domain')[0].text.strip()]
          
          #check each verystream link
          for verystream_links_page in verystream_links_pages:
            verystream_link_page=verystream_links_page(class_='watch')[0]('a')[0]['href']
            soup2=bs4.BeautifulSoup(requests.get(verystream_link_page,timeout=timeout).content,bsp)
            verystream_embed_link=soup2(class_='watch-button')
            if not verystream_embed_link:
              toprint.append(highlight('no verystream links found on page for '+episode_given_name,'red_fore'))
            else:
              verystream_embed_link=verystream_embed_link[0]['href']
              file_id=verystream_embed_link.split('/')
              file_id=file_id[file_id.index('stream')+1].strip()
              file_info=requests.get('https://api.verystream.com/file/info?file='+file_id).json()
              if file_id in file_info['result'] and file_info['result'][file_id]['size']:
                video_size=file_info['result'][file_id]['size']
                video_size=float(video_size)/(1024*1024)
                if not episode_given_name.endswith('.mp4'):episode_given_name+='.mp4'
                line='vs-x :: %s :: %.1f Mb :: %s' %(episode_given_name,video_size,file_id)
                if type(line)==unicode:  line=line.encode('ascii','replace')    
                results.append(line)
                episodes_found+=1
                link_found=True
                break               
          
          if not link_found:
            #then openload
            openload_links_pages=[link for link in streamlinks if 'openload.co' in link(class_='domain')[0].text.strip()]
            
            #check each verystream link
            for openload_links_page in openload_links_pages:
              openload_links_page=openload_links_page(class_='watch')[0]('a')[0]['href']
              soup2=bs4.BeautifulSoup(requests.get(openload_links_page,timeout=timeout).content,bsp)
              openload_embed_link=soup2(class_='watch-button')
              if not openload_embed_link:
                toprint.append(highlight('no openload links found on page for '+episode_given_name,'red_fore'))
              else:
                openload_embed_link=openload_embed_link[0]['href'].replace('openload.io','openload.co')
                if not openload_embed_link: continue
                file_id=openload_embed_link.split('/')
                if 'embed' in file_id:
                  file_id=file_id[file_id.index('embed')+1].strip()
                elif 'f' in file_id:
                  file_id=file_id[file_id.index('f')+1].strip()
                else:
                  print 'error getting file_id for %s in episode %s' %(file_id,episode_given_name)
                  sys.exit(1)
                link_pair=(episode_given_name,openload_embed_link)
                video_size=100.0
                if openload_check_link(link_pair):
                  if not episode_given_name.endswith('.mp4'):episode_given_name+='.mp4'
                  line='ol-0 :: %s :: %.1f Mb :: %s' %(episode_given_name,video_size,file_id)
                  if type(line)==unicode:  line=line.encode('ascii','replace')    
                  results_openload.append(line)
                  episodes_found+=1
                  link_found=True
                  break
 
          if not link_found:
            toprint.append('episode: '+highlight('%s' %(episode_given_name),'red_fore')+' gone!') 
            failed.append(episode_given_name)
  if toprint:
    for i in toprint: print i
  a=open('./l','w')
  for i in results:    a.write(i+'\n')
  a.close()
  print 'wrote %d valid links to ./l' %(len(results))
  if results_openload:
    a=open('./l.2','w')
    for i in results_openload:    a.write(i+'\n')
    print 'wrote %d valid openload links to ./l.2' %(len(results_openload))
    a.close()
  if failed:
    a=open('./failed','w')
    for i in failed: a.write(i+'\n')
    a.close()


  
def watch_episodes_process_single_episode(in_tuple):
  
  try:
    import cfscrape  
  except ImportError:
    if not IS_LOCAL_MACHINE: os.system('pip install -U --user cfscrape&&rm -rf ~/.cache')    
    import cfscrape  
  cfscrape.DEFAULT_CIPHERS = 'TLS_AES_256_GCM_SHA384:ECDHE-ECDSA-AES256-SHA384'
  scraper = cfscrape.create_scraper()  # returns a CloudflareScraper instance
  
  # ~ episode,min_season,timeout,series_name=in_tuple
  hlink,episode_given_name,min_season,timeout,series_name=in_tuple
  output=''
  result=''
  result_openload=''
  

  soup2=bs4.BeautifulSoup(scraper.get(hlink).content,bsp)
  streamlinks=soup2(class_='ll-item')
  link_found=False
  
  #first try verystream
  verystream_links_pages=[link for link in streamlinks if 'verystream.com' in link(class_='domain')[0].text.strip()]
  
  #check each verystream link
  for verystream_links_page in verystream_links_pages:
    verystream_link_page=verystream_links_page(class_='watch')[0]('a')[0]['href']

    soup2=bs4.BeautifulSoup(scraper.get(verystream_link_page).content,bsp)
    verystream_embed_link=soup2(class_='watch-button')
    if not verystream_embed_link:
      output+=highlight('no verystream links found on page for '+episode_given_name+'\n','red_fore')
      # ~ pass
    else:
      verystream_embed_link=verystream_embed_link[0]['href']
      file_id=verystream_embed_link.split('/')
      file_id=file_id[file_id.index('stream')+1].strip()
      file_info=requests.get('https://api.verystream.com/file/info?file='+file_id).json()
      if file_id in file_info['result'] and file_info['result'][file_id]['size']:
        video_size=file_info['result'][file_id]['size']
        video_size=float(video_size)/(1024*1024)
        if not episode_given_name.endswith('.mp4'):episode_given_name+='.mp4'
        line='vs-x :: %s :: %.1f Mb :: %s' %(episode_given_name,video_size,file_id)
        if type(line)==unicode:  line=line.encode('ascii','replace')    
        result=line
        # ~ episodes_found+=1
        link_found=True
        break               
  
  if not link_found:
    #then openload
    
    openload_links_pages=[link for link in streamlinks if 'openload.co' in link(class_='domain')[0].text.strip()]
    if timeout in ['debug'] and openload_links_pages: print 'going into openload'
    
    #check each verystream link
    for openload_links_page in openload_links_pages:
      openload_links_page=openload_links_page(class_='watch')[0]('a')[0]['href']
      soup2=bs4.BeautifulSoup(scraper.get(openload_links_page).content,bsp)
      openload_embed_link=soup2(class_='watch-button')
      if not openload_embed_link:
        if timeout in ['debug']: 
          output+=highlight('no openload links found on page'+openload_links_page+' for '+episode_given_name+'\n','red_fore')
        else: pass
      else:
        openload_embed_link=openload_embed_link[0]['href'].replace('openload.io','openload.co')
        if not openload_embed_link: continue
        file_id=openload_embed_link.split('/')
        if 'embed' in file_id:
          file_id=file_id[file_id.index('embed')+1].strip()
        elif 'f' in file_id:
          file_id=file_id[file_id.index('f')+1].strip()
        else:
          print 'error getting file_id for %s in episode %s' %(file_id,episode_given_name)
          file_id='0x0x0x'
          openload_embed_link='https://openload.co/f/'+file_id
        link_pair=(episode_given_name,openload_embed_link)
        if timeout in ['debug']: print link_pair
        video_size=100.0
        if openload_check_link(link_pair):
          if not episode_given_name.endswith('.mp4'):episode_given_name+='.mp4'
          line='ol-0 :: %s :: %.1f Mb :: %s' %(episode_given_name,video_size,file_id)
          if type(line)==unicode:  line=line.encode('ascii','replace')    
          result_openload=line
          link_found=True
          break
  
  if not link_found:
    output+='episode: '+highlight('%s' %(episode_given_name),'red_fore')+' gone!'
  return (result,result_openload,output,episode_given_name)


def mediafire_join_chunks(infile='',limited_print=False,verbose=True):
  debug=False
  if 'debug' in sys.argv:
    debug=True;
    print highlight('debug mode on...','red')
  vid_files=[i for i in os.listdir('.') if i.endswith(video_extensions)]
  if vid_files:
    print highlight('There are already video files in ./. Cannot concat partial-files in directory!!','red')
    sys.exit(1)
  b=[i.strip() for i in open(infile).readlines() if i.startswith(('mf-','gd-')) and '.of.' in i]
  if not b: 
    print 'no chunk type lines (1.of.x) in ',infile;
    return
  chunk_map={}
  for i in b:
    i0=i
    i=i.split('::')[1].strip()
    l=i.split('.')
    l.reverse(); # to make sure any intermediate '.of' is not found    
    of_index=l.index('of');    
    #this is needed , since sometime index+-1 may give exception
    try:
      chunk_number='foo';total_chunks='foo';
      chunk_number=l[of_index+1];    total_chunks=l[of_index-1]
    except:
      print highlight('could not get chunk_number and total_chunks for '+i,'red')
      
    #only continue if it os of chunk_type
    if not (chunk_number.isdigit() and (total_chunks.isdigit() or  total_chunks in ['0N','0L']) ): continue; 
    
    base_file=l[of_index+2:];base_file.reverse();base_file='.'.join(base_file)+'.mkv'
    if debug: print 'base_file: '+base_file

    # ~ if '-pg' in base_file and base_file[base_file.rindex('-pg')+3:base_file.rindex('-pg')+6].isdigit():
      # ~ base_file=base_file.replace('-','_').replace('#','').replace('+',' ')); #hack for porngo
      
    if base_file in chunk_map: chunk_map[base_file].append((i,int(chunk_number),total_chunks,i0))
    else: chunk_map[base_file]=[(i,int(chunk_number),total_chunks,i0)]
  # ~ if verbose: 
    # ~ print chunk_map
  #check for missing chunks for each base_file
  chunks_to_remove=[]
  base_files=chunk_map.keys();base_files.sort();
  
  if sys.argv[-1] in ['rem','r','j','jr','rj','js'] or (sys.argv[-1].startswith('j') and sys.argv[-1][1:].isdigit()): arem=open('rem','w');rem_lines=0
  
  for base_file in base_files:
    total_chunks=chunk_map[base_file][0][2]
    if total_chunks not in ['0N','0L']: total_chunks=int(total_chunks)
    else: 
      total_chunks=0
      #check if 0L is present in list of .of.0N. type files. That particular chunk_number sets total_chunks
      for k in chunk_map[base_file]:
        if k[2]=='0L':#needewd since .of.0L maybe there, but some intermediate chunk may be missing
          total_chunks=k[1]
      if not total_chunks: #dont process "0X.of.0N" if 'OL' is not present in list.This means encode is incomplete
        if verbose and (not limited_print):         
          print highlight('chunks upto %d done for %s of type 0X.of.0N' %(len(chunk_map[base_file]),base_file),'yellow_fore')
        chunks_to_remove.append(base_file); 
        continue
      
    chunks_available=[]
    for j in chunk_map[base_file]:      chunks_available.append(j[1])
    chunks_available=list(set(chunks_available))
    # ~ print chunks_available
    chunks_missing=list(set(range(1,total_chunks+1))-set(chunks_available))
    chunks_missing.sort()
    if chunks_missing:
      # ~ print chunks_missing
      chunks_missing.sort()
      if verbose: 
        # ~ for k in chunks_missing:
        if not limited_print:
          print highlight('chunk numbers: %s /%d missing for %s' %(str(chunks_missing),total_chunks,base_file),'red_fore')
        if sys.argv[-1] in ['rem','r','j','jr','rj','js'] or (sys.argv[-1].startswith('j') and sys.argv[-1][1:].isdigit()):
          arem.write('gd-0 :: '+base_file+' :: 1 mb :: '+str(chunks_missing[0]))
          for cc in chunks_missing[1:]: arem.write(','+str(cc))
          arem.write('\n')
          rem_lines+=1
      chunks_to_remove.append(base_file)
    else:
      if verbose and (not limited_print):
        print highlight('all %.2d/%.2d chunks found for %s' %(total_chunks,total_chunks,base_file))
  for k in chunks_to_remove: chunk_map.pop(k)
  
  #only do on server, write ./tojoin with names+sizes of all chunks to be joined
  alllines=[]
  if sys.argv[-1] in ['rem','r','j','jr','rj','js'] or (sys.argv[-1].startswith('j') and sys.argv[-1][1:].isdigit()):
    arem.close();print 'written %d lines to ./rem' %(rem_lines)

  if not IS_LOCAL_MACHINE:
    if os.path.exists('tojoin') and sys.argv[-1] in ['jold','jalready']:
      print highlight('./tojoin already existed and "jold/jalready" given. not rewriting. joined chunks will be from that file'.upper(),'bright')
      alllines=[i.strip() for i in open('tojoin').readlines()]
      alllines.sort(); 
    else:
      for base_file in chunk_map:
        for i in chunk_map[base_file]:        line=i[3];alllines.append(line)
      alllines.sort(); 
      a=open('tojoin','w')
      for i in alllines:    a.write(i+'\n')
      a.close()
      print 'written %d mf- lines of files to be joined to ./tojoin' %(len(alllines))
      os.system('vd gsff ./tojoin')
    
  #actually concatenate files if asked
  if not IS_LOCAL_MACHINE and sys.argv[-1].startswith('j'):
    
    if os.path.exists('delfile'): os.remove('delfile')
    finished=[]
    os.system('mkdir -p /tmp/test123')
    keyl=chunk_map.keys();keyl.sort()
    joined_files=0
    #only do on server
    # ~ for base_file in chunk_map:
    for base_file in keyl:
      a=open('tmpfile','w')
      lines_written_to_linksfile=[]
      
      for i in chunk_map[base_file]:        
        line=i[3].strip();
        if line in alllines:
          a.write(line+'\n')
          lines_written_to_linksfile.append(line)
      a.close()     
      if not [i.strip() for i in open('tmpfile').readlines() if i.strip()]:
        print highlight('no chunks to join!!.exiting','red_fore')
        os.remove('tmpfile')
        sys.exit(1) 
      #download chunks from mf-,and concat them with enc ul concat
      os.system('cat tmpfile|grep gd- > tmpfile.gd');os.system('cat tmpfile|grep mf- > tmpfile.mf')
      os.system('rm -f linksfile.txt&&echo "">linksfile.txt&&vd mfd tmpfile.mf quiet');os.system('enc rclone_pull_file_delete_zero tmpfile.gd')
      os.system('aria2c --check-certificate=false --quiet  -i linksfile.txt 1> /dev/null')
      os.system('enc ml')
      # ~ ax=open('.vlist');bx=[i.strip() for i in ax.readlines() if i.strip()];ax.close()
      bx=[i.strip() for i in os.listdir('.') if i.endswith(video_extensions)];
      if not bx:
        zero_size_files=[i for  i in lines_written_to_linksfile if i.strip() and float(i.split('::')[2].strip().split()[0].lower().replace('mb','').replace('kb','').replace('b',''))==0] #in mb        
        if zero_size_files: #add to delfile
          zero_file_names=[]
          adel=open('delfile','a')
          for ii in zero_size_files:
            adel.write(ii.strip()+'\n')
            zero_file_names.append(ii.split('::')[1].strip())
          adel.close()
          zero_file_names.sort()
          print 'adding to ./delfile, zero-length files: '+highlight(str(zero_file_names),'red_fore_bright')+'. Deleting all video-files in curdir and continuing'
        print highlight('could not download chunk files for %s.skipping' %(base_file),'red')
        continue

      #number of video chunks downloaded must match number of chunks written. Fails when mediafire problem
      try: assert(len(bx)==len(lines_written_to_linksfile))
      except AssertionError:
        ll2=[i.split('::')[1].strip() for i in lines_written_to_linksfile]
        zero_size_files=[i for  i in lines_written_to_linksfile if i.strip() and float(i.split('::')[2].strip().split()[0].lower().replace('mb','').replace('kb','').replace('b',''))==0] #in mb
        bx.sort()        
        if zero_size_files: #add to delfile
          adel=open('delfile','a')
          zero_file_names=[]
          for ii in zero_size_files:
            adel.write(ii.strip()+'\n')
            zero_file_names.append(ii.split('::')[1].strip())
          adel.close()
          zero_file_names.sort()
          print 'adding to ./delfile, zero-length files: '+highlight(str(zero_file_names),'red_fore_bright')+'. Deleting all video-files in curdir and continuing'
        else:
          print 'failed in checking length of filelist: '+highlight(str(bx),'red_fore')+'\nnomatch chunks: '+highlight(str(ll2),'red_fore_bright')+'\nProbably some medfiire links has failed.Removing all files and continuing'
        #remove files and continue
        for jj in bx:
          if os.path.exists(jj): os.remove(jj)
        [os.remove(i) for i in os.listdir('.') if i.endswith(video_extensions)]
        continue
      #check all chunks have same codec, and same video height
      vfs=[i for i in os.listdir('.') if i.endswith(video_extensions)];vfs.sort()
      
      codec_name_cmd='ffprobe  -hide_banner -loglevel quiet   -select_streams v -show_entries stream=codec_name -of default=noprint_wrappers=1:nokey=1 '
      codec_check_pass=True;codec_name=''
      try:codec_name=os.popen(codec_name_cmd+'"'+vfs[0]+'"').readlines()[0].strip()
      except: codec_check_pass=False
      if not codec_name: codec_check_pass=False

      if codec_check_pass:
        for vfss in vfs[1:]:
          try: codec_name_cur=os.popen(codec_name_cmd+'"'+vfss+'"').readlines()[0].strip()
          except:
            print highlight('could not find codec_name for a prt file','red_fore')
            codec_check_pass=False
            break
          if codec_name_cur!=codec_name: 
            codec_check_pass=False
            break
      
      # ~ video_height=int(os.popen('ffprobe -v error -show_entries stream=height -of default=noprint_wrappers=1:nokey=1   "'+vfs[0]+'"').read().replace('N/A','').strip().replace(' ','').strip())
      # ~ video_height_check_pass=True
      # ~ for vfss in vfs[1:]:
        # ~ video_height_cur=int(os.popen('ffprobe -v error -show_entries stream=height -of default=noprint_wrappers=1:nokey=1   "'+vfss+'"').read().replace('N/A','').strip().replace(' ','').strip())
        # ~ if video_height_cur!=video_height: 
          # ~ video_height_check_pass=False
          # ~ break
      
      #concat if checks pass. else continue with next loop elem
      if codec_check_pass : #and video_height_check_pass:
        # ~ print highlight('passed codec check. All files in %s have same codec type as file_0: ' %(str(vfs)),'green_fore')+highlight('%s' %(codec_name) , 'green')
        os.system('enc ul concat quiet')
        os.system('cat tmpfile >> delfile')
      else:
        print highlight('failed in checking codec. All files in %s dont have same codec type as file_0: %s \nRemoving all files and continuing' %(str(vfs),codec_name),'red')
        #remove files and continue
        for vfss in vfs: os.remove(vfss)
        continue
      
      
      #delete the chunk files
      for i in chunk_map[base_file]:
        if os.path.exists(i[0]): os.remove(i[0])
      #rename concat.mkv to base_file
      if os.path.exists('concat.mkv'): os.rename('concat.mkv',base_file)
      if debug:
        print 'before mv base_file: '+base_file
        vfiles=[i for i in os.listdir('.') if i.endswith(video_extensions)]
        print 'video files in curdir: '+str(vfiles)
      os.system('mv  "'+base_file.strip()+'" /tmp/test123/')
      finished.append(base_file)
      joined_files+=1
      place_loc='%d/%d' %(joined_files,len(keyl))
      if verbose:
        print '%s) joined %d chunks: ' %(place_loc,len(chunk_map[base_file]))+highlight('%s' %(base_file),'green')
    # ~ for i in finished:
      # ~ if os.path.exists('"../'+base_file+'"'):    os.system('mv "../'+base_file+'" . -v')
    os.system('mv -f /tmp/test123/*.mkv .  && rmdir -v /tmp/test123/')
    print highlight('------\nDONE JOINING FILES!','green')
    # ~ os.system('ls --color')
    if (sys.argv[-1].startswith(('jr','js')) and sys.argv[-1][2:].strip().isdigit()) or (sys.argv[-1].startswith('j') and sys.argv[-1][1:].strip().isdigit()):
      account_to_upload_joined_files=-1
      if sys.argv[-1].startswith(('jr','js')):
        account_to_upload_joined_files=int(sys.argv[-1][2:].strip())
      else:
        account_to_upload_joined_files=int(sys.argv[-1][1:].strip())
      if sys.argv[-1].startswith('js'):
        print 'automatically uploading joined files to slarch://%d and deleting "partial" chunk files  in "delfile" '%(account_to_upload_joined_files)
      else:
        print 'automatically uploading joined files to mf://%d and deleting "partial" chunk files  in "delfile" '%(account_to_upload_joined_files)
      # ~ rem_space=mediafire_get_space_remaining(account_to_upload_joined_files,use_long_mfspace=True)
      rem_space=''
      if sys.argv[-1].startswith('js'):
        # ~ allinfo,totsize,total=solidfiles_queryfiles(account=account_to_upload_joined_files,verbose=False,get_space_only=False,printall=False,archival='archival')
        rem_space=9000. #always works
      else:
        rem_space=float(os.popen('vd mediafire_get_space_remaining '+str(account_to_upload_joined_files)).read().strip())
      video_files_size=sum([float(os.stat(fff).st_size) for fff in os.listdir('.') if fff.endswith('.mkv')])/(1024.**2)
      # ~ up_cmd='enc ml&&vd lum .vlist '+str(account_to_upload_joined_files)+' && vd pf delfile'
      up_cmd='enc ml&&enc ul bwf&&vd tdmlimited .vlist use_long_mfspace '+str(account_to_upload_joined_files)
      if sys.argv[-1].startswith('js'):
        up_cmd='enc ml&&enc ul bwf&&vd lus .vlist '+str(account_to_upload_joined_files)+' arch '
      if video_files_size< 0.9*rem_space: #can safely use lum instead of tdm
        print highlight('size of vid_file: %.1f mb is less than space remaining: %.1f mb in mf://%d.  Using "lum" in place of "tdm"' %(video_files_size,rem_space,account_to_upload_joined_files),'green_fore_bright')
        up_cmd=up_cmd.replace('vd tdmlimited .vlist use_long_mfspace','vd lumlimited .vlist ')
      os.system(up_cmd);
      os.system('cat delfile|grep mf- > delfile.mf&&vd pf delfile.mf');
      os.system('cat delfile|grep gd- > delfile.gd&&vd pf delfile.gd f')
    # ~ os.system('ls --color')
    

def video_get_montage(vid,delete_orig_file=False,verbose=True):
  if systype in ['cloudshell'] and not os.path.exists('/usr/bin/montage'):
    print highlight('"montage" not found on cloudshell. insatalling...','yellow_fore')
    x=os.popen3('sudo apt-get install imagemagick -y')[1].read()

  if vid=='.vlist':
    b=[i.strip().replace('./','') for i in open(vid).readlines() if i.strip() if 'montages/' not in i]
  else:
    b=[vid]
  os.system('mkdir -pv montages')
  prefs=parse_config(verbose=False)
  num_images=6;montage_size=272;
  if prefs.has_key('num_images'): num_images=int(prefs['num_images'])
  if prefs.has_key('montage_size'): montage_size=int(prefs['montage_size'])
  
  for vid in tqdm.tqdm(b,desc='making montages'):
    cmd='ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "'+vid+'"  | xargs printf %.0f'
    tlen_seconds=float(os.popen(cmd).read().strip())
    tlen_min=tlen_seconds/60.

    #get metadata
    if os.path.exists('tmp.metadata'):os.remove('tmp.metadata')
    meta_cmd=ffmpeg_cmd_name+' -hide_banner  -loglevel quiet -i "'+vid+'" -f ffmetadata tmp.metadata -y'
    os.system(meta_cmd)
    
    if tlen_min<10:                         num_images=4; montage_size='320x180'
    elif tlen_min>=10 and tlen_min<30:      num_images=6; montage_size='424x240'
    elif tlen_min>=30 and tlen_min<60:      num_images=8; montage_size='480x272'
    elif tlen_min>=60 and tlen_min<120:     num_images=12;montage_size='640x360'
    else:                                   num_images=16;montage_size='640x360'
    toget=[]
    for j in range(1,num_images+1):
      tpoint=(tlen_seconds*(j/float(num_images))) - (tlen_seconds/(float(num_images)*2))
      cmd=ffmpeg_cmd_name+' -hide_banner -loglevel quiet -ss '+str(int(tpoint))+' -i "'+vid+'" -vframes 1 -an -s 480x272 -f image2 '+str(j)+'.jpg'
      os.system(cmd)
    vtext='%.1f min' %(tlen_min)
    # ~ cmd="convert -background black -size 480x272 -fill '#ff0080'  -pointsize 72 -gravity center label:'"+vtext+"' 12.jpg"
    # ~ os.system(cmd)
    
  # ~ make montage
    vname=os.path.split(os.path.splitext(vid)[0])[1]
    cmd='montage '
    for j in range(1,int(num_images)+1):
      filename=str(j)+'.jpg'
      if os.path.exists(filename): cmd+=filename+' ';   
    cmd+=' -geometry +0+0 "montages/'+vname+'.png"'
    if 'verbose' in sys.argv[-1]: print cmd
    x=os.popen3(cmd);x=x[1].read()
    # ~ #add duration info strip at top
    # ~ cmd='convert "montages/'+vname+'.png" -gravity North -background Grey  -splice 0x10 -annotate +0+2 "'+vtext+'" tmp.png'
    # ~ x=os.popen3(cmd);x=x[1].read()
    # ~ cmd='mv -f tmp.png "montages/'+vname+'.png"'
    # ~ x=os.popen3(cmd);x=x[1].read()
  
    
    # ~ convert to webp
    # ~ cmd='cwebp -quiet -m 6 -q '+webp_quality+' -resize  640 360 montages/'+video_id+'.png -o  montages/'+vname+'.jpg'
    
    # ~ cmd='cwebp -quiet -m 6 -q '+webp_quality+' -resize  480 272 montages/'+vname+'.png -o  montages/'+vname+'.jpg'
    cpu_used=3
    if prefs.has_key('cpu_used'): cpu_used=int(prefs['cpu_used'])
    
    text_font='/usr/share/fonts/truetype/dejavu/DejaVuSerif.ttf';

    # ~ cmd=ffmpeg_cmd_name+' -loglevel quiet  -hide_banner   -i "montages/'+vname+'.png"  -s '+montage_size+' '
    cmd=ffmpeg_cmd_name+' -loglevel quiet  -hide_banner   -i "montages/'+vname+'.png" -i tmp.metadata -map_metadata 1 -s '+montage_size+' '
    cmd+=' -vf drawtext="fontfile='+text_font+':fontcolor=\'white\':text=\''+vtext+'\':fontsize=28:box=1:boxcolor=black@0.5:boxborderw=5:x=(w-text_w):y=0" '
    cmd+=' -c libaom-av1  -cpu-used '+str(cpu_used)+' -strict -2 -crf 37   -y "montages/'+vname+'.mkv"'
    os.system(cmd)
    if os.path.exists('montages/'+vname+'.png'): os.remove('montages/'+vname+'.png')
    # ~ if os.path.exists('montages/'+vname+'.jpg'): os.rename('montages/'+vname+'.jpg','montages/'+vname+'.mkv')
    
    for j in range(1,num_images+1):
      filename=str(j)+'.jpg'
      if os.path.exists(filename): os.remove(filename)
    if delete_orig_file:
      if os.path.exists(vid): os.remove(vid)
  
def porntrex_get_previews(infile,verbose=True):
  th_url='https://statics.cdntrex.com/contents/videos_screenshots/858000/858351/300x168/9.jpg?v=3'
  
  b=[i.strip() for i in open(infile).readlines() if 'porntrex.com/video' in i]
  found=[]
  os.system('mkdir -pv montages')
  webp_quality='45'
  for i in tqdm.tqdm(b,desc='getting ptrex previews'):
    video_id=i.split('/')[i.split('/').index('video')+1]   
    i2=i
    if i2.endswith('/'):i2=i2[:-1]
    vname='PORN__'+os.path.split(i2)[1].strip()+'-PTX'+video_id
    toget=[]
    for j in range(1,11):
      th_url='https://statics.cdntrex.com/contents/videos_screenshots/'+video_id[:-3]+'000/'+video_id+'/300x168/'+str(j)+'.jpg?v=3'
      toget.append(th_url)
    #download images
    a=open('linksfile.txt','w');
    for i in toget: a.write(i+'\n')
    a.close()
    os.system('aria2c -q -i linksfile.txt')
    
  # ~ make montage
    cmd='montage '
    for j in range(1,11):
      filename=str(j)+'.jpg'
      if os.path.exists(filename): cmd+=filename+' ';   
    cmd+=' -geometry +0+0 montages/'+video_id+'.png'
    if 'verbose' in sys.argv[-1]: print cmd
    x=os.popen3(cmd)
    x=x[1].read()

    
    # ~ convert to webp
    # ~ cmd='cwebp -quiet -m 6 -q '+webp_quality+' -resize  640 360 montages/'+video_id+'.png -o  montages/'+vname+'.jpg'
    cmd='cwebp -quiet -m 6 -q '+webp_quality+' -resize  424 240 montages/'+video_id+'.png -o  montages/'+vname+'.jpg'
    os.system(cmd)
    if os.path.exists('montages/'+video_id+'.png'): os.remove('montages/'+video_id+'.png')
    if os.path.exists('montages/'+vname+'.jpg'): os.rename('montages/'+vname+'.jpg','montages/'+vname+'.mp4')
    
    for j in range(1,11):
      filename=str(j)+'.jpg'
      if os.path.exists(filename): os.remove(filename)
  
def porntrex_get_video_urls_single(url):
  try:
    x=porntrex_get_video_urls_single_nocatch(url)
    return x
  except:
    return None
def porntrex_get_video_urls_single_nocatch(url):
    # ~ import demjson
    # ~ from requests.auth import HTTPBasicAuth
    # ~ auth=HTTPBasicAuth('shananalla88','nazgul0288')
    # ~ os.system('curl --silent -u shananalla88:nazgul0288 "'+url+'" -o tmp')
    htm_string=os.popen('wget -q --load-cookies="'+HOME+'/testpaw/cookies.spankbang.txt" "'+url+'" -O -').read().strip()
    if sys.argv[-1] in ['debug']:
      print 'lenhtm',len(htm_string)
      a=open('tmp','w');a.write(htm_string);a.close();
    # ~ htm_string=open('tmp').read();
    # ~ if os.path.exists('tmp'): os.remeve('tmp')
    # ~ soup=bs4.BeautifulSoup(requests.get(url,auth=auth).content,bsp)
    soup=bs4.BeautifulSoup(htm_string,bsp)
    video_url=soup('link',attrs={'rel':'canonical'})[0]['href']
    if video_url.endswith('/'): video_url=video_url[:-1]
    video_id=video_url.split('/')[video_url.split('/').index('video')+1]
    
    # ~ x=[i for i in soup('script',attrs={'type':'text/javascript'}) if 'flashvars' in i.text]
    x=[str(i) for i in soup('script',attrs={'type':'text/javascript'}) if 'flashvars' in str(i)]
    if sys.argv[-1] in ['debug']: print 'lemnmx',len(x)
    if not x: return None    
    # ~ content=x[0].text
    content=x[0]
    content=content[content.find('var flashvars = ')+16:]
    content=content[:content.find('};') + 1]
    dm={}
    # ~ dm=demjson.decode(content.strip())    
    try:
      content=content[1:-1].replace('\n','').replace('\t','').strip()
      for pp in content.split(', '):
        opt=pp.strip().split(':')[0].strip()
        val=':'.join(pp.strip().split(':')[1:]).strip().replace('"','').replace("'",'')
        dm[opt]=val
    except:
      print 'error in loading json.flashvars deumped to ./debug'
      ad=open('debug','w');ad.write(content.strip());ad.close()
      return
    video_name='PORN__'+os.path.split(video_url)[1]+'-PTX'+video_id+'.mp4'
    video_url=''
    if dm.has_key('video_url'): video_url=dm['video_url']
    if sys.argv[-1] in ['debug']: print dm
    if not dm.has_key('video_url_text'): return (video_url,video_name,video_id) #means its a single url probably
    
    if '480p' in dm['video_url_text']: video_url=dm['video_url']
    else:
      for k in dm:
        if k.startswith('video_alt_url') and k.endswith('text'): 
          if '480' in dm[k]:
            k2=k.replace('_text','')
            if k2 in dm:
              video_url=dm[k2]
    if video_url:
      return (video_url,video_name,video_id)
    else: 
      return None
  # ~ except:
    # ~ return None
    
def porntrex_get_video_urls(infile,verbose=True):
  b=[i.strip() for i in open(infile).readlines() if 'porntrex.com/video' in i]
  b2=[i.strip() for i in open(infile).readlines() if i.startswith('ptrex-')]
  if b2: #logfile
    b3=[]
    for i in b2:
      vidd=i.split('::')[3].strip()
      vname=i.split('::')[1].strip().split('---')[0].replace('PORN__','').replace('.mp4','')
      vurl='https://www.porntrex.com/video/'+vidd+'/'+vname
      if sys.argv[-1] in ['debug']: print vurl
      b3.append(vurl)
    b=b3
  # ~ try:
    # ~ import demjson
  # ~ except ImportError:
    # ~ x=os.popen3('pip2 install --user demjson')[1].read()
    # ~ import demjson
  # ~ found=[]
  found=imap_bar_serial(porntrex_get_video_urls_single,b,5,False,desc='getting ptrex urls')
  
  if found:
    found=[i for i in found if i]
    a=open('linksfile.txt','w')    
    for i in found: a.write(i[0]+'\n\tout='+i[1]+'\n')
    a.close()
  if sys.argv[-1] not in ['nomklog','nolog']:
    a=open('.ptrexlog','w')
    fz=imap_bar(get_filesize_of_url_basic,[j[0] for j in found],5,False,desc='getting filesizes')
    sep='  ::  '
    raw_data=[]
    for i in range(len(found)):
      fsize=fz[i]
      i=found[i]
      line='ptrex-0'+sep+i[1]+sep+'%.1d mb' %(fsize)+sep+i[2]
      raw_data.append(line)
      a.write(line+'\n')
      
    a.close()
    print 'wrote %d/%d lines to .ptrexlog' %(len(found),len(b))
    return raw_data
    
def cinewhale_process(infile,openload_account=20):
  
  try:
    import cfscrape  
  except ImportError:
    os.system('pip install -U --user cfscrape')    
    import cfscrape  
  cfscrape.DEFAULT_CIPHERS = 'TLS_AES_256_GCM_SHA384:ECDHE-ECDSA-AES256-SHA384'
  scraper = cfscrape.create_scraper()  # returns a CloudflareScraper instance

  b=[i.strip() for i in open(infile).readlines() if 'cinewhale.com' in i and not i.startswith('#')]
  
  for site in b:
    basepage=os.path.split(site)[1]
    if os.path.exists(basepage+'.htm'):
      soup=bs4.BeautifulSoup(open(basepage+'.htm').read(),bsp)
    else:
      soup=bs4.BeautifulSoup(scraper.get(site).content,bsp)
    x=[i['href'] for i in soup('a') if i.has_attr('href') and 'verystream.xyz' in i['href']]
    x=imap_bar(get_redirect_url,x,6,desc='redirecting verystream.xyz links')
    
    verystream_links=[i for i in x if 'verystream' in i]
    openload_links=[i for i in x if 'openload' in i]
    if verystream_links:
      a=open('tmpfile','w');
      for i in verystream_links: a.write(i+'\n')
      a.close()
      verystream_logfile_remote_upload(infile='tmpfile')
      os.system('cat l >> l2')
      print highlight('Upload %d links to vs:// with "ruvs ./l2 <account>' %(len(verystream_links)),'green_fore')
    
    if openload_links:
      a=open('tmpfile','w');
      for i in openload_links: a.write(i+'\n')
      a.close()
      os.system('vd openload_upload_to_my_account tmpfile '+str(openload_account))
    if os.path.exists('tmpfile'):os.remove('tmpfile')

def watch_episodes_process_parallel(infile='',verbose=False):
  if sys.argv[-1] in ['verbose']: verbose=True
  b=[i.strip() for i in open(infile).readlines() if 'watchepisodeseries' in i and not i.startswith('#')]
  if [i.strip() for i in open(infile).readlines() if 'cinewhale.com' in i and not i.startswith('#')]:
    return cinewhale_process(infile)
  results=[];  results_openload=[]  
  toprint=[];failed=[];
  timeout=60
  
  try:
    import cfscrape  
  except ImportError:
    os.system('pip install -U --user cfscrape')    
    import cfscrape  
  cfscrape.DEFAULT_CIPHERS = 'TLS_AES_256_GCM_SHA384:ECDHE-ECDSA-AES256-SHA384'
  scraper = cfscrape.create_scraper()  # returns a CloudflareScraper instance

  
  for site in b:
    basepage=os.path.split(site)[1]
    if os.path.exists(basepage+'.htm'):
      soup=bs4.BeautifulSoup(open(basepage+'.htm').read(),bsp)
    else:
      soup=bs4.BeautifulSoup(scraper.get(site).content,bsp)
  
  with tqdm.tqdm(b,desc='series:') as pbar1:
    
    for i in pbar1:
      min_season=1
      if '##' in i:        
        min_season=int(i.split('##')[1].strip())
        i=i.split('##')[0].strip()
      # ~ soup=bs4.BeautifulSoup(requests.get(i,timeout=timeout).content,bsp)
      # ~ soup=bs4.BeautifulSoup(scraper.get(i,timeout=timeout).content,bsp)
      soup=bs4.BeautifulSoup(scraper.get(i).content,bsp)
      episodes=soup(class_='el-item')
      series_name=soup('h2')[0].text.strip().replace(' Episodes','').replace("'",'').replace(",",'').replace(' ','.')
      pbar1.desc=series_name
      pbar1.refresh()
      episodes_found=0
      hlinks=[]
      for episode in episodes:
        season=episode(class_='season')[0].text.strip().lower().replace('season ','')        
        number=episode(class_='episode')[0].text.strip().lower().replace('episode ','')
        name=episode(class_='name')[0].text.strip()
        name=name.replace(' ','.')
        replace_chars=["'",",","?",":","!","+","/"]
        for replace_char in replace_chars:          name=name.replace(replace_char,'')        
        season=int(season);number=int(number)
        episode_given_name='%s.S%.2dE%.2d.%s' %(series_name,season,number,name)
        if type(episode_given_name)==unicode:  episode_given_name=episode_given_name.encode('ascii','replace')    
        if verbose: print episode_given_name
        if season<min_season: continue          
        hlink=episode('a')[0]['href']
        hlinks.append((hlink,episode_given_name))
      
      input_tuples=[(hlink[0],hlink[1],min_season,timeout,series_name) for hlink in hlinks] 
      #find links for each episode (parallel)
      if systype in ['xshellz']:
        output_tuples=imap_bar(watch_episodes_process_single_episode,input_tuples,3,True,desc='getting episode links')
      else:
        output_tuples=imap_bar(watch_episodes_process_single_episode,input_tuples,3,False,desc='getting episode links')
      #parse outputs
      for out in output_tuples:
        result,result_openload,output,episode_given_name=out
        
        if output: toprint.append(output)
        
        if result:                results.append(result)
        elif result_openload:     results_openload.append(result_openload)
        else:                     
          if episode_given_name!='SEASON0':
            failed.append(episode_given_name)
      
      
  if toprint:
    for i in toprint: print i
  a=open('./l','w')
  for i in results:    a.write(i+'\n')
  a.close()
  print 'wrote %d valid links to ./l' %(len(results))
  if results_openload:
    a=open('./l.2','w')
    for i in results_openload:    a.write(i+'\n')
    print 'wrote %d valid openload links to ./l.2' %(len(results_openload))
    a.close()
  if failed:
    a=open('./failed','w')
    for i in failed: a.write(i+'\n')
    a.close()

  
def csh_spawn():
  if systype=='cloudshell' and os.environ['USER'] in ['anirudh_iitm']:
    #check that accounts range is last arg
    accounts_present_in_last_arg_condition=('-' in sys.argv[-1]) and len(sys.argv[-1].split('-'))==2 and sys.argv[-1].split('-')[0].strip().isdigit() and sys.argv[-1].split('-')[1].strip().isdigit()
    try: assert(accounts_present_in_last_arg_condition)
    except AssertionError: 
      print 'running on gsh//500. Last arg must be range of gsh-accounts.\nGive like vd csh_spawn <start-gsh>-<end-gsh>'
    
    start_gsh=int(sys.argv[-1].split('-')[0].strip())
    end_gsh  =int(sys.argv[-1].split('-')[1].strip())
    next_gsh_sleep_seconds=22
    
    for gsh_account in range(start_gsh,end_gsh+1):
      print 'now trying to boost '+highlight('gsh://%d' %(gsh_account))
      csh_boosted=selenium_csh_boost(gsh_account,auto_spawn=True)
      if csh_boosted and csh_boosted in ['success']:
        print highlight('selenium_csh_boost() reported success. Uploading result to server.','green_fore')
        current_time_seconds=int(time.time())
        #write like <current_time_seconds , gsh_account_number>
        auto_boost_content='%d , %d' %(current_time_seconds,gsh_account)
        a=open('auto_boost','w');a.write(auto_boost_content);a.close()
        
        ftp_server=os.popen('source ~/.bash_aliases;echo $FTP_SERVER').read().strip()
        ftp_user=os.popen('source ~/.bash_aliases;echo $FTP_USER').read().strip()
        ftp_pass=os.popen('source ~/.bash_aliases;echo $FTP_PASS').read().strip()
        curl_ftp_upload_cmd='curl --progress-bar -T auto_boost "ftp://'+ftp_server+'" --user "'+ftp_user+':'+ftp_pass+'"'
        os.system(curl_ftp_upload_cmd)
        print highlight('finished uploading ./auto_boost. File contains\n%s\nContinuing with next gsh:// account in %d*(1-2) seconds' %(auto_boost_content,next_gsh_sleep_seconds),'bright')
        time.sleep(next_gsh_sleep_seconds+random.choice(range(next_gsh_sleep_seconds)))
      else:
        print highlight('selenium_csh_boost() reported failure.continuing with next gsh account','red_fore_bright')
        time.sleep((next_gsh_sleep_seconds/5.)+random.choice(range(int(next_gsh_sleep_seconds/5))))
        
        
  elif systype=='hashbang':
    allowed_time_difference=15;sleep_seconds=5;
    accounts_present_in_last_arg_condition=('-' in sys.argv[-1]) and len(sys.argv[-1].split('-'))==2 and sys.argv[-1].split('-')[0].strip().isdigit() and sys.argv[-1].split('-')[1].strip().isdigit()
    print 'running on hashbang. Will poll "auto_boost" file on server every %d seconds. If recent (within %d seconds) change is detected to the file, starts  gsh:// process new screen window\nExit with ctrl-c' %(sleep_seconds,allowed_time_difference)
    # ~ http_ftp_server=os.popen('source ~/.bash_aliases;echo $HTTP_FTP_SERVER').read().strip()
    #HACK!! shell on hashbang doesn't support "source".so hardcoded here
    http_ftp_server='guha67512.atwebpages.com'
    try:
      current_boosted_gsh_hashbang=-1;
      while 1>0:
        auto_boost_contents=os.popen('wget -q "'+http_ftp_server+'/auto_boost" -O -').read().strip()
        if not auto_boost_contents:
          print 'seems the auto_boost file on server was empty.retry in 1 sec'
          time.sleep(1)
          continue
        auto_boost_updated_time=int(auto_boost_contents.split(',')[0].strip())
        gsh_account_boosted=int(auto_boost_contents.split(',')[1].strip())
        current_time=int(time.time())
          
        time_difference=current_time-auto_boost_updated_time
        time_difference=time_difference if time_difference>0 else time_difference*-1; #absolute value of difference
        
        if time_difference<=allowed_time_difference and gsh_account_boosted!=current_boosted_gsh_hashbang:
          # ~ print '"auto_boost" on server was updated less than %d seconds ago and gsh boosted on server (gsh://%d) is different from last spawned gsh://%d\nspawning gsh://%d in new screen window' %(allowed_time_difference,gsh_account_boosted,current_boosted_gsh_hashbang,gsh_account_boosted)
          print 'spawning gsh://%d in new screen window' %(gsh_account_boosted)
          
          #need the last ';bash' to prevent window from closing when gsh(0 exits
          spawn_cmd='screen -t G%d bash -c "echo \'sleeping 15s for safety\';sleep 15s;TERM=screen CLOUDSDK_CONFIG=~/google-cloud-sdk/myconf/gcloud/gcloud%d   ~/google-cloud-sdk/bin/gcloud alpha cloud-shell ssh;TERM=screen bash ~/gssh.log;bash"' %(gsh_account_boosted,gsh_account_boosted)
          # ~ print "spawn_cmd: %s" %(spawn_cmd)
          os.system(spawn_cmd)
          
          current_boosted_gsh_hashbang=gsh_account_boosted
          print highlight('successfully opened new screen window for gsh://%d' %(current_boosted_gsh_hashbang),'green')
          
        time.sleep(sleep_seconds)
        
    except KeyboardInterrupt:
      print highlight('got ctrl-c. Exiting!!','green')
    
  else:
    print 'System not gsh://500 or hashbang\nusing this command we create boosted shells on gsh://500 and spawn new screen windows with those shells on hashbang.'
    print highlight('Exiting!!!','red')
    sys.exit(1)
