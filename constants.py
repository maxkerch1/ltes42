import os,sys,logging,lazy_import

HOME=os.getenv('HOME')+'/';

IS_LOCAL_MACHINE=False #true if local ubuntu pc.false on vps server
if  'ani' in os.uname()[1]:IS_LOCAL_MACHINE=True

if  'alwaysdata' in os.uname()[2]: sys.path.append(HOME+'/testpaw')

# ~ logging.getLogger("requests").setLevel(logging.WARNING) 
# ~ logging.getLogger("urllib3").setLevel(logging.WARNING)
# ~ logging=lazy_import.lazy_module('logging');

#IMPORTS
time=lazy_import.lazy_module('time');
json=lazy_import.lazy_module('json')
cPickle=lazy_import.lazy_module('cPickle')
math=lazy_import.lazy_module('math')
re=lazy_import.lazy_module('re')
multiprocessing=lazy_import.lazy_module('multiprocessing')
multiprocessing.dummy=lazy_import.lazy_module('multiprocessing.dummy')
random=lazy_import.lazy_module('random')
hashlib=lazy_import.lazy_module('hashlib')
urllib=lazy_import.lazy_module('urllib')

progressbar=lazy_import.lazy_module('progressbar')
tqdm=lazy_import.lazy_module('tqdm')
colorama=lazy_import.lazy_module('colorama')
bs4=lazy_import.lazy_module('bs4')
pprint=lazy_import.lazy_module('pprint')
traceback=lazy_import.lazy_module('traceback')
yaml=lazy_import.lazy_module('yaml')


#import all account info
from accounts import * 



metabase=HOME+'.python_scripts/vids_metadata/'
metabasesb=HOME+'.python_scripts/vids_metadata/'
num_connections=4

starting_solidfiles_account_for_temp_storage=2 #should be (last archival drive+1)
max_solidfiles_number_of_files_for_fillaccounts=15; #needed to avoid ban from solidfiles due to overload

num_mediafire_temp_storage_account=55
max_submissions_per_gdrive=10; #to avoid full
max_submit_margin_per_gdrive=1200.; #in MB,to avoid full

# ~ max_gdrive_account=len(gdpasses_multiup) #should be (max valid drive+1)
max_gdrive_account=54 #should be (max valid drive+1)
starting_gdrive_for_temp_storage=1 #should be (last archival drive+1)
# ~ max_gdrive_for_temp_storage=max_gdrive_account #only use upto 8,rest sendspace. Actual used is (this-1)
max_gdrive_for_temp_storage=27 #only use upto 8,rest sendspace. Actual used is (this-1)
gdrive_path_base=os.getenv('HOME')+'/work/gdrives/'
base_multiup_gdrive_account=27; #location of first gdrive account in multiup_accounts alist


  
#IDs are  set in ~/.env on heroku and openshift. 
hk_app_id='';oc_id='';platform_id='';scalingo_id=''
if os.path.exists(HOME+'/.env'):
  b=[i.strip() for i in open(HOME+'/.env').readlines() if i.strip()]
  b2=[i for i in b if 'HK_APP_ID=' in i]
  b3=[i for i in b if 'OC_NUM=' in i]
  b4=[i for i in b if 'SC_NUM=' in i]
  if b2:
    hk_app_id=b2[0].replace('HK_APP_ID=','').strip()
  elif b3:
    oc_id=b3[0].replace('OC_NUM=','').strip()
  elif b4:
    x=int(b4[0].replace('SC_NUM=','').strip())
    #oveeride based on worker , use sc_num to mul 5
    if os.environ.has_key('CONTAINER') and os.environ['CONTAINER'].startswith('worker'):
      scalingo_id=(5*(x-1))+int(os.environ['CONTAINER'].replace('worker-',''))
      # ~ if os.environ.has_key('APP') and os.environ['APP'][-1].isdigit():#multiply by 5*app
        # ~ scalingo_id+=(int(os.environ['APP'][-1])-1)*5 #means scalingo_app mult be s1,s2 etc for 1st,2nd etc

#platform_id is derived from app_name and branch_name
if os.environ.has_key('PLATFORM_BRANCH') and os.environ['PLATFORM_BRANCH']:
  platform_id=os.environ['PLATFORM_BRANCH'].replace('p','').strip()  
  platform_app_number=os.environ['PLATFORM_APPLICATION_NAME'].replace('platapp','').strip()
  if platform_id in ['master']:
    #master means 1 for platapp, 5 for platapp1 , 9 for platapp2 etc
    if platform_app_number: platform_id=str((4*int(platform_app_number))+1)
    else: platform_id='1'

#detect systype
systype='';uname=os.uname()
if 'ubuntu-' in uname[1] and 'mb-' in uname[1]: systype='vpsserver';
elif 'shananalla88' in uname[1] or 'jan781198791' in  uname[1]: systype='paiza'
elif 'goorm' in uname[1]: systype='goorm'
elif uname[1] in ['to1','de1']: systype='hashbang'
elif 'devshell' in uname[1]: systype='cloudshell'
elif 'xshell' in uname[1]: systype='xshellz'
elif  'alwaysdata' in uname[2]: systype='alwaysdata'
elif  'ubu16_test' in uname[1]: systype='codeanywhere'
elif  'time4vps' in uname[1]: systype='time4vps'
elif  'fuman' in uname[1]: systype='dediserve'
elif  scalingo_id: systype='scalingo'
elif  platform_id: systype='platform'
elif  hk_app_id or os.path.exists(HOME+'/Procfile'): systype='heroku'
elif  oc_id or 'blog-django-py' in uname[1] or 'os-sample' in uname[1]: systype='openshift'

if os.popen('whoami').read().strip()=='anirudh_iitm': systype='cloudshell'
bsp='lxml'
if systype in ['xshellz','alwaysdata'] or platform_id or scalingo_id:  bsp='html.parser';  

#useful tuples. There should be NO overlap between "hosters" and "heroku_clones"
hosters=('mf-','gd-','sl-','slarch-','slarch2-','sb-','xh-','ph-','ypp-','vshr-','xv-','sn-','bp-','pg-','ptrex-','mg-','goun-','pmk-','full-','vb-','ytdl-','mxd-')
heroku_clones=('pl','hk','oc','ga','az','sc')
video_extensions=('.avi','.mp4','.mkv','.MKV','.rmvb','.vob','.3gp','.webm','.flv','.m4v','.mpg')
image_extensions=('.jpg','.jpeg','.png','.gif','.webp','.ivf','.avif','.JPG','.JPEG','.PNG','.IVF')


ffmpeg_cmd_name='parg'
if IS_LOCAL_MACHINE:  ffmpeg_cmd_name='ffmpeg'


#functions to download (via http) and upload (via ftp) to guha and thorondor
curl_ftp_upload=lambda up_file: os.system('curl --silent -T "'+up_file+'" "ftp://'+ftp_server+'" --user "'+ftp_user+':'+ftp_pass+'"')
curl_ftp_upload2=lambda up_file: os.system('curl --silent -T "'+up_file+'" "ftp://'+ftp_server2+'" --user "'+ftp_user2+':'+ftp_pass2+'"')

def curl_ftp_download(down_file):
  os.system('rm -f "'+down_file+'"&&curl --silent "http://'+http_ftp_server+'/'+down_file+'" -o "'+down_file+'"')
  while not os.path.exists(down_file): #repeat command
    print 'downloading '+down_file+' failed.retrying in 2 sec..'
    time.sleep(2)
    os.system('rm -f "'+down_file+'"&&curl --silent "http://'+http_ftp_server+'/'+down_file+'" -o "'+down_file+'"')

def curl_ftp_download2(down_file):
  os.system('rm -f "'+down_file+'"&&curl --silent "http://'+http_ftp_server2+'/'+down_file+'" -o "'+down_file+'"')
  while not os.path.exists(down_file): #repeat command
    print 'downloading '+down_file+' failed.retrying in 2 sec..'
    time.sleep(2)
    os.system('rm -f "'+down_file+'"&&curl --silent "http://'+http_ftp_server2+'/'+down_file+'" -o "'+down_file+'"')

#accounts to use for gen and pn files
mf_account_pp=447
mf_account_gen=423
gd_account_pp=115
gd_account_gen=114
rclone_available_gd_account=1
