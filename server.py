#!/usr/bin/env python3
"""
Very simple HTTP server in python for logging requests
Usage::
    ./server.py [<port>]
"""
from http.server import BaseHTTPRequestHandler, HTTPServer
import os, logging,colorama,time,pprint,gzip

hk_server_id='';
if os.path.exists(os.getenv('HOME')+'/.env'):
  b=[i.strip() for i in open(os.getenv('HOME')+'/.env').readlines() if i.strip()]
  b2=[i for i in b if 'HK_SERVER_ID=' in i]
  if b2:
      hk_server_id=b2[0].replace('HK_SERVER_ID=','').strip()
      hk_server_id=int(hk_server_id)

server2='https://boiling-coast-14874.herokuapp.com'

http_ftp_server='guha67512.atwebpages.com'

#build it every time, since we want to reload
# ~ hmap={};#reverse map
# ~ for k in hmap0:    hmap[hmap0[k]]=k

latest_top={}
latest_jobs={}

highlight_begin=colorama.Back.BLACK+colorama.Fore.WHITE+colorama.Style.BRIGHT
highlight_yellow_fore_bright=colorama.Back.BLACK+colorama.Fore.YELLOW+colorama.Style.BRIGHT
highlight_red=colorama.Back.BLACK+colorama.Fore.RED+colorama.Style.BRIGHT
highlight_reset=colorama.Back.RESET+colorama.Fore.RESET+colorama.Style.RESET_ALL
    
def context_highlight(mem):
    m=float(mem.replace('MB','').strip())
    
    if m<50: # worker has restarted
        return highlight_begin+mem+highlight_reset
    elif m<150 and m>=50:
        return highlight_yellow_fore_bright+mem+highlight_reset
    elif m<480 and m>=150:
        return mem
    elif m>=480 and m<500:
        return colorama.Fore.RED+colorama.Style.BRIGHT+mem+highlight_reset
    else:
        return highlight_red+mem+highlight_reset
        
def job_string_parse(acc,job):

    filename,encode_config,encode_chunk,job_start_time=job    
    
    encode_config=encode_config.replace('mca','').strip()
    elapsed_time=int((time.time()-job_start_time)/60.)
    elapsed_time='%.2d min' %(elapsed_time)    
    if acc.startswith('hk'):  acc='hk%.2d' %(int(acc[2:].strip()))
    res='%-5s %-50s : %-8s : %-24s : %-10s' %(acc,filename[:50],encode_chunk,encode_config,elapsed_time)        
    return res
    
class S(BaseHTTPRequestHandler):
    
    hmap0={
        1:  'd.e5604598-dd03-4781-b5b5-5f4e59079ae9',
        2:  'd.664c918d-9509-4c26-b00f-8340d76836fb',
        3:  'd.66eb88d1-406a-4484-8776-676f4e7d0149',
        4:  'd.11e213eb-43e6-4001-9268-06b883bb1249',
        5:  'd.e566260c-9e2e-45a8-9301-4f1c6199b5bb',
        6:  'd.839c9deb-e8d7-47bd-883e-dd87b865fa9c',
        7:  'd.59d7e756-9c7b-496a-af7f-ec64f2636851',
        8:  'd.7f3673c0-ec30-498f-99d8-08acece40775',
        9:  'd.0b8a1e20-b8ce-4a2b-8828-a9bee8a0ae0a',
        10: 'd.1f7e9199-da55-4688-8736-3d5e312445ae',
        11: 'd.60712afd-1080-4da6-9450-4a3c1d552a79',
        12: 'd.a782fb24-778e-47d4-bff8-b40efdb30251',
        13: 'd.16593a89-0cad-4111-9728-4a5abe9991ce',
        14: 'd.e53b83b6-38f3-4aa7-bd53-3a22855aae09',
        15: 'd.0a251040-011c-47a3-8a77-e0725338f363',
        16: 'd.1eca7552-e2a4-47fe-a709-7c1b30449619',
        17: 'd.b289e9bc-3d8d-4a2b-aff5-cdd59b73d2cc',
        18: 'd.78cc9387-cafa-49b2-a7ef-042d946aa082',
        19: 'd.0184ba11-ac2a-49be-8f57-01a62d0c5998',
        20: 'd.837f6a56-fcb4-4700-b95f-a6f27e0dbe19',
        21: 'd.cee86621-0b25-4365-8017-ca0c18ff4027',
        22: 'd.6b4dfd57-72fb-443a-969e-445371b44bbd',
        23: 'd.af91d53f-4c13-4c84-a91b-31f67a214178',
        24: 'd.2421818d-cf3f-48bc-be61-658cd79fbf21',
        25: 'd.33340df1-ff65-41e6-8bee-0ea3bf667621',
        26: 'd.0c1d4fe4-7a54-4352-a353-75a869e2d3ea',
        27: 'd.18c322ba-0706-49cd-ac69-3182bb33d1ca',
        28: 'd.05be265d-e8e5-4f05-a3b3-ce46efe1ddff',
        29: 'd.1e71f808-50ae-4c53-898c-c7605c9ce1ca',
        30: 'd.e0718c82-7a2a-48ae-bee3-10abc22e8c09',
        31: 'd.a687ff47-5284-4278-bce9-d51cc4dbea1e',
        32: 'd.b0842e01-1e03-479c-bc16-691d6663d5de',
        33: 'd.f214dc25-dc63-4bcc-b0df-66453881bf4f',
        34: 'd.e9189a5e-6a2a-4774-a101-c643e7ada24e',
        35: 'd.a07036c8-ac99-4979-97a6-9821a0006ef1',
        36: 'd.d9c27354-8def-4fa5-92ad-b3a69549f9e7',
        37: 'd.cf42b8d9-2195-4149-ac7b-2837008176d8',
        38: 'd.55a4ea4f-5b59-4d90-8aa4-67243b4968b4',
        39: 'd.d69801c4-b642-4ccb-b939-43aec5bcfe03',
        40: 'd.fb8f0a87-bfb9-4427-be8d-0770fa0f2a30',
    }
    
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        path=self.path;
        if 'hktoppu' in path:
            logging.info('Got a HKTOPPU request!latest top:\n%s',str(latest_top))
            self._set_response();rs='';
            table_rows=5
            table_rows_cnt=0
            for key, value in sorted(latest_top.items()): 
                rs+='%s: ' %(key)+context_highlight('%s' %(value))
                table_rows_cnt+=1
                key2=int(key[2:])
                if key2%5==0 or key in []:
                    table_rows_cnt=0
                    rs+='\n'
                else:
                    rs+='   '
            rs=rs.encode('utf-8')
            # ~ if hk_server_id==1:
                # ~ x=os.popen('curl --silent '+server2+"/hktoppu").read().strip()
                # ~ rs+=x
            self.wfile.write(rs)
        elif 'gztoppu' in path:
            logging.info('Got a GZTOPPU request!latest top:\n%s',str(latest_top))
            self._set_response();rs='';
            table_rows=5
            table_rows_cnt=0
            for key, value in sorted(latest_top.items()): 
                rs+='%s: ' %(key)+context_highlight('%s' %(value))
                table_rows_cnt+=1
                key2=int(key[2:])
                if key2%5==0 or key in []:
                    table_rows_cnt=0
                    rs+='\n'
                else:
                    rs+='   '
            rs=rs.encode('utf-8')
            #do gzip
            # ~ content=rs.encode('zlib')   
            content = gzip.compress(bytes(str(rs),'utf-8'))
            # ~ self.send_header("Content-length", str(len(str(content))))
            # ~ self.send_header("Content-Encoding", "gzip")
            # ~ self.end_headers()
            #send gzipped content
            self.wfile.write(content)
        elif 'reload_drains' in path:
            drains_url='http://'+http_ftp_server+'/drains'
            wget_cmd='wget -q "'+drains_url+'" -O - '
            drains_info=os.popen(wget_cmd).readlines()
            drains_info=[i.strip() for i in drains_info if i.strip()]

            new_drains={}
            for line in drains_info:
                ac=line.split(':')[0].strip()
                if not ac.isdigit():
                    print('entry: %s did not have account of type digit' %(line))
                ac=int(ac)
                dr=line.split(':')[1].strip().replace('"','').replace("'","").replace(',','')
                new_drains[ac]=dr            

            # ~ hmap={}
            # ~ for k in new_drains:    hmap[new_drains[k]]=k
            logging.info('Got a RELOAD_DRAINS request!\n')
            print('original drains dict:')
            pprint.pprint(self.hmap0)
            for k in new_drains: self.hmap0[k]=new_drains[k]         

            print('latest drains dict:')
            pprint.pprint(new_drains)
            # ~ print('\n\nreverse-dict::')
            # ~ pprint.pprint(hmap)
            self._set_response()
            self.wfile.write("GET request for {}".format(self.path).encode('utf-8'))

        elif 'jobs' in path:
            logging.info('Got a JOBS request!latest jobs:\n%s',str(latest_jobs))
            self._set_response();rs='';
            for key, value in sorted(latest_jobs.items()): rs+=job_string_parse(key,value)+'\n'
            rs=rs.encode('utf-8')
            # ~ if hk_server_id==1:
                # ~ x=os.popen('curl --silent '+server2+"/jobs").read().strip()
                # ~ rs+=x
            self.wfile.write(rs)

        else: 
            logging.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
            self._set_response()
            self.wfile.write("GET request for {}".format(self.path).encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself
        
        path=self.path;headers=self.headers;raw_data=post_data.decode('utf-8')
        
        if 'Content-Type' in headers and headers['Content-Type'].strip()=='application/logplex-1':
            #is a logging POST msg
            if 'Logplex-Drain-Token' in headers:
                drain_token=headers['Logplex-Drain-Token']
                hmap={};#reverse map
                for k in self.hmap0:    hmap[self.hmap0[k]]=k
                if drain_token in hmap:
                    hk_acc=hmap[drain_token]
                    lines=str(raw_data).splitlines()

                    if [i for i in lines if 'memory_rss=' in i]: #process memory POSTS
                        memres=[i for i in lines if 'memory_rss=' in i][0]
                        memres=[i for i in memres.split('#') if 'memory_rss=' in i][0]
                        memres=memres.split('memory_rss=')[1].split()[0].strip()
                        latest_top['hk%.2d' %(hk_acc)]=memres
                        logging.info('updated TOP entry for h%s', str(hk_acc))
                        # ~ print('updated TOP entry for '+highlight_begin+'h%d'%(hk_acc)+highlight_reset)
                    elif [i for i in lines if 'load_avg_1m=' in i]:
                        pass; #ignore the load average messages for now
                    else: #log non-memory non-cpu POSTS
                        print(highlight_begin+'got POST from h%d' %(hk_acc)+highlight_reset)
                        logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",str(self.path), str(self.headers), post_data.decode('utf-8'))
        elif 'oc_full_ref=' in raw_data and 'cpu=' in raw_data:
            ss=raw_data.split('&');ds={}
            for i in ss:ds[i.split('=')[0].strip()]=i.split('=')[1].strip()
            oc_full_ref=ds['oc_full_ref']
            cpu=ds['cpu'];mem=str(ds['mem'])+'MB';
            latest_top[oc_full_ref]=mem
            logging.info('updated TOP entry for %s', (oc_full_ref))
        elif 'app_num=' in raw_data and 'encode_config=' in raw_data:
            ss=raw_data.split('&');ds={}
            for i in ss:ds[i.split('=')[0].strip()]=i.split('=')[1].strip()
            app_num=ds['app_num']
            filename=ds['filename']
            encode_chunk=ds['encode_chunk']
            encode_config=ds['encode_config'].replace('+',' ')
            job_start_time=time.time()
            latest_jobs[app_num]=(filename,encode_config,encode_chunk,job_start_time)
            logging.info('updated latest JOB entry for %s', str(app_num))
            logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",str(self.path), str(self.headers), post_data.decode('utf-8'))
        else: #log non-drain token posts
            print(highlight_begin+'got POST '+highlight_reset)
            logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",str(self.path), str(self.headers), post_data.decode('utf-8'))
            
        
        # ~ logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",
                # ~ str(self.path), str(self.headers), post_data.decode('utf-8'))

        self._set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))

def run(server_class=HTTPServer, handler_class=S, port=8080):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')

if __name__ == '__main__':
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run(port=int(os.environ['PORT']))



###### CODE for ocmon.py
# ~ #!/usr/bin/python2.7

# ~ import os,time
# ~ try:
  # ~ import unirest
# ~ except ImportError:
  # ~ print 'Installing unirest'
  # ~ os.system('pip2 install --user unirest')
  # ~ import unirest
  
# ~ heroku_logging_app='https://fast-forest-56891.herokuapp.com/'

# ~ oc_id='';oc_sub_id=0;oc_full_ref='';HOME=os.getenv('HOME')
# ~ sleep_interval=60

# ~ if os.path.exists(HOME+'/.env'):
  # ~ b=[i.strip() for i in open(HOME+'/.env').readlines() if 'OC_NUM=' in i]
  # ~ if b: oc_id=b[0].replace('OC_NUM=','').strip()
# ~ unm=os.uname()[1]

# ~ if oc_id:  
  # ~ if 'blog-django' in unm or 'oc11-' in unm or 'oc21-' in unm: oc_sub_id=1
  # ~ elif 'os-sample-php' in unm or 'oc12-' in unm or 'oc22-' in unm: oc_sub_id=2
  # ~ elif 'os-sample-python' in unm or 'oc13-' in unm or 'oc23-' in unm: oc_sub_id=3
  # ~ elif 'os-sample-ruby' in unm or 'oc14-' in unm or 'oc24-' in unm: oc_sub_id=4

# ~ if oc_id and oc_sub_id:  oc_full_ref='oc'+oc_id+str(oc_sub_id)

# ~ if __name__=='__main__':
  # ~ if oc_full_ref:
    # ~ print 'starting %s monitoring' %(oc_full_ref)
    # ~ while 1>0:
      # ~ cmd="top -b -d 1 -n 2 | awk '$1 == \"PID\" {block_num++; next} block_num == 2 {sum += $9;} END {print sum}'"
      # ~ cpu_use=os.popen(cmd).read().strip()
      # ~ mem=os.popen("ps -e --format rss  | awk 'BEGIN{c=0} {c+=$1} END{print c/1024}'").read().strip()
      # ~ mem='%.2f' %(float(mem))
      # ~ data={"oc_full_ref": oc_full_ref,"cpu": cpu_use, "mem": mem }
      # ~ response = unirest.post(heroku_logging_app,params=data)
      # ~ print 'posted: %s cpu: %s mem: %s' %(oc_full_ref,cpu_use,mem)
      # ~ time.sleep(sleep_interval)
  # ~ else:
    # ~ print 'seems this is not openshift system.exiting'
    

##FLASK helper app on alwaysdata
# ~ from flask import Flask
# ~ import os,sys
# ~ home=os.environ['HOME']
# ~ sys.path.append(home+'/testpaw/')
# ~ sys.path.append(home+'/.local/lib/python2.7/site-packages/')
# ~ import enc

# ~ app = Flask(__name__)

# ~ @app.route('/aj')
# ~ def index():
    # ~ x=enc.autojobs_status(return_string=True,autojobs_file='autojobs');return x
# ~ @app.route('/aj2')
# ~ def index():
    # ~ x=enc.autojobs_status(return_string=True,autojobs_file='autojobs2');return x
# ~ @app.route('/ajr')
# ~ def index():
    # ~ x=enc.autojobs_status(return_string=True,autojobs_file='rem_autojobs');return x
