"""Example Python hooks for ELinks.

If ELinks is compiled with an embedded Python interpreter, it will try
to import a Python module called hooks when the browser starts up. To
use Python code from within ELinks, create a file called hooks.py in
the ~/.elinks directory, or in the system-wide configuration directory
(defined when ELinks was compiled), or in the standard Python search path.
An example hooks.py file can be found in the contrib/python directory of
the ELinks source distribution.

The hooks module may implement any of several functions that will be
called automatically by ELinks in appropriate circumstances; it may also
bind ELinks keystrokes to callable Python objects so that arbitrary Python
code can be invoked at the whim of the user.

Functions that will be automatically called by ELinks (if they're defined):

follow_url_hook() -- Rewrite a URL for a link that's about to be followed.
goto_url_hook() -- Rewrite a URL received from a "Go to URL" dialog box.
pre_format_html_hook() -- Rewrite a document's body before it's formatted.
proxy_for_hook() -- Determine what proxy server to use for a given URL.
quit_hook() -- Clean up before ELinks exits.

"""
import elinks

import sys,os
HOME=os.getenv('HOME')+'/'

IS_LOCAL_MACHINE=False #true if local ubuntu pc.false on vps server
if  'ani' in os.uname()[1]:IS_LOCAL_MACHINE=True

pdirs=['/root/testpaw', '/root/.python_scripts/scripts', '/root/work', '/usr/lib/python2.7', '/usr/lib/python2.7/plat-x86_64-linux-gnu', '/usr/lib/python2.7/lib-tk', '/usr/lib/python2.7/lib-old', '/usr/lib/python2.7/lib-dynload', '/root/.local/lib/python2.7/site-packages', '/usr/local/lib/python2.7/dist-packages', '/usr/lib/python2.7/dist-packages',HOME+'.python_scripts/scripts', HOME+'work', '/usr/alwaysdata/python/2.7.15/lib/python27.zip', '/usr/alwaysdata/python/2.7.15/lib/python2.7', '/usr/alwaysdata/python/2.7.15/lib/python2.7/plat-linux2', '/usr/alwaysdata/python/2.7.15/lib/python2.7/lib-tk', '/usr/alwaysdata/python/2.7.15/lib/python2.7/lib-old', '/usr/alwaysdata/python/2.7.15/lib/python2.7/lib-dynload', HOME+'.local/lib/python2.7/site-packages', '/usr/alwaysdata/python/2.7.15/lib/python2.7/site-packages' ]

sys.path.extend([d for d in pdirs if os.path.exists(d) ])

import re

links_remove=['icon','wlwmanifest','preconnect','profile','preload','manifest','canonical','alternate','edit','search','amphtml','dns-prefetch','prefetch','publisher','shortcut','EditURI','license','assets','pingback','shortlink','image_src',re.compile('apple*'),'next','prev']
# ~ bsp='html.parser';  
bsp='html.parser';  

from bs4 import BeautifulSoup

# ~ dumbprefixes = {
        # ~ "7th" : "http://7thguard.net/",
        # ~ "b" : "http://babelfish.altavista.com/babelfish/tr/",
        # ~ "bz" : "http://bugzilla.elinks.cz/",
        # ~ "bug" : "http://bugzilla.elinks.cz/",
        # ~ "d" : "http://www.dict.org/",
        # ~ "g" : "http://www.google.com/",
        # ~ "gg" : "http://www.google.com/",
        # ~ "go" : "http://www.google.com/",
        # ~ "fm" : "http://www.freshmeat.net/",
        # ~ "sf" : "http://www.sourceforge.net/",
        # ~ "dbug" : "http://bugs.debian.org/",
        # ~ "dpkg" : "http://packages.debian.org/",
        # ~ "pycur" : "http://www.python.org/doc/current/",
        # ~ "pydev" : "http://www.python.org/dev/doc/devel/",
        # ~ "pyhelp" : "http://starship.python.net/crew/theller/pyhelp.cgi",
        # ~ "pyvault" : "http://www.vex.net/parnassus/",
        # ~ "e2" : "http://www.everything2.org/",
        # ~ "sd" : "http://www.slashdot.org/"
# ~ }

def goto_url_hook(url):
    """Rewrite a URL that was entered in a "Go to URL" dialog box.

    This function should return a string containing a URL for ELinks to
    follow, or an empty string if no URL should be followed, or None if
    ELinks should follow the original URL.

    Arguments:

    url -- The URL provided by the user.

    """
    pass
    # ~ if url in dumbprefixes:
        # ~ return dumbprefixes[url]

def follow_url_hook(url):
    """Rewrite a URL for a link that's about to be followed.

    This function should return a string containing a URL for ELinks to
    follow, or an empty string if no URL should be followed, or None if
    ELinks should follow the original URL.

    Arguments:

    url -- The URL of the link.

    """
    # ~ pass
    if url.startswith('https://www.reddit.com/'):
        return url.replace('https://www.reddit.com','https://www.m.reddit.com')
    else: pass
    # ~ if url.startswith('https://www.ft.com/content/'):
        # ~ return 'http://www.google.co.in/search?q='+url
    # ~ else: pass
    # ~ google_redirect = 'http://www.google.com/url?sa=D&q='
    # ~ if url.startswith(google_redirect):
        # ~ return url.replace(google_redirect, '')
        
def remove_script(s):  
  rem=s[s.find('<script'):]
  s=s[:s.find('<script')]
  rem=rem[rem.find('</script>')+9:]  
  while rem.find('<script')!=-1:
    s+=rem[:rem.find('<script')]
    rem=rem[rem.find('</script>')+9:]
  return s
  
def cleanup_guardian(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in soup('ul',attrs={'class':['dropdown-menu','menu-group','subnav__list']})]
    [i.decompose() for i in soup('link',attrs={'rel':links_remove})]
    [i.decompose() for i in soup('nav',attrs={'role':['navigation']})]
    [i.decompose() for i in soup('a',attrs={'class':['skip']})]
    return str(soup)
def cleanup_independent_uk(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in soup('link',attrs={'rel':links_remove})]    
    [i.decompose() for i in soup('header')]    
    [i.decompose() for i in soup('nav')]    
    [i.decompose() for i in soup('amp-sidebar')]    
    [i.decompose() for i in soup('figure')]    
    return str(soup)
    
def cleanup_thehindu(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in soup('div',attrs={'class':['justin-text-cont','artcl-social-media']})]
    # ~ [i.decompose() for i in soup('link') if i.has_attr('rel') and 'stylesheet' not in i['rel']]
    [i.decompose() for i in soup('link',attrs={'rel':links_remove})]
    [i.decompose() for i in soup(class_='socialize')] 
    [i.decompose() for i in  soup('noscript')]
    [i.decompose() for i in  soup(class_=['navbar'],attrs={'role':'navigation'})]
    return str(soup)
    
def cleanup_livemint(html):
    soup=BeautifulSoup(html,bsp)
    # ~ [i.decompose() for i in soup('link') if i.has_attr('rel') and 'stylesheet' not in i['rel']]
    [i.decompose() for i in soup('header',attrs={'id':'myheader'})]
    [i.decompose() for i in soup('link',attrs={'rel':links_remove})]
    [i.decompose() for i in soup('div',attrs={'class':['home-nav','artcl-social-media',re.compile('overlay*'),re.compile('sign*')]})]    
    [i.decompose() for i in soup(class_='navCategories')]    
    return str(soup)

def cleanup_manorama(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in soup('link',attrs={'rel':links_remove})]
    [i.decompose() for i in soup('nav',attrs={'class':['menu','navigation','main-menu']})]
    
    return str(soup)
    
def cleanup_ft(html):
    soup=BeautifulSoup(html,bsp);
    [i.decompose() for i in  soup('div',attrs={'class':['n-layout__row--header',re.compile('.*share*')]})]
    [i.decompose() for i in soup('link',attrs={'rel':links_remove})]
    for i in soup('a'):
        if i.has_attr('href'):
            i['href']='http://www.google.co.uk/search?q='+i['href']
    return str(soup)
def cleanup_github(html):
    soup=BeautifulSoup(html,bsp);
    [i.decompose() for i in soup('link',attrs={'rel':links_remove})]
    [i.decompose() for i in soup('nav')]
    return str(soup)
    
def cleanup_eztv(html):
    soup=BeautifulSoup(html,bsp);
    [i.decompose() for i in  soup('img',attrs={'src':re.compile('.*play2.png*')})]
    [i.decompose() for i in  soup('img',attrs={'data-cfsrc':re.compile('.*play2.png*')})]
    return str(soup)


def cleanup_washingtonpost(html):
    soup=BeautifulSoup(html,bsp);
    [i.decompose() for i in soup('link',attrs={'rel':links_remove})]    
    return str(soup)
def cleanup_economist(html):
    soup=BeautifulSoup(html,bsp);
    [i.decompose() for i in soup('link',attrs={'rel':links_remove})]
    [i.decompose() for i in  soup('div',attrs={'class':['navigation','newsletter-form','latest-updates-panel__container']})]
    return str(soup)
    
def cleanup_ie(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in soup('link',attrs={'rel':links_remove})]
    [i.decompose() for i in soup(class_=['menu','m-nav-list','g-menu','m-story-meta__share-actions'])]
    [i.decompose() for i in soup('div',attrs={'class':['navi','mainnav','adboxtop','adbox-border']})]
    # ~ [i.decompose()  if (i is not None) and i.has_attr('rel') and ]
    return str(soup)

def cleanup_the_wire(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in soup('link',attrs={'rel':links_remove})]
    # ~ [i.decompose()  if (i is not None) and i.has_attr('rel') and ]
    return str(soup)

def cleanup_business_standard(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in soup('div',attrs={'class':['m-header']})]
    return str(soup)
def cleanup_hindustan_times(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in  soup('link',attrs={'rel':links_remove})]
    [i.decompose() for i in soup('div',attrs={'class':['navList','htpop share_social']})]
    return str(soup)
def cleanup_ndtv(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in  soup('link',attrs={'rel':links_remove})]
    [i.decompose() for i in soup('div',attrs={'class':['scroll__nav']})]
    return str(soup)
    
def cleanup_tribune_pk(html):
    soup=BeautifulSoup(html,bsp)
    # ~ reg_navi=re.compile('.*nav*')
    [i.decompose() for i in  soup('link',attrs={'rel':links_remove})]
    [i.decompose() for i in soup('div',attrs={'class':['left-links','right-links','top-brand-bar','navborder']})]
    [i.decompose() for i in soup('div',attrs={'id':['top-bar']})]
    [i.decompose() for i in soup('ul',attrs={'class':['logo-links']})]
    # ~ [i.decompose() for i in soup('div',attrs={'id':['navigation']})]
    return str(soup)
    
def cleanup_wikipedia(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in  soup('link',attrs={'rel':links_remove})]
    [i.decompose() for i in  soup('ul',attrs={'id':['page-actions']})]
    return str(soup)
    
def cleanup_dawn(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in  soup('link',attrs={'rel':links_remove})]
    [i.decompose() for i in soup('div',attrs={'class':['float-left','adblock-alert','adblocker-modal','mailer','ie-alert']})]
    [i.decompose() for i in soup('ul',attrs={'class':['nav']})]
    # ~ [i.decompose() for i in soup('div',attrs={'id':['navigation']})]
    return str(soup)
    
def cleanup_bbc(html):
    soup=BeautifulSoup(html,bsp)
    reg_navi=re.compile('navi*')
    [i.decompose() for i in  soup('link',attrs={'rel':links_remove})]
    # ~ [i.decompose() for i in  soup('link',attrs={'rel':links_remove})]
    [i.decompose() for i in soup('div',attrs={'class':[reg_navi,re.compile("share*"),'navigation']})]
    return str(soup)
    
def cleanup_tfpdl(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in soup('div',attrs={'class':'main-menu'})]
    [i.decompose() for i in  soup('link',attrs={'rel':links_remove})]
    return str(soup)

def cleanup_mohfw(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in soup(class_=['accessibility-menu','header-sticky','canvas-menu'])]
    [i.decompose() for i in  soup('link',attrs={'rel':links_remove})]
    return str(soup)
def cleanup_scroll(html):
    soup=BeautifulSoup(html,bsp)
    # ~ [i.decompose() for i in soup('div',attrs={'class':'main-menu'})]
    [i.decompose() for i in soup(class_=['global-navigation','article-share-sheet'])]
    [i.decompose() for i in  soup('link',attrs={'rel':links_remove})]
    return str(soup)

def cleanup_rotten_tomatoes(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in soup('div',attrs={'id':['navMenu','mobile-nav','trending-list-wrap']})]
    [i.decompose() for i in soup('div',attrs={'class':['innerSubnav','subnav','trendingBar','dropdown-menu','modal-dialog']})]
    # ~ [i.decompose() for i in soup('ul',attrs={'class':['list-inline']})]
    [i.decompose() for i in  soup('link',attrs={'rel':links_remove})]
    return str(soup)
def cleanup_serakon(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in soup(class_='menu')]
    # ~ [i.decompose() for i in soup('ul',attrs={'class':['list-inline']})]
    [i.decompose() for i in  soup('link',attrs={'rel':links_remove})]
    return str(soup)
def cleanup_cricbuzz(html):
    soup=BeautifulSoup(html,bsp)
    # ~ [i.decompose() for i in soup(class_='menu')]
    # ~ [i.decompose() for i in soup('ul',attrs={'class':['list-inline']})]
    [i.decompose() for i in  soup('link',attrs={'rel':links_remove})]
    return str(soup)
def cleanup_cricbuzz_commentary(html):
    soup=BeautifulSoup(html,bsp)
    # ~ [i.decompose() for i in soup(class_='menu')]
    # ~ [i.decompose() for i in soup('a',attrs={'class':['pull-left','pull-right']})]
    [i.decompose() for i in soup('a',attrs={'href':re.compile('.*mobileapps*')})]
    [i.decompose() for i in soup('img',attrs={'alt':'Cricbuzz Logo'})]
#    [i.decompose() for i in soup('meta',attrs={'http-equiv':['refresh']})]
    [i.decompose() for i in  soup('link',attrs={'rel':links_remove})]
    return str(soup)
def cleanup_tribune_india(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in  soup('link',attrs={'rel':links_remove})]
    [i.decompose() for i in  soup('nav')]
    return str(soup)
def cleanup_highlightsguru(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in  soup('link',attrs={'rel':links_remove})]
    return str(soup)
def cleanup_mcumovieshome(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in  soup('link',attrs={'rel':links_remove})]
    [i.decompose() for i in  soup('li',attrs={'id':re.compile('.*menu*')})]
    [i.decompose() for i in  soup('ul',attrs={'id':re.compile('.*menu*')})]
    return str(soup)
def cleanup_theprint(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in  soup('link',attrs={'rel':links_remove})]
    [i.decompose() for i in  soup('li',attrs={'class':['menu-item']})]    
    return str(soup)

def cleanup_github(html):
    soup=BeautifulSoup(html,bsp)
    [i.decompose() for i in  soup('link',attrs={'rel':links_remove})]
    [i.decompose() for i in  soup('div',attrs={'class':['js-header-wrapper','unsupported-browser','header-search','signup-prompt','repohead-details-container','HeaderMenu']})]
    # ~ [i.decompose() for i in  soup('a',attrs={'class':['tooltipped','HeaderMenu-link']})]
    [i.decompose() for i in  soup('a',attrs={'class':['tooltipped']})]
    [i.decompose() for i in  soup('nav',attrs={'class':['reponav']})]
    [i.decompose() for i in  soup('nav',attrs={'aria-label':['Global']})]
    [i.decompose() for i in  soup('details',attrs={'class':['details-overlay']})]
    return str(soup)

  

def proxy_for_hook(url):
    """Determine what proxy server to use for a given URL.

    This function should return a string of the form "hostname:portnumber"
    identifying a proxy server to use, or an empty string if the request
    shouldn't use any proxy server, or None if ELinks should use its
    default proxy server.

    Arguments:

    url -- The URL that is about to be followed.

    """
    pass

def quit_hook():
    """Clean up before ELinks exits.

    This function should handle any clean-up tasks that need to be
    performed before ELinks exits. Its return value is ignored.

    """
    pass


# The rest of this file demonstrates some of the functionality available
# within hooks.py through the embedded Python interpreter's elinks module.
# Full documentation for the elinks module (as well as the hooks module)
# can be found in doc/python.txt in the ELinks source distribution.


# This class shows how to use elinks.input_box() and elinks.open(). It
# creates a dialog box to prompt the user for a URL, and provides a callback
# function that opens the URL in a new tab.
#
class goto_url_in_new_tab:
    """Prompter that opens a given URL in a new tab."""
    def __init__(self):
        """Prompt for a URL."""
        elinks.input_box("Enter URL", self._callback, title="Go to URL")
    def _callback(self, url):
        """Open the given URL in a new tab."""
        if 'goto_url_hook' in globals():
            # Mimic the standard "Go to URL" dialog by calling goto_url_hook().
            url = goto_url_hook(url) or url
        if url:
            elinks.open(url, new_tab=True)
# The elinks.bind_key() function can be used to create a keystroke binding
# that will instantiate the class.
#
elinks.bind_key("Ctrl-g", goto_url_in_new_tab)


# This class can be used to enter arbitrary Python expressions for the embedded
# interpreter to evaluate. (Note that it can only evalute Python expressions,
# not execute arbitrary Python code.) The callback function will use
# elinks.info_box() to display the results.
#
class simple_console:
    """Simple console for passing expressions to the Python interpreter."""
    def __init__(self):
        """Prompt for a Python expression."""
        elinks.input_box("Enter Python expression", self._callback, "Console")
    def _callback(self, input):
        """Evalute input and display the result (unless it's None)."""
        if input is None:
            # The user canceled.
            return
        result = eval(input)
        if result is not None:
            elinks.info_box(result, "Result")
elinks.bind_key("F5", simple_console)


# If you edit ~/.elinks/hooks.py while the browser is running, you can use
# this function to make your changes take effect immediately (without having
# to restart ELinks).
#
def reload_hooks():
    """Reload the ELinks Python hooks."""
    import hooks
    reload(hooks)
elinks.bind_key("F6", reload_hooks)


# This example demonstrates how to create a menu by providing a sequence of
# (string, function) tuples specifying each menu item.
#
def menu():
    """Let the user choose from a menu of Python functions."""
    items = (
        ("~Go to URL in new tab", goto_url_in_new_tab),
        ("Simple Python ~console", simple_console),
        ("~Reload Python hooks", reload_hooks),
        # ~ ("Read my favorite RSS/ATOM ~feeds", feedreader),
    )
    elinks.menu(items)
elinks.bind_key("F4", menu)


# Finally, a more elaborate demonstration: If you install the add-on Python
# module from http://feedparser.org/ this class can use it to parse a list
# of your favorite RSS/ATOM feeds, figure out which entries you haven't seen
# yet, open each of those entries in a new tab, and report statistics on what
# it fetched. The class is instantiated by pressing the "!" key.
#
# This class demonstrates the elinks.load() function, which can be used to
# load a document into the ELinks cache without displaying it to the user;
# the document's contents are passed to a Python callback function.

# ~ my_favorite_feeds = (
        # ~ "http://rss.gmane.org/messages/excerpts/gmane.comp.web.elinks.user",
        # ~ # ... add any other RSS or ATOM feeds that interest you ...
        # ~ # "http://elinks.cz/news.rss",
        # ~ # "http://www.python.org/channews.rdf",
# ~ )

# ~ class feedreader:

    # ~ """RSS/ATOM feed reader."""

    # ~ def __init__(self, feeds=my_favorite_feeds):
        # ~ """Constructor."""
        # ~ if elinks.home is None:
            # ~ raise elinks.error("Cannot identify unread entries without "
                               # ~ "a ~/.elinks configuration directory.")
        # ~ self._results = {}
        # ~ self._feeds = feeds
        # ~ for feed in feeds:
            # ~ elinks.load(feed, self._callback)

    # ~ def _callback(self, header, body):
        # ~ """Read a feed, identify unseen entries, and open them in new tabs."""
        # ~ import anydbm
        # ~ import feedparser # you need to get this module from feedparser.org
        # ~ import os

        # ~ if not body:
            # ~ return
        # ~ seen = anydbm.open(os.path.join(elinks.home, "rss.seen"), "c")
        # ~ feed = feedparser.parse(body)
        # ~ new = 0
        # ~ errors = 0
        # ~ for entry in feed.entries:
            # ~ try:
                # ~ if not seen.has_key(entry.link):
                    # ~ elinks.open(entry.link, new_tab=True)
                    # ~ seen[entry.link] = ""
                    # ~ new += 1
            # ~ except:
                # ~ errors += 1
        # ~ seen.close()
        # ~ self._tally(feed.channel.title, new, errors)

    # ~ def _tally(self, title, new, errors):
        # ~ """Record and report feed statistics."""
        # ~ self._results[title] = (new, errors)
        # ~ if len(self._results) == len(self._feeds):
            # ~ feeds = self._results.keys()
            # ~ feeds.sort()
            # ~ width = max([len(title) for title in feeds])
            # ~ fmt = "%*s   new entries: %2d   errors: %2d\n"
            # ~ summary = ""
            # ~ for title in feeds:
                # ~ new, errors = self._results[title]
                # ~ summary += fmt % (-width, title, new, errors)
            # ~ elinks.info_box(summary, "Feed Statistics")

# ~ elinks.bind_key("!", feedreader)

def pre_format_html_hook(url, html):
    """Rewrite the body of a document before it's formatted.

    This function should return a string for ELinks to format, or None
    if ELinks should format the original document body. It can be used
    to repair bad HTML, alter the layout or colors of a document, etc.

    Arguments:

    url -- The URL of the document.
    html -- The body of the document.

    """
    # ~ a=open('/home/aniru/urls','a');a.write('visiitng: '+str(url)+'\n');a.close()
    if "thehindu.com" in url:        return cleanup_thehindu(html)
    elif "tfp.is" in url or 'tfpdl.de' in url:        return cleanup_tfpdl(html)
    elif "indianexpress.com" in url:        return cleanup_ie(html)
    elif "thewire.in" in url:        return cleanup_the_wire(html)
    elif "manoramaonline.com" in url:        return cleanup_manorama(html)
    elif "livemint.com" in url:        return cleanup_livemint(html)
    elif "economist.com" in url:        return cleanup_economist(html)
    elif "washingtonpost.com" in url:        return cleanup_washingtonpost(html)
    elif "ft.com" in url:        return cleanup_ft(html)
    elif "hindustantimes.com" in url:        return cleanup_hindustan_times(html)
    elif "ndtv.com" in url:        return cleanup_ndtv(html)
    elif "eztv.io" in url:        return cleanup_eztv(html)
    elif "github.com" in url:        return cleanup_github(html)
    elif "business-standard.com" in url:        return cleanup_business_standard(html)
    elif "bbc.co.uk" in url or 'bbc.com/news' in url:        return cleanup_bbc(html)
    elif "tribune.com.pk" in url:        return cleanup_tribune_pk(html)
    elif "dawn.com" in url:        return cleanup_dawn(html)
    elif "wikipedia.org" in url:        return cleanup_wikipedia(html)
    elif "scroll.in" in url:        return cleanup_scroll(html)
    elif "mohfw.gov.in" in url:        return cleanup_mohfw(html)
    elif "independent.co.uk" in url:        return cleanup_independent_uk(html)
    elif "guardian.com" in url:        return cleanup_guardian(html)
    elif "rottentomatoes.com" in url:        return cleanup_rotten_tomatoes(html)
    elif "serakon.com" in url:        return cleanup_serakon(html)
    elif "cricbuzz.com/cricket-commentary/" in url:        return cleanup_cricbuzz_commentary(html)
    elif "cricbuzz.com" in url:        return cleanup_cricbuzz(html)
    elif "highlightsguru.com" in url:        return cleanup_highlightsguru(html)
    elif "tribuneindia.com" in url:        return cleanup_tribune_india(html)
    elif "mcumovieshome.com" in url:        return cleanup_mcumovieshome(html)
    elif "theprint.in" in url:        return cleanup_theprint(html)
    elif "github.com" in url:        return cleanup_github(html)
    # ~ elif "reddit.com" in url and 'm.reddit.com' not in url:        return cleanup_reddit(html)
    # ~ elif "livemint.com" in url:        return cleanup_livemint(html)
